﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Security;
using System.Threading;
using SbeAccountManager;
using Director.PortalApi;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Reflection;
using System.Media;

namespace Director
{

    public partial class Form1 : Form
    {

        struct NameAndID
        {
            public string Type;
            public string Name;
            public string ID;

            public override string ToString()
            {
                return Type + ": " + Name;
            }
        }
        
        
        #region Global Objects

        #region Misc Objects
        string m_program = "dbDirector";
        string m_source = "Application Error";
        string m_mode = "";
        string m_companyName = "";

        string m_reportEmailerBody = "Greetings Pedro!,\r\n\r\nYou are receiving this because of Dakota Backup's Automated " + 
            "EVault Reporting Tool and because Matt's too lazy to manually sift through tasks every day.";

        string m_groupBIEmailerBody = "Here is a copy of your Monthly Auto-Generated Backup Item Report. Take care now. Bye bye then.";

        object _locker = new object();

        bool m_formShown = false;

        Dictionary<string, string> m_vault2CWBoard = new Dictionary<string, string>();
        Dictionary<string, string> m_vault2CWServiceType = new Dictionary<string, string>();
        Dictionary<string, string> m_vault2CWLogin = new Dictionary<string, string>();
        Dictionary<string, int> m_vault2CWTeam = new Dictionary<string, int>();

        DateTime m_megaTaskGenerated = DateTime.MinValue;

        int m_slowMyAssDownSon = 0; // 500ms is good for outside work
        int _maxTicketErrorLines = 20;
        long _maxActiveThreads = 15;
        long _threadsReceived = 0;

        List<string> m_log = null;

        DataTable m_dtMegaTasksDetails = null;
        DataTable m_dtFilteredMegaTasksDetails = null;
        #endregion

        #region Director Objects
        CManager m_cmManager = null;
        CVaultConnection m_cvcConnector = null;

        DataTable m_dtCWTable = null;
        DataTable m_dtDirectorCustomerDetails = null;
        DataTable m_dtDirectorTasksDetails = null;
        DataTable m_dtDirectorCustomerLocationDetailsBasic = null;

        string m_baseVaultsFileName = "BaseVaults.txt";
        string m_tool = null;
        string m_vaultAddress = null;    
        bool m_loggedIntoDirector = false;
        bool m_requiresDirector = false;
        #endregion

        #region WebPortal Objects
        List<AgentData> _agentData = null;

        SoapApiClient m_sacApi = null;

        PortalApi.User m_uPortalUser = null;

        DataTable m_dtSites = null;
        DataTable m_dtSubsites = null;
        DataTable m_dtGroups = null;
        DataTable m_dtAgents = null;
        DataTable m_dtTasks = null;
        DataTable m_dtPortalTasksDetails = null;

        bool m_loggedIntoPortal = false;
        bool m_requiresPortal = false;
        #endregion

        #region Asigra Objects
        string m_accountID = null;
        string m_accountName = null;
        string m_accountNumber = null;
        string m_clientDescription = null;
        string m_clientID = null;
        string m_clientNumber = null;
        string m_setID = null;
        string m_setName = null;
        string m_systemID = null;
        string m_systemName = null;
        LoginInfo m_nocLogin = null;
        DataTable m_dtMegaSetsDetails = null;
        DataTable m_dtFilteredMegaSetsDetails = null;
        NOCApiService m_nocApi = null;
        bool m_loggedIntoNOC = false;
        bool m_requiresNOC = false;
        DataTable m_dtNOCSetsDetails = null;
        #endregion

        #region Veeam Objects
        
        string m_vConnStr = "Data Source=vsql.dakotabackup.local\\VEEAM;Initial Catalog = VMBP; Persist Security Info=True;User ID = sa; Password=K00l!oDud3";
        string m_vQuery = "";

        DataTable m_dtVeeamTable = null;

        #endregion

        #region ConnectWise Objects
        string m_cwConnStr = "Data Source=connect.ktconnections.com;Initial Catalog=cwwebapp_ktc;User Id=DBReports;Password=V@r7$ucUr;";
        string m_cwCompany = "ktc";
        string m_cwSite = "connect.ktconnections.com";
        string m_cwPassword = "3v@uuu17";
        #endregion

        #endregion



        public Form1(bool showWinform, string[] args)
        {
            m_formShown = showWinform;
            
            m_vQuery = Properties.Settings.Default.m_vQuery;
            
            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " RUN Application", EventLogEntryType.Information);

            InitializeComponent();
            
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            textBoxWritePath.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            m_vault2CWBoard.Add("dbvault.dakotabackup.com", "Backup Alerts - DCR");
            m_vault2CWServiceType.Add("dbvault.dakotabackup.com", "Backup Alert");
            m_vault2CWLogin.Add("dbvault.dakotabackup.com", "Evault-DBA");
            m_vault2CWTeam.Add("dbvault.dakotabackup.com", 29);

            m_vault2CWBoard.Add("ktcvault.dakotabackup.com", "Backup Alerts - KTC");
            m_vault2CWServiceType.Add("ktcvault.dakotabackup.com", "Backup Alert");
            m_vault2CWLogin.Add("ktcvault.dakotabackup.com", "Evault-MSA");
            m_vault2CWTeam.Add("ktcvault.dakotabackup.com", 30);

            m_vault2CWBoard.Add("rcvault.dakotabackup.com", "Backup Alerts - KTC");
            m_vault2CWServiceType.Add("rcvault.dakotabackup.com", "Backup Alert");
            m_vault2CWLogin.Add("rcvault.dakotabackup.com", "Evault-MSA");
            m_vault2CWTeam.Add("rcvault.dakotabackup.com", 30);

            switch (m_mode)
            {
                case "BI":
                    {
                        enableOnlyGroupBI();
                    }
                    break;
                case "Matt":
                    {
                        textBoxPortalPassword.Text = "";
                        textBoxPortalUsername.Text = "matt@dakotabackup.com";
                        textBoxDirectorUsername.Text = "mattadmin";
                        textBoxDirectorPassword.Text = "";
                        textBoxVault.Text = "ktcvault.dakotabackup.com";
                    }
                    break;
                default:
                    break;
            }
                
            //CWManipulator cw = new CWManipulator(m_cwCompany, "Evault-DBA", m_cwPassword, m_cwSite);
    
            if (!m_formShown)
            {
                handleHiddenRequest(args);
            }
        }



        #region Events

        #region Misc Events
        private void comboBoxDoWhat_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxDoWhat.Text)
            {
                case "Configure Agents with..":
                    tabControl1.SelectedTab = tabPageAgentConfig;
                    break;
                case "Show Asigra Sets that..":
                    tabControlSystem.SelectedTab = tabPageAsigra;
                    break;
                default:
                    tabControl1.SelectedTab = tabPageTaskQuery;
                    break;
            }
        }

        private void buttonDoIt_Click(object sender, EventArgs e)
        {
            if (m_log != null)
            {
                m_log.Clear();
                m_log = null;
            }

            m_log = new List<string>();

            switch (comboBoxDoWhat.Text)
            {
                case "Director: Quota/Pool Report":
                    {
                        m_requiresDirector = true;
                        m_requiresPortal = false;

                        if (m_loggedIntoDirector)
                        {
                            showWaitCursor();

                            m_tool = "Quota Pool";

                            generatePoolQuota(null, null);

                            showDefaultCursor();
                        }
                    }
                    break;
                case "Director: Base Vaults Usage Report":
                    {
                        showWaitCursor();

                        m_tool = "Base Vaults Usage";

                        generateBaseVaultsUsageExtended();

                        showDefaultCursor();
                    }
                    break;
                case "Portal: Task: Backup Items Report":
                    {
                        m_requiresPortal = true;
                        m_requiresDirector = false;

                        if (m_loggedIntoPortal)
                        {
                            showWaitCursor();

                            m_tool = "Task Backup Items";

                            generateTaskBI();

                            showDefaultCursor();
                        }
                    }
                    break;
                case "Portal: Group: Backup Items Report":
                    {
                        m_requiresPortal = true;
                        m_requiresDirector = false;

                        if (m_loggedIntoPortal)
                        {
                            showWaitCursor();

                            m_tool = "Group Backup Items";

                            generateGroupBI();

                            showDefaultCursor();
                        }
                    }
                    break;
                case "Portal: Subsite: Backup Items Report":
                    {
                        m_requiresPortal = true;
                        m_requiresDirector = false;

                        if (m_loggedIntoPortal)
                        {
                            showWaitCursor();

                            m_tool = "Subsite Backup Items";

                            generateSubsiteBI();

                            showDefaultCursor();
                        }
                    }
                     break;
                case "Show EVault Tasks that..":
                    {
                        m_requiresDirector = true;
                        m_requiresPortal = true;

                        if (m_loggedIntoDirector && m_loggedIntoPortal)
                        {
                            showWaitCursor();

                            m_tool = "Show EVault Tasks That";

                            BackgroundWorker getShowEVaultTasksBackground = new BackgroundWorker();
                            getShowEVaultTasksBackground.DoWork += getShowEVaultTasksBackground_DoWork;
                            getShowEVaultTasksBackground.RunWorkerCompleted += getShowEVaultTasksBackground_RunWorkerCompleted;

                            getShowEVaultTasksBackground.RunWorkerAsync();
                        }
                    }
                    break;
                case "Show Asigra Sets that..":
                    {
                        m_requiresNOC = true;

                        if (m_loggedIntoNOC)
                        {
                            showWaitCursor();

                            m_tool = "Show Asigra Sets That";

                            BackgroundWorker getShowAsigraSetsBackground = new BackgroundWorker();
                            getShowAsigraSetsBackground.DoWork += getShowAsigraSetsBackground_DoWork;
                            getShowAsigraSetsBackground.RunWorkerCompleted += getShowAsigraSetsBackground_RunWorkerCompleted;

                            getShowAsigraSetsBackground.RunWorkerAsync();
                        }
                    }
                    break;
                case "Configure Agents with..":
                    {                      
                        m_requiresDirector = true;
                        m_requiresPortal = true;

                        if (m_loggedIntoDirector && m_loggedIntoPortal)
                        {
                            showWaitCursor();

                            m_tool = "Configure Agents With";

                            configureAgents();

                            cleanUpAgentConfigTool();

                            showDefaultCursor();
                        }
                    }
                    break;
                case "Create CW Tickets from EVault Tasks":
                    {
                        showWaitCursor();

                        m_tool = "Create Tickets from Tasks";

                        textBox1.Clear();

                        createTasksFromHighlightedRows(true, null, null, null, null, null, -1);

                        showDefaultCursor();
                    }
                    break;
                case "Create CW Tickets from Asigra Sets":
                    {
                        showWaitCursor();

                        m_tool = "Create Tickets from Sets";

                        createAsigraTasksFromHighlightedRows(true, null, null, null, null, null, -1);

                        showDefaultCursor();
                    }
                    break;
                default:
                    break;
            }

            if ((m_requiresDirector && !m_loggedIntoDirector) ||
                (m_requiresPortal && !m_loggedIntoPortal) ||
                (m_requiresNOC && !m_loggedIntoNOC))
            {
                MessageBox.Show("Please to log in, first.");
            }
        }

        private void buttonTextToCSV_Click(object sender, EventArgs e)
        {
            showWaitCursor();

            if (!string.IsNullOrEmpty(textBox1.Text.Trim()))
            {
                string address = "";

                if (m_loggedIntoDirector)
                    address = m_cvcConnector.Address.Replace(".", "_") + "_";

                string fileName = m_tool + "_" + address +
                    DateTime.Now.ToString().Replace(":", "_").Replace("/", "_").Replace(" ", "_") + ".csv";

                File.WriteAllText(textBoxWritePath.Text + "\\" + fileName, textBox1.Text);

                MessageBox.Show(textBoxWritePath.Text + "\\" + fileName, "CSV Generated");
            }

            showDefaultCursor();
        }

        private void buttonTextToPDF_Click(object sender, EventArgs e)
        {
            showWaitCursor();

            if (!string.IsNullOrEmpty(textBox1.Text.Trim()))
            {
                string fileName = m_companyName + "_Backup Items_" + DateTime.Now.Month.ToString() + "." +
                    DateTime.Now.Day.ToString() + "." + DateTime.Now.Year.ToString() + ".pdf";

                Document pdfDoc = createBIDocument(m_companyName);

                PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always);

                pdfRenderer.Document = pdfDoc;
                pdfRenderer.RenderDocument();
                pdfRenderer.PdfDocument.Save(textBoxWritePath.Text + "\\" + fileName);
                MessageBox.Show(textBoxWritePath.Text + "\\" + fileName, "PDF Generated");
            }

            showDefaultCursor();
        }

        private void getShowEVaultTasksBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Invoke(new MethodInvoker(() => textBox1.Clear()));

            getMegaTasksDetailsTable(null);

            this.Invoke(new MethodInvoker(()=> this.Refresh()));
        }

        private void getShowEVaultTasksBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dataGridViewResults.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            if (checkBoxBackupLogs.Checked || checkBoxRestoreLogs.Checked)
            {
                string logType = "Backup";
                string filterType = "All";

                if (checkBoxRestoreLogs.Checked)
                    logType = "Restore";

                if (checkBoxErrorsWarningsOnly.Checked)
                    filterType = "ErrorsWarnings";
                else if (checkBoxErrorsOnly.Checked)
                    filterType = "Errors";

                string numLines = int.MaxValue.ToString();

                if (checkBoxLimitLogs.Checked)
                {
                    numLines = textBoxLimitLogs.Text;
                }

                getTasksLogs(logType, filterType, numLines);

                string fileName = "EVault Logs_" + DateTime.Now.ToString().Replace(":", "_").Replace("/", "_").Replace(" ", "_") + ".csv";

                using (StreamWriter writer = new StreamWriter(textBoxWritePath.Text + "\\" + fileName))
                {
                    if (m_log != null && m_log.Count() > 0)
                    {
                        foreach (string line in m_log)
                        {
                            if (!string.IsNullOrEmpty(line))
                                writer.WriteLine(line);
                        }
                    }
                }
            }
         
            this.Invoke(new MethodInvoker(()=> showDefaultCursor()));

            SystemSounds.Exclamation.Play();
        }

        private void reportEmailerBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string[] args = (string[])e.Argument;

                textBoxDirectorUsername.Text = args[1];
                textBoxDirectorPassword.Text = args[2];
                textBoxDomain.Text = args[3];
                textBoxVault.Text = args[4];
                textBoxPortalUsername.Text = args[5];
                textBoxPortalPassword.Text = args[6];

                string smtpServer = args[7];
                string smtpDomain = args[8];
                string smtpUsername = args[9];
                string smtpPassword = args[10];

                int smtpPort = Convert.ToInt32(args[11]);

                string configFile = args[12];   // configFile content structure ex:
                                                //matt.moore@dakotabackup.com;caleb.straub@dakotabackup.com -- to:
                                                //donotreply@dakotabackup.com                               -- from
                                                //email subject                                             -- subject
                                                //NumberOfSafesets > 0 AND AgentOnline = true               -- query
                                                //ErrorsWarnings or Errors or All or None                   -- last backup logType
                                                //All or #                                                  -- last number of lines in log
                                                //filename.csv                                              -- csv file and path to write results to
                                                //*agentonline*ptaskid*                                     -- column names to hide from results, separated by *'s
                                                // ** repeat previous structure as required **

                string[] fileLines = File.ReadAllLines(configFile);

                directorLogin();
                portalLogin();

                if (m_loggedIntoDirector && m_loggedIntoPortal)
                {
                    int reports = fileLines.Count() / 8;

                    for (int i = 0; i < reports; i++)
                    {
                        string to = fileLines[i * 8 + 0];
                        string from = fileLines[i * 8 + 1];
                        string subject = fileLines[i * 8 + 2];
                        string query = fileLines[i * 8 + 3];
                        string logType = fileLines[i * 8 + 4];
                        string numLines = fileLines[i * 8 + 5];
                        string fileName = fileLines[i * 8 + 6];
                        string hideColumns = fileLines[i * 8 + 7];

                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN reportEmailer:\"" + query + "\"", EventLogEntryType.Information);

                        this.Invoke(new MethodInvoker(() => textBox1.Clear()));

                        getMegaTasksDetailsTable(query);

                        if (m_log != null)
                        {
                            m_log.Clear();
                            m_log = null;
                        }

                        m_log = new List<string>();

                        if (logType != "None")
                        {
                            getTasksLogs("Backup", logType, numLines);
                        }

                        using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                        {
                            smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                            string body = m_reportEmailerBody;

                            using (MailMessage message = new MailMessage())
                            {
                                string[] tos = to.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                                foreach (string theto in tos)
                                {
                                    message.To.Add(theto);
                                }

                                message.From = new MailAddress(from);
                                message.Body = body;
                                message.Subject = subject;

                                string file = "";

                                if (m_dtFilteredMegaTasksDetails != null && m_dtFilteredMegaTasksDetails.Rows.Count > 0)
                                {
                                    foreach (DataColumn dc in m_dtFilteredMegaTasksDetails.Columns)
                                    {
                                        if (!hideColumns.Contains("*" + dc.ColumnName.ToLower() + "*"))
                                        {
                                            file += "\"" + dc.ColumnName + "\",";
                                        }
                                    }

                                    file += "\r\n";

                                    foreach (DataRow dr in m_dtFilteredMegaTasksDetails.Rows)
                                    {
                                        for (int j = 0; j < m_dtFilteredMegaTasksDetails.Columns.Count; j++)
                                        {
                                            if (!hideColumns.Contains("*" + m_dtFilteredMegaTasksDetails.Columns[j].ColumnName.ToLower() + "*"))
                                            {
                                                file += "\"" + dr[j].ToString() + "\",";
                                            }
                                        }

                                        file += "\r\n";
                                    }

                                    Attachment resultAttachment = Attachment.CreateAttachmentFromString(file, fileName);

                                    message.Attachments.Add(resultAttachment);
                                }

                                if (m_log.Count() > 0)
                                {
                                    file = "";

                                    foreach (string line in m_log)
                                    {
                                        file += line + "\r\n";
                                    }

                                    Attachment logsAttachment = Attachment.CreateAttachmentFromString(file, "Logs.csv");

                                    message.Attachments.Add(logsAttachment);
                                }

                                smtp.Send(message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void reportEmailerBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void reportCWTicketMakerBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string[] args = (string[])e.Argument;
                string filterNames = null;

                this.Invoke(new MethodInvoker(() =>
                {
                    textBoxDirectorUsername.Text = args[1];
                    textBoxDirectorPassword.Text = args[2];
                    textBoxDomain.Text = args[3];
                    textBoxVault.Text = args[4];
                    textBoxPortalUsername.Text = args[5];
                    textBoxPortalPassword.Text = args[6];
                    textBoxWritePath.Text = args[7];
                    filterNames = args[8];
                    //textBoxSaveLoadList.Text = args[8];
                }));
                
                string query = args[9];

                string recipientEmails = args[10];
                string smtpServer = args[11];
                string smtpDomain = args[12];
                string smtpUsername = args[13];
                string smtpPassword = args[14];

                int smtpPort = Convert.ToInt32(args[15]);

                string priority = args[16];
                string board = args[17];
                string serviceType = args[18];
                string login = args[19];
                string cwDefaultCompanyID = args[20];

                int team = Convert.ToInt32(args[21]);

                string[] filters = string.IsNullOrEmpty(filterNames) ? null : filterNames.Split(new string[] { " ::: " }, StringSplitOptions.RemoveEmptyEntries);
                string[] queries = string.IsNullOrEmpty(query) ? null : query.Split(new string[] {" ::: "}, StringSplitOptions.RemoveEmptyEntries);
                string[] priorities = string.IsNullOrEmpty(priority) ? null : priority.Split(new string[] {" ::: "}, StringSplitOptions.RemoveEmptyEntries);

                directorLogin();
                portalLogin();

                if (m_loggedIntoDirector && m_loggedIntoPortal)
                {
                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN reportCWTicketMaker:" + args[4] + ":\"" + query + "\"", EventLogEntryType.Information);

                    if (queries != null && queries.Count() > 0)
                    {
                        this.Invoke(new MethodInvoker(() => textBox1.Clear()));

                        for (int i = 0; i < queries.Count(); i++)
                        {
                            textBoxSaveLoadList.Text = filters[i];

                            if (m_dtMegaTasksDetails != null)
                            {
                                m_dtMegaTasksDetails.Dispose();
                                m_dtMegaTasksDetails = null;
                            }

                            string theQuery = queries[i];

                            if (theQuery.Contains("NOT_IN_LIST"))
                            {
                                buttonLoadList_Click(this, null);

                                this.Invoke(new MethodInvoker(() =>
                                {
                                    checkBoxThatChecklisted.Checked = true;
                                    comboBoxAreChecklisted.Text = "not in";
                                }));

                                theQuery = theQuery.Replace("NOT_IN_LIST", generateQuery());
                            }

                            if (theQuery.Contains("IS_IN_LIST"))
                            {
                                buttonLoadList_Click(this, null);

                                this.Invoke(new MethodInvoker(() =>
                                {
                                    checkBoxThatChecklisted.Checked = true;
                                    comboBoxAreChecklisted.Text = "in";
                                }));

                                theQuery = theQuery.Replace("IS_IN_LIST", generateQuery());
                            }

                            getMegaTasksDetailsTable(theQuery);

                            if (dataGridViewResults.InvokeRequired)
                            {
                                this.Invoke(new MethodInvoker(() =>
                                {
                                    dataGridViewResults.SelectAll();
                                }));
                            }
                            else
                            {
                                dataGridViewResults.SelectAll();
                            }

                            createTasksFromHighlightedRows(true, priorities[i], board, serviceType, login, cwDefaultCompanyID, team);
                        }
                    }

                    using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                    {
                        smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                        using (MailMessage message = new MailMessage())
                        {
                            string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string theto in tos)
                            {
                                message.To.Add(theto);
                            }

                            message.From = new MailAddress("donotreply@ktconnections.com");
                            
                            message.IsBodyHtml = true;

                            this.Invoke(new MethodInvoker(() => message.Body = "<div style=\"font-size:11pt;font-family:calibri\">" +  textBox1.Text.Replace("\r\n", "<br/>") + "</div>"));

                            message.Subject = textBoxVault.Text + " Auto Ticket Maker Results: " + args[16] + ": " + DateTime.Now.ToShortDateString();

                            smtp.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void reportCWTicketMakerBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void buttonChecklistSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBoxQuery.Items.Count; i++)
            {
                checkedListBoxQuery.SetItemChecked(i, true);
            }         
        }

        private void buttonChecklistAddTask_Click(object sender, EventArgs e)
        {
            if (comboBoxAgents.SelectedItem != null && comboBoxJobs.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxAgents.SelectedItem))["Parent"].ToString() + "\\" +
                    ((DataRowView)(comboBoxAgents.SelectedItem))["Name"].ToString() + "\\" +
                    ((DataRowView)(comboBoxJobs.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxJobs.SelectedItem))["Id"].ToString();

                newObject.Type = "Task";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddAgent_Click(object sender, EventArgs e)
        {
            if (comboBoxAgents.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxAgents.SelectedItem))["Parent"].ToString() + "\\" +
                    ((DataRowView)(comboBoxAgents.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxAgents.SelectedItem))["Id"].ToString();

                newObject.Type = "Agent";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddGroup_Click(object sender, EventArgs e)
        {
            if (comboBoxGroups.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxGroups.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxGroups.SelectedItem))["Id"].ToString();

                newObject.Type = "Group";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddSubsite_Click(object sender, EventArgs e)
        {
            if (comboBoxSubsites.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxSubsites.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxSubsites.SelectedItem))["Id"].ToString();

                newObject.Type = "Site";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddSite_Click(object sender, EventArgs e)
        {
            if (comboBoxSites.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxSites.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxSites.SelectedItem))["Id"].ToString();

                newObject.Type = "Site";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddDSS_Click(object sender, EventArgs e)
        {
            if (comboBoxSystems.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxSystems.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxSystems.SelectedItem))["Id"].ToString();

                newObject.Type = "DSS";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddDSCustomer_Click(object sender, EventArgs e)
        {
            if (comboBoxCustomers.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxCustomers.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxCustomers.SelectedItem))["Id"].ToString();

                newObject.Type = "DSCustomer";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddDSC_Click(object sender, EventArgs e)
        {
            if (comboBoxClients.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxClients.SelectedItem))["Description"].ToString();

                newObject.ID = ((DataRowView)(comboBoxClients.SelectedItem))["Name"].ToString();

                newObject.Type = "DSC";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistAddDSCJob_Click(object sender, EventArgs e)
        {
            if (comboBoxSets.SelectedItem != null)
            {
                NameAndID newObject = new NameAndID();

                newObject.Name = ((DataRowView)(comboBoxSets.SelectedItem))["Name"].ToString();

                newObject.ID = ((DataRowView)(comboBoxSets.SelectedItem))["Id"].ToString();

                newObject.Type = "DSCJob";

                checkedListBoxQuery.Items.Add(newObject);
            }
        }

        private void buttonChecklistRemoveSelected_Click(object sender, EventArgs e)
        {
            for (int i = checkedListBoxQuery.Items.Count - 1; i > -1; i--)
            {
                if (checkedListBoxQuery.GetItemChecked(i))
                {
                    checkedListBoxQuery.Items.RemoveAt(i);
                }
            }
        }

        private void buttonSaveTaskList_Click(object sender, EventArgs e)
        {
            string toWrite = "";

            foreach (NameAndID n in checkedListBoxQuery.Items)
            {
                toWrite += n.Type + "<***>" + n.Name + "<***>" + n.ID + "\r\n";
            }

            try
            {
                File.WriteAllText(textBoxWritePath.Text + "\\" + textBoxSaveLoadList.Text.Trim(), toWrite);
            }
            catch { }
        }

        private void buttonLoadList_Click(object sender, EventArgs e)
        {
            string[] readLines = null;

            try
            {
                checkedListBoxQuery.Items.Clear();

                readLines = File.ReadAllLines(textBoxWritePath.Text + "\\" + textBoxSaveLoadList.Text.Trim());

                foreach (string line in readLines)
                {
                    string[] splits = line.Split(new string[] { "<***>" }, StringSplitOptions.RemoveEmptyEntries);

                    NameAndID n = new NameAndID();
                    n.Type = splits[0];
                    n.Name = splits[1];
                    n.ID = splits[2];

                    checkedListBoxQuery.Items.Add(n);
                }
            }
            catch { }
        }

        private void dataGridViewResults_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            showWaitCursor();

            if (e.RowIndex > -1)
            {
                string taskID = dataGridViewResults.Rows[e.RowIndex].Cells["PTaskID"].Value.ToString();
                Guid agentID = Guid.Parse(dataGridViewResults.Rows[e.RowIndex].Cells["PComputerID"].Value.ToString());

                string[] results = getAgentJobLogMessages(agentID, taskID, JobLogFileType.Backup, m_uPortalUser.Id, "newest", LogFilterType.None);

                tabControlResults.SelectedTab = tabPageTextBoxResults;
                richTextBoxResults.Clear();

                if (results != null && results.Count() > 0)
                {
                    int startIndex = 0;

                    foreach (string line in results)
                    {
                        richTextBoxResults.AppendText(line + "\r\n");

                        richTextBoxResults.SelectionStart = startIndex;
                        richTextBoxResults.SelectionLength = line.Length;

                        startIndex += line.Length + 1;

                        if (line.Contains("-E-") || line.Contains("-F-"))
                        {
                            richTextBoxResults.SelectionColor = System.Drawing.Color.DarkRed;
                            richTextBoxResults.SelectionFont = new System.Drawing.Font("Microsoft Sans Serif", 9.0f, FontStyle.Bold);
                        }
                        else if (line.Contains("-W-"))
                        {
                            richTextBoxResults.SelectionColor = System.Drawing.Color.DarkGoldenrod;
                            richTextBoxResults.SelectionFont = new System.Drawing.Font("Microsoft Sans Serif", 9.0f, FontStyle.Bold);
                        }
                        else
                        {
                            richTextBoxResults.SelectionColor = System.Drawing.Color.DarkGreen;
                            richTextBoxResults.SelectionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular);
                        }
                    }
                }

                showDefaultCursor();
            }
        }

        private void buttonResetQuery_Click(object sender, EventArgs e)
        {
            cleanUpQueryTool();
        }

        private void buttonGenerateQuery_Click(object sender, EventArgs e)
        {
            textBoxQuery.Text = generateQuery();
        }

        private void buttonReadWritePath_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxWritePath.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        #endregion


        #region WebPortal Events


        private void buttonPortalLogIn_Click(object sender, EventArgs e)
        {
            showWaitCursor();

            portalLogin();

            showDefaultCursor();
        }

        private void buttonGetSubsites_Click(object sender, EventArgs e)
        {
            if (m_loggedIntoPortal)
            {
                if (m_dtSubsites != null)
                {
                    m_dtSubsites.Dispose();
                    m_dtSubsites = null;
                }

                m_dtSubsites = new DataTable();

                m_dtSubsites.Columns.Add("Id");
                m_dtSubsites.Columns.Add("Name");

                try
                {
                    Guid companyID = Guid.Parse(((DataRowView)(comboBoxSites.SelectedItem))["Id"].ToString());

                    Company[] companies = m_sacApi.GetUserCompanies(m_uPortalUser.Id, m_uPortalUser.Id);

                    if (companies != null && companies.Count() > 0)
                    {
                        foreach (Company c in companies)
                        {
                            DataRow newRow = m_dtSubsites.NewRow();

                            newRow["Id"] = c.Id;
                            newRow["Name"] = c.Name;

                            m_dtSubsites.Rows.Add(newRow);
                            m_dtSubsites.AcceptChanges();
                        }
                    
                        comboBoxSubsites.DataSource = m_dtSubsites;
                        comboBoxSubsites.DisplayMember = "Name";
                        comboBoxSubsites.ValueMember = "Id";
                    }
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void buttonGetSiteAgents_Click(object sender, EventArgs e)
        {
            if (m_loggedIntoPortal)
            {
                if (m_dtAgents != null)
                {
                    m_dtAgents.Dispose();
                    m_dtAgents = null;
                }

                m_dtAgents = new DataTable();

                m_dtAgents.Columns.Add("Id");
                m_dtAgents.Columns.Add("Name");

                try
                {
                    Guid companyID = Guid.Parse(((DataRowView)(comboBoxSites.SelectedItem))["Id"].ToString());
                    string companyName = ((DataRowView)(comboBoxSubsites.SelectedItem))["Name"].ToString();

                    Agent[] agents = m_sacApi.GetCompanyAgents(companyID, true, m_uPortalUser.Id);

                    if (agents != null && agents.Count() > 0)
                    {
                        foreach (Agent a in agents)
                        {
                            DataRow newRow = m_dtAgents.NewRow();

                            newRow["Id"] = a.Id;
                            newRow["Name"] = a.Name;
                            newRow["Parent"] = companyName;

                            m_dtAgents.Rows.Add(newRow);
                            m_dtAgents.AcceptChanges();
                        }
                    }

                    comboBoxAgents.DataSource = m_dtAgents;
                    comboBoxAgents.DisplayMember = "Name";
                    comboBoxAgents.ValueMember = "Id";
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void buttonGetSubsiteAgents_Click(object sender, EventArgs e)
        {
            if (m_loggedIntoPortal)
            {
                if (m_dtAgents != null)
                {
                    m_dtAgents.Dispose();
                    m_dtAgents = null;
                }

                m_dtAgents = new DataTable();

                m_dtAgents.Columns.Add("Id");
                m_dtAgents.Columns.Add("Name");
                m_dtAgents.Columns.Add("Parent");

                try
                {
                    Guid companyID = Guid.Parse(((DataRowView)(comboBoxSubsites.SelectedItem))["Id"].ToString());
                    string companyName = ((DataRowView)(comboBoxSubsites.SelectedItem))["Name"].ToString();

                    Agent[] agents = m_sacApi.GetCompanyAgents(companyID, true, m_uPortalUser.Id);

                    if (agents != null && agents.Count() > 0)
                    {
                        foreach (Agent a in agents)
                        {
                            DataRow newRow = m_dtAgents.NewRow();

                            newRow["Id"] = a.Id;
                            newRow["Name"] = a.Name;
                            newRow["Parent"] = companyName;

                            m_dtAgents.Rows.Add(newRow);
                            m_dtAgents.AcceptChanges();
                        }
                    }

                    comboBoxAgents.DataSource = m_dtAgents;
                    comboBoxAgents.DisplayMember = "Name";
                    comboBoxAgents.ValueMember = "Id";
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void buttonGroupAgents_Click(object sender, EventArgs e)
        {
            if (m_loggedIntoPortal)
            {
                if (m_dtAgents != null)
                {
                    m_dtAgents.Dispose();
                    m_dtAgents = null;
                }

                m_dtAgents = new DataTable();

                m_dtAgents.Columns.Add("Id");
                m_dtAgents.Columns.Add("Name");
                m_dtAgents.Columns.Add("Parent");

                try
                {
                    string groupName = ((DataRowView)(comboBoxGroups.SelectedItem))["Name"].ToString();
                    
                    Agent[] agents = m_sacApi.GetUserAgents(m_uPortalUser.Id, m_uPortalUser.Id);

                    if (agents != null && agents.Count() > 0)
                    {
                        foreach (Agent a in agents)
                        {
                            if (a.AssignedAgentGroups.Contains(groupName))
                            {
                                DataRow newRow = m_dtAgents.NewRow();

                                newRow["Id"] = a.Id;
                                newRow["Name"] = a.Name;
                                newRow["Parent"] = groupName;

                                m_dtAgents.Rows.Add(newRow);
                                m_dtAgents.AcceptChanges();
                            }
                        }
                    }

                    comboBoxAgents.DataSource = m_dtAgents;
                    comboBoxAgents.DisplayMember = "Name";
                    comboBoxAgents.ValueMember = "Id";
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void buttonGetTasks_Click(object sender, EventArgs e)
        {
            if (m_loggedIntoPortal)
            {
                if (m_dtTasks != null)
                {
                    m_dtTasks.Dispose();
                    m_dtTasks = null;
                }

                m_dtTasks = new DataTable();

                m_dtTasks.Columns.Add("Id");
                m_dtTasks.Columns.Add("Name");

                try
                {
                    Guid agentID = Guid.Parse(((DataRowView)(comboBoxAgents.SelectedItem))["Id"].ToString());

                    Job[] jobs = m_sacApi.GetAgentJobs(agentID, m_uPortalUser.Id);

                    if (jobs != null && jobs.Count() > 0)
                    {
                        foreach (Job j in jobs)
                        {
                            DataRow newRow = m_dtTasks.NewRow();

                            newRow["Id"] = j.Id;
                            newRow["Name"] = j.Name;

                            m_dtTasks.Rows.Add(newRow);
                            m_dtTasks.AcceptChanges();
                        }

                        comboBoxJobs.DataSource = m_dtTasks;
                        comboBoxJobs.DisplayMember = "Name";
                        comboBoxAgents.ValueMember = "Id";
                    }
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void textBoxPortalInfos_TextChanged(object sender, EventArgs e)
        {
            m_loggedIntoPortal = false;
        }
        #endregion


        #region Director Events

        private void buttonDirectorLogIn_Click(object sender, EventArgs e)
        {
            showWaitCursor();

            directorLogin();

            showDefaultCursor();
        }

        private void textBoxDirectorInfos_TextChanged(object sender, EventArgs e)
        {
            m_loggedIntoDirector = false;
        }

        private void reportDirectorCWTicketMakerBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string[] args = (string[])e.Argument;

                this.Invoke(new MethodInvoker(() =>
                {
                    textBoxDirectorUsername.Text = args[1];
                    textBoxDirectorPassword.Text = args[2];
                    textBoxDomain.Text = args[3];
                    textBoxVault.Text = args[4];
                    textBoxWritePath.Text = args[5];
                    textBoxSaveLoadList.Text = args[6];
                }));

                string query = args[7];

                string recipientEmails = args[8];
                string smtpServer = args[9];
                string smtpDomain = args[10];
                string smtpUsername = args[11];
                string smtpPassword = args[12];

                int smtpPort = Convert.ToInt32(args[13]);

                string priority = args[14];
                string board = args[15];
                string serviceType = args[16];
                string login = args[17];
                string cwDefaultCompanyID = args[18];

                int team = Convert.ToInt32(args[19]);

                string[] queries = string.IsNullOrEmpty(query) ? null : query.Split(new string[] { " ::: " }, StringSplitOptions.RemoveEmptyEntries);
                string[] priorities = string.IsNullOrEmpty(priority) ? null : priority.Split(new string[] { " ::: " }, StringSplitOptions.RemoveEmptyEntries);

                directorLogin();

                if (m_loggedIntoDirector)
                {
                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN reportDirectorCWTicketMaker:" + args[4] + ":\"" + query + "\"", EventLogEntryType.Information);

                    if (queries != null && queries.Count() > 0)
                    {
                        this.Invoke(new MethodInvoker(() => textBox1.Clear()));

                        for (int i = 0; i < queries.Count(); i++)
                        {
                            string theQuery = queries[i];

                            if (theQuery.Contains("NOT_IN_LIST"))
                            {
                                buttonLoadList_Click(this, null);

                                this.Invoke(new MethodInvoker(() =>
                                {
                                    checkBoxThatChecklisted.Checked = true;
                                    comboBoxAreChecklisted.Text = "not in";
                                }));

                                theQuery = theQuery.Replace("NOT_IN_LIST", generateQuery());
                            }

                            if (theQuery.Contains("IS_IN_LIST"))
                            {
                                buttonLoadList_Click(this, null);

                                this.Invoke(new MethodInvoker(() =>
                                {
                                    checkBoxThatChecklisted.Checked = true;
                                    comboBoxAreChecklisted.Text = "in";
                                }));

                                theQuery = theQuery.Replace("IS_IN_LIST", generateQuery());
                            }

                            if (m_dtDirectorCustomerDetails != null)
                            {
                                m_dtDirectorCustomerDetails.Dispose();
                                m_dtDirectorCustomerDetails = null;
                            }

                            if (m_dtDirectorTasksDetails != null)
                            {
                                m_dtDirectorTasksDetails.Dispose();
                                m_dtDirectorTasksDetails = null;
                            }

                            getDirectorCustomerDetailsTable(null);

                            getDirectorTasksDetailsTable(theQuery);

                            this.Invoke(new MethodInvoker(() =>
                            {
                                dataGridViewResults.SelectAll();

                                createTasksFromHighlightedRows(true, priorities[i], board, serviceType, login, cwDefaultCompanyID, team);
                            }));
                        }
                    }

                    using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                    {
                        smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                        using (MailMessage message = new MailMessage())
                        {
                            string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string theto in tos)
                            {
                                message.To.Add(theto);
                            }

                            message.From = new MailAddress("donotreply@ktconnections.com");

                            message.IsBodyHtml = true;

                            this.Invoke(new MethodInvoker(() => message.Body = "<div style=\"font-size:11pt;font-family:calibri\">" + textBox1.Text.Replace("\r\n", "<br/>") + "</div>"));

                            message.Subject = textBoxVault.Text + " Auto Ticket Maker Results for Director: " + args[16] + ": " + DateTime.Now.ToShortDateString();

                            smtp.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void reportDirectorCWTicketMakerBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        #endregion


        #region Asigra Events
        private void buttonNOCLogIn_Click(object sender, EventArgs e)
        {
            showWaitCursor();

            nocLogin();

            showDefaultCursor();
        }
        private void textBoxNOCInfos_TextChanged(object sender, EventArgs e)
        {
            m_loggedIntoNOC = false;
        }
        private void comboBoxSystems_SelectedValueChanged(object sender, EventArgs e)
        {
            showWaitCursor();

            m_systemID = ((DataRowView)(comboBoxSystems.SelectedValue))["ID"].ToString();
            m_systemName = ((DataRowView)(comboBoxSystems.SelectedValue))["Name"].ToString();

            DataTable dt = new DataTable();

            dt.Columns.Add("ID");
            dt.Columns.Add("Name");
            dt.Columns.Add("Number");

            Customer[] customers = null;
            
            try
            {
                customers = m_nocApi.getAccountList(m_nocLogin, 0, true, Convert.ToInt64(m_systemID), true);
            }
            catch { }

            if (customers != null && customers.Count() > 0)
            {
                foreach (Customer customer in customers)
                {
                    DataRow dr = dt.NewRow();

                    dr["ID"] = customer.accountId;
                    dr["Name"] = customer.accountName;
                    dr["Number"] = customer.accountNum;

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
            }

            dt.DefaultView.Sort = "Name ASC";

            comboBoxCustomers.DataSource = dt;
            comboBoxCustomers.DisplayMember = "Name";

            showDefaultCursor();
        }
        private void comboBoxCustomers_SelectedValueChanged(object sender, EventArgs e)
        {
            showWaitCursor();

            m_accountID = ((DataRowView)(comboBoxCustomers.SelectedValue))["ID"].ToString();
            m_accountName = ((DataRowView)(comboBoxCustomers.SelectedValue))["Name"].ToString();
            m_accountNumber = ((DataRowView)(comboBoxCustomers.SelectedValue))["Number"].ToString();

            DataTable dt = new DataTable();

            dt.Columns.Add("Description");
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");

            DSClient[] clients = null;

            try
            {
                clients = m_nocApi.getDSClientList(m_nocLogin, 0, true, Convert.ToInt64(m_systemID), true,
                    m_accountNumber, false, true, false, true, false, true);
            }
            catch { }

            if (clients != null && clients.Count() > 0)
            {
                foreach (DSClient client in clients)
                {
                    DataRow dr = dt.NewRow();

                    dr["Description"] = client.DSBoxNumber + " - " + "[" + client.description + "]";
                    dr["ID"] = client.id.ToString();
                    dr["Name"] = client.DSBoxNumber;

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
            }

            dt.DefaultView.Sort = "Description ASC";

            comboBoxClients.DataSource = dt;
            comboBoxClients.DisplayMember = "Description";

            showDefaultCursor();
        }
        private void comboBoxClients_SelectedValueChanged(object sender, EventArgs e)
        {
            showWaitCursor();

            m_clientDescription = ((DataRowView)(comboBoxClients.SelectedValue))["Description"].ToString();
            m_clientID = ((DataRowView)(comboBoxClients.SelectedValue))["ID"].ToString();
            m_clientNumber = ((DataRowView)(comboBoxClients.SelectedValue))["Name"].ToString();

            DataTable dt = new DataTable();

            dt.Columns.Add("ID");
            dt.Columns.Add("Name");

            BackupSet[] sets = null;

            try
            {
                sets = m_nocApi.getBackupSetList(m_nocLogin, Convert.ToInt64(m_systemID), true, m_clientNumber);
            }
            catch { }

            if (sets != null && sets.Count() > 0)
            {
                foreach (BackupSet set in sets)
                {
                    DataRow dr = dt.NewRow();

                    dr["ID"] = set.reqID;
                    dr["Name"] = set.BSetFullName;

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
            }

            dt.DefaultView.Sort = "Name ASC";

            comboBoxSets.DataSource = dt;
            comboBoxSets.DisplayMember = "Name";

            showDefaultCursor();
        }
        private void getShowAsigraSetsBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            getMegaSetsDetailsTable(null);

            this.Invoke(new MethodInvoker(() => this.Refresh()));
        }
        private void getShowAsigraSetsBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dataGridViewResults.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            this.Invoke(new MethodInvoker(() => showDefaultCursor()));

            SystemSounds.Exclamation.Play();
        }
        private void asigraCWTicketMakerBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string filterNames = null;

                string[] args = (string[])e.Argument;

                textBoxNOCUsername.Text = args[1];
                textBoxNOCPassword.Text = args[2];
                textBoxNOC.Text = args[3];
                textBoxWritePath.Text = args[4];
                filterNames = args[5];
                //textBoxSaveLoadList.Text = args[5];

                string query = args[6];

                string recipientEmails = args[7];
                string smtpServer = args[8];
                string smtpDomain = args[9];
                string smtpUsername = args[10];
                string smtpPassword = args[11];

                int smtpPort = Convert.ToInt32(args[12]);

                string priority = args[13];
                string board = args[14];
                string serviceType = args[15];
                string login = args[16];
                string cwDefaultCompanyID = args[17];

                int team = Convert.ToInt32(args[18]);

                string[] filters = string.IsNullOrEmpty(filterNames) ? null : filterNames.Split(new string[] { " ::: " }, StringSplitOptions.RemoveEmptyEntries);
                string[] queries = string.IsNullOrEmpty(query) ? null : query.Split(new string[] { " ::: " }, StringSplitOptions.RemoveEmptyEntries);
                string[] priorities = string.IsNullOrEmpty(priority) ? null : priority.Split(new string[] { " ::: " }, StringSplitOptions.RemoveEmptyEntries);

                nocLogin();

                if (m_loggedIntoNOC)
                {
                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN asigraCWTicketMaker:\"" + query + "\"", EventLogEntryType.Information);

                    if (queries != null && queries.Count() > 0)
                    {
                        this.Invoke(new MethodInvoker(() => textBox1.Clear()));

                        for (int i = 0; i < queries.Count(); i++)
                        {
                            textBoxSaveLoadList.Text = filters[i];

                            if (m_dtMegaTasksDetails != null)
                            {
                                m_dtMegaTasksDetails.Dispose();
                                m_dtMegaTasksDetails = null;
                            }

                            string theQuery = queries[i];

                            if (theQuery.Contains("NOT_IN_LIST"))
                            {
                                buttonLoadList_Click(this, null);

                                this.Invoke(new MethodInvoker(() =>
                                {
                                    checkBoxThatChecklisted.Checked = true;
                                    comboBoxAreChecklisted.Text = "not in";
                                }));

                                theQuery = theQuery.Replace("NOT_IN_LIST", generateQuery());
                            }

                            if (theQuery.Contains("IS_IN_LIST"))
                            {
                                buttonLoadList_Click(this, null);

                                this.Invoke(new MethodInvoker(() =>
                                {
                                    checkBoxThatChecklisted.Checked = true;
                                    comboBoxAreChecklisted.Text = "in";
                                }));

                                theQuery = theQuery.Replace("IS_IN_LIST", generateQuery());
                            }

                            getMegaSetsDetailsTable(theQuery);

                            this.Invoke(new MethodInvoker(() =>
                            {
                                dataGridViewResults.SelectAll();

                                createAsigraTasksFromHighlightedRows(true, priorities[i], board, serviceType, login, cwDefaultCompanyID, team);
                            }));
                        }
                    }

                    using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                    {
                        smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                        using (MailMessage message = new MailMessage())
                        {
                            string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string theto in tos)
                            {
                                message.To.Add(theto);
                            }

                            message.From = new MailAddress("donotreply@ktconnections.com");

                            message.IsBodyHtml = true;

                            this.Invoke(new MethodInvoker(() => message.Body = "<div style=\"font-size:11pt;font-family:calibri\">" + textBox1.Text.Replace("\r\n", "<br/>") + "</div>"));

                            message.Subject = textBoxNOC.Text + " Auto Ticket Maker Results: " + DateTime.Now.ToShortDateString();

                            smtp.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }
        private void asigraCWTicketMakerBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }
        #endregion


        #region Veeam Events
        private void veeamCWTicketMakerBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string[] args = (string[])e.Argument;

                string mapPath = args[1] + args[2];

                string recipientEmails = args[3];
                string smtpServer = args[4];
                string smtpDomain = args[5];
                string smtpUsername = args[6];
                string smtpPassword = args[7];

                int smtpPort = Convert.ToInt32(args[8]);

                string priority = args[9];
                string serviceType = args[10];
                string queryFile = args[11];
                string filter = "";

                string[] filterLines = File.ReadAllLines(queryFile);
                
                foreach (string filterLine in filterLines)
                {
                    filter += filterLine;
                }

                Dictionary<string, string> v2cMap = new Dictionary<string, string>();

                foreach (string mapLine in File.ReadAllLines(mapPath))
                {
                    if (!string.IsNullOrEmpty(mapLine))
                    {
                        string[] maps = mapLine.Split(new string[] { "<->" }, StringSplitOptions.RemoveEmptyEntries);

                        v2cMap.Add(maps[0], mapLine);
                    }
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN veeamCWTicketMaker", EventLogEntryType.Information);

                fillVeeamTable(ref m_dtVeeamTable, m_vQuery + " AND " + filter);

                this.Invoke(new MethodInvoker(() => textBox1.Clear()));

                foreach (string vbrServer in v2cMap.Keys)
                {
                    DataRow[] jobRows = m_dtVeeamTable.Select("server_name = '" + vbrServer + "'", "job_name ASC");

                    List<string> jobs = new List<string>();

                    string currJobName = "";

                    if (jobRows != null && jobRows.Count() > 0)
                    {
                        foreach (DataRow row in jobRows)
                        {
                            if (currJobName != row["job_name"].ToString())
                            {
                                jobs.Add(row["job_name"].ToString());
                            }

                            currJobName = row["job_name"].ToString();
                        }
                    }

                    jobRows = null;

                    if (jobs != null && jobs.Count > 0)
                    {
                        foreach (string jobName in jobs)
                        {
                            bool needsTicket = false;

                            DataRow[] jsRows = m_dtVeeamTable.Select("server_name = '" + vbrServer + "' AND job_name = '" + jobName + "'", "start_time DESC");

                            if (jsRows.Count() == 1 && string.IsNullOrEmpty(jsRows[0]["start_time"].ToString()))
                            {
                                needsTicket = true;
                            }
                            else
                            {
                                DateTime lastStart = Convert.ToDateTime(jsRows[0]["start_time"].ToString()).ToLocalTime();

                                DayOfWeek dow = lastStart.DayOfWeek;

                                double dsls = ((TimeSpan)DateTime.Now.Subtract(lastStart)).TotalDays;

                                bool hadSuccess = false;

                                foreach (DataRow session in jsRows)
                                {
                                    if (session["session_result"].ToString().ToLower().Contains("success"))
                                    {
                                        hadSuccess = true;
                                    }
                                }

                                if (!hadSuccess || ((dsls > 2.25 && dow != DayOfWeek.Friday) || (dsls > 3.25 && dow == DayOfWeek.Friday)))
                                {
                                    needsTicket = true;
                                }
                            }

                            if (needsTicket)
                            {
                                string[] map = v2cMap[vbrServer].Split(new string[] { "<->" }, StringSplitOptions.RemoveEmptyEntries);

                                createVeeamTasks(jsRows, true, priority, map[4], serviceType, map[2], map[1], Convert.ToInt32(map[3]));
                            }
                        }
                    }
                }

                using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                {
                    smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                    using (MailMessage message = new MailMessage())
                    {
                        string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string theto in tos)
                        {
                            message.To.Add(theto);
                        }

                        message.From = new MailAddress("donotreply@ktconnections.com");

                        message.IsBodyHtml = true;

                        this.Invoke(new MethodInvoker(() => message.Body = "<div style=\"font-size:11pt;font-family:calibri\">" + textBox1.Text.Replace("\r\n", "<br/>") + "</div>"));

                        message.Subject = "Veeam Auto Ticket Maker Results: " + DateTime.Now.ToShortDateString();

                        smtp.Send(message);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void veeamCWTicketMakerBackground_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }
        #endregion

        #endregion



        #region Helper Methods

        #region Misc Helpers
        private void enableOnlyGroupBI()
        {
            this.Size = new Size(1100, 460);

            traverseControlsForBI(this);

            tabPageAsigra.Dispose();
            tabPageAsigra = null;
        }

        private void traverseControlsForBI(Control theControl)
        {
            foreach (Control c in theControl.Controls)
            {
                if (c.HasChildren)
                {
                    traverseControlsForBI(c);
                }

                if (c.Equals(label13) || c.Equals(label14) || c.Equals(textBoxPortalPassword) ||
                    c.Equals(textBoxPortalUsername) || c.Equals(buttonPortalLogIn))
                {
                    c.Location = new Point(c.Location.X, 9);
                }
                else if (c.Equals(label7) || c.Equals(comboBoxGroups))
                {
                    c.Location = new Point(c.Location.X, 70);
                }
                else if (c.Equals(buttonDoIt))
                {
                    c.Text = "Report";
                    c.Location = new Point(c.Location.X - 30, 135);
                }
                else if (c.Equals(buttonTextToPDF) || c.Equals(textBox1))
                {
                    if (c.Equals(textBox1))
                    {
                        c.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
                    }

                    c.Location = new Point(500, c.Location.Y);
                }
                else if (c.Equals(tabControlSystem))
                {
                    c.Size = new Size(c.Size.Width - 30, c.Size.Height - 150);
                }
                else
                {
                    if (!c.Equals(tabPageEVault))
                    {
                        c.Hide();
                    }
                }

                comboBoxDoWhat.Text = "Portal: Group: Backup Items Report";
            }
        }

        private void createTasksFromHighlightedRows(bool skipExistingSummary, string priority, string board, string serviceType, string login, string defaultCompanyID, int team)
        {
            DataGridViewSelectedRowCollection rows = dataGridViewResults.SelectedRows;

            bool isBoth = false;
            bool isD = false;

            if (dataGridViewResults.Columns.Contains("DComputerID") && dataGridViewResults.Columns.Contains("PComputerID"))
            {
                isBoth = true;
            }
            else if (dataGridViewResults.Columns.Contains("DComputerID"))
            {
                isD = true;
            }
            












            if (rows != null && rows.Count > 0)
            {
                if (board == null || serviceType == null || login == null || team < 0)
                {
                    try
                    {
                        try
                        {
                            board = m_vault2CWBoard[m_vaultAddress];
                        }
                        catch { }

                        try
                        {
                            serviceType = m_vault2CWServiceType[m_vaultAddress];
                        }
                        catch { }

                        try
                        {
                            login = m_vault2CWLogin[m_vaultAddress];
                        }
                        catch { }

                        try
                        {
                            team = m_vault2CWTeam[m_vaultAddress];
                        }
                        catch { }
                    }
                    catch (Exception ex)
                    {
                        if (m_formShown)
                        {
                            MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                        }

                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);

                        board = "Dakota Backup Alerts";
                        serviceType = "Backup Alert";
                        login = "Evault-DBA";
                        team = 29;
                    }
                }

                











                foreach (DataGridViewRow row in rows)
                {
                    Guid agentID = Guid.Empty;

                    string dCustomerName = string.Empty;
                    string groupName = string.Empty;
                    string agent = string.Empty;
                    string task = string.Empty;
                    string taskID = string.Empty;
                    string lbr = string.Empty;
                    string lsd = string.Empty;
                    string lsbd = string.Empty;
                    string lsbt = string.Empty;
                    string lvsbt = string.Empty;
                    string lvsbd = string.Empty;
                    string lpbt = string.Empty;
                    string lbt = string.Empty;
                    string lpd = string.Empty;

                    double dslsb = double.MaxValue;
                    double dslvsb = double.MaxValue;
                    double dslpb = double.MaxValue;
                    double dslp = double.MaxValue;

                    bool agentOnline = false;

                    string summary = string.Empty;
                    string description = string.Empty;
                    string errorsWarnings = string.Empty;
                    string duplicateCheckCompare = string.Empty;
                    string exactDuplicateCheckAndClosedCompare = string.Empty;
                    string dupeAgent = string.Empty;





                    dCustomerName = row.Cells["CustomerName"].Value.ToString();
                    lsd = ((DateTime)row.Cells["LastSafesetDate"].Value).ToShortDateString();
                    task = row.Cells["TaskName"].Value.ToString();
                    agent = row.Cells["ComputerName"].Value.ToString();

                    if (isBoth)
                    {
                        agentID = Guid.Parse(row.Cells["PComputerID"].Value.ToString());
                        groupName = row.Cells["GroupName"].Value.ToString();   
                        taskID = row.Cells["PTaskID"].Value.ToString();
                        lbr = row.Cells["LastBackupResult"].Value.ToString();
                        
                        lsbd = ((DateTime)row.Cells["LastSuccessfulBackupTime"].Value).ToShortDateString();
                        lsbt = row.Cells["LastSuccessfulBackupTime"].Value.ToString();
                        lvsbt = row.Cells["LastVerySuccessfulBackupTime"].Value.ToString();
                        lvsbd = ((DateTime)row.Cells["LastVerySuccessfulBackupTime"].Value).ToShortDateString();
                        lpbt = row.Cells["LastPerfectBackupTime"].Value.ToString();
                        lbt = row.Cells["LastBackupTime"].Value.ToString();
                        lpd = ((DateTime)row.Cells["LastPresent"].Value).ToShortDateString();

                        dslp = (double)row.Cells["DaysSinceLastPresent"].Value; 
                        dslsb = (double)row.Cells["DaysSinceLastSuccessfulBackup"].Value;
                        dslvsb = (double)row.Cells["DaysSinceLastVerySuccessfulBackup"].Value;
                        dslpb = (double)row.Cells["DaysSinceLastPerfectBackup"].Value;

                        agentOnline = row.Cells["AgentOnline"].Value.ToString() == "True" ? true : false;
                    }
                    else if (isD)
                    {    
                        taskID = row.Cells["DTaskID"].Value.ToString();
                    }



                    










                    if (isBoth)
                    {
                        if (agentOnline)
                        {
                            description = "Last Backup Result:\t\t\t\t" + lbr + "\r\n" +
                               "Last Backup Time:\t\t\t\t" + lbt + "\r\n" +
                               "Last Successful Backup Time:\t\t" + lsbt + "\r\n" +
                               "Last Very Successful Backup Time:\t" + lvsbt + "\r\n" +
                               "Last Perfect Backup Time:\t\t" + lpbt + "\r\n" +
                               "Last Safeset Date:\t\t\t\t" + lsd + "\r\n" +
                               "\r\n" +
                               "Days Since Last Successful Backup:\t\t" + dslsb.ToString(".00") + "\r\n" +
                               "Days Since Last Very Successful Backup:\t" + dslvsb.ToString(".00") + "\r\n" +
                               "Days Since Last Perfect Backup:\t\t\t" + dslpb.ToString(".00") + "\r\n";

                            dupeAgent = agent;

                            summary = agent + "\\" + task + "  LBR: " + lbr + "  LSBD: " + lsbd + "  LSD: " + lsd + "  LVSBD: " + lvsbd;

                            if (!string.IsNullOrEmpty(defaultCompanyID))
                            {
                                summary = groupName + "\\" + agent + "\\" + task + "  LBR: " + lbr + "  LSBD: " + lsbd + "  LSD: " + lsd + "  LVSBD: " + lvsbd;

                                dupeAgent = groupName.Replace("'", "%") + "%" + agent;

                                dCustomerName = defaultCompanyID;
                            }

                            /* Medium Priority duplicate check
                         
                                a = agent in summary
                                b = job in summary
                                c = lbr in summary
                                d = lsbd in summary
                                e = lsd in summary
                                f = lvsbd in summary
                    
                                duplicate check:
                                a*b*c*d + a*b*c*e + a*b*c*f

                                reduces to:
                                a*b*c(d + e + f) 
                             */

                            string aANDb = "Summary like '%" + dupeAgent + "%" + task.Replace("'", "%") + "%'";
                            string c = "Summary like '%LBR: " + lbr + "%'";
                            string d = "Summary like '%LSBD: " + lsbd + "%'";
                            string e = "Summary like '%LSD: " + lsd + "%'";
                            string f = "Summary like '%LVSBD: " + lvsbd + "%'";
                            string g = "(StatusName != 'Completed' AND StatusName != 'Completed~' AND StatusName != 'Closed' AND StatusName != 'Closed-Silent' AND StatusName != 'Pending Closure~')";

                            duplicateCheckCompare = "(" + aANDb + " AND " + g + ")";

                            if (!string.IsNullOrEmpty(priority))
                            {
                                if (priority.Contains("No SLA"))
                                {
                                    duplicateCheckCompare = aANDb + " AND " + c + " AND (" + d + " OR " + e + " OR " + f +") AND " + g;
                                }
                            }

                            //exactDuplicateCheckAndClosedCompare = "(" + aANDb + " AND " + c + " AND " + d + " AND " + e + " AND " + f +")";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(defaultCompanyID))
                            {
                                dCustomerName = defaultCompanyID;
                            }

                            summary = groupName + "\\" + agent + "_" + agentID.ToString().Substring(0, 3) + " OFFLINE Since: " + lpd;

                            description = "Agent is offline.";

                            duplicateCheckCompare = "Summary like '%" + groupName.Replace("'", "%") + "%" + agent + "_" + agentID.ToString().Substring(0, 3) + " OFFLINE Since: " + lpd + "%'";
                        }
                    }
                    else if (isD)
                    {
                        description = "Last Safeset Date:\t\t\t\t" + lsd + "\r\n";

                        dupeAgent = agent;

                        summary = agent + "\\" + task + "  LSD: " + lsd;

                        if (!string.IsNullOrEmpty(defaultCompanyID))
                        {
                            summary = dCustomerName + "\\" + agent + "\\" + task + "  LSD: " + lsd;

                            dupeAgent = dCustomerName.Replace("'", "%") + "%" + agent;

                            dCustomerName = defaultCompanyID;
                        }

                        /* Medium Priority duplicate check
                         
                            a = agent in summary
                            b = job in summary
                            c = lsd in summary
                    
                            duplicate check:
                            a*b*c

                            reduces to:
                            a*b*c
                         */

                        string aANDb = "Summary like '%" + dupeAgent + "%" + task.Replace("'", "%") + "%'";
                        string c = "Summary like '%LSD: " + lsd + "%'";
                        string f = "(StatusName != 'Completed' AND StatusName != 'Completed~' AND StatusName != 'Closed' AND StatusName != 'Closed-Silent' AND StatusName != 'Pending Closure~')";

                        duplicateCheckCompare = "(" + aANDb + " AND " + f + ")";

                        if (!string.IsNullOrEmpty(priority))
                        {
                            if (priority.Contains("No SLA"))
                            {
                                duplicateCheckCompare = aANDb + " AND " + c + " AND " + f;
                            }
                        }

                        //exactDuplicateCheckAndClosedCompare = "(" + aANDb + " AND " + c + ")";
                    }













                    string[] logs = null;

                    if (isBoth && agentOnline)
                    {
                        try
                        {
                            logs = getAgentJobLogMessages(agentID, taskID, JobLogFileType.Backup, m_uPortalUser.Id, "newest", LogFilterType.Errors_Warnings);

                            int logLines = 0;

                            if (logs != null)
                            {
                                logLines = logs.Count();
                            }

                            if (logLines > 0)
                            {
                                errorsWarnings += "\r\nLast Backup's Errors/Warnings:\r\n\r\n";

                                int j = 0;

                                if (logLines > _maxTicketErrorLines)
                                {
                                    j = logLines - _maxTicketErrorLines;
                                }

                                for (int i = j; i < logLines; i++)
                                {
                                    errorsWarnings += logs[i] + "\r\n";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (m_formShown)
                            {
                                MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                            }

                            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                        }

                        description += errorsWarnings;
                    }










                    if (!string.IsNullOrEmpty(summary))
                    {
                        this.Invoke(new MethodInvoker(() =>
                        {
                            try
                            {
                                int ticketID = 0;

                                using (CWManipulator ticketMaker = new CWManipulator(m_cwCompany, login, m_cwPassword, m_cwSite))
                                {
                                    ticketID = ticketMaker.CreateTicket(dCustomerName, summary, description, board, serviceType, team, duplicateCheckCompare,
                                        null, priority);
                                }

                                string comp = string.IsNullOrEmpty(dCustomerName) ? "CatchAll" : dCustomerName;

                                string textBoxText = "<font color=red>Ticket ID: " + ticketID.ToString() + "  Summary: " + comp + "\\" + summary + " - " + priority + "</font>";

                                switch (ticketID)
                                {
                                    case -1:
                                        {
                                            textBoxText = "No New Ticket. DupeCheck on Summary: " + comp + "\\" + summary + " - " + priority;
                                        }
                                        break;
                                    case -2:
                                        {
                                            textBoxText = "UNDEFINED RETVAL: -2";
                                        }
                                        break;
                                    case -3:
                                        {
                                            textBoxText = dCustomerName + " is in a status that does not allow creating Service Tickets.";
                                        }
                                        break;
                                    default:
                                        break;
                                }

                                textBox1.Text += textBoxText + "\r\n";
                            }
                            catch (Exception ex)
                            {
                                if (m_formShown)
                                {
                                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                                }

                                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                            }
                        }));
                    }
                }
            }
        }

        private void getMegaTasksDetailsTable(string query)
        {
            if (m_dtMegaTasksDetails == null)
            {
                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(()=> textBox1.Text += DateTime.Now.ToLongTimeString() + "\tStart\r\n"));
                    this.Invoke(new MethodInvoker(()=> textBox1.Update()));
                }

                if (m_dtDirectorCustomerDetails == null)
                {
                    getDirectorCustomerDetailsTable(null);
                }

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(()=> textBox1.Text += DateTime.Now.ToLongTimeString() + "\tFinished Getting Director Customer Details\r\n"));
                    this.Invoke(new MethodInvoker(()=> textBox1.Update()));
                }

                if (m_dtDirectorTasksDetails == null)
                {
                    getDirectorTasksDetailsTable(null);
                }

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(()=> textBox1.Text += DateTime.Now.ToLongTimeString() + "\tFinished Getting Director Tasks Details\r\n"));
                    this.Invoke(new MethodInvoker(()=> textBox1.Update()));
                }

                if (m_dtPortalTasksDetails == null)
                {
                    getPortalTasksDetailsTable();
                }

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(()=> textBox1.Text += DateTime.Now.ToLongTimeString() + "\tFinished Getting Portal Tasks Details\r\n"));
                    this.Invoke(new MethodInvoker(()=> textBox1.Update()));
                }

                m_dtMegaTasksDetails = new DataTable();

                m_dtMegaTasksDetails.Columns.Add("GroupName");
                m_dtMegaTasksDetails.Columns.Add("ComputerName");
                m_dtMegaTasksDetails.Columns.Add("AgentVersion");
                m_dtMegaTasksDetails.Columns.Add("OSVersion");
                m_dtMegaTasksDetails.Columns.Add("TaskName");
                m_dtMegaTasksDetails.Columns.Add("LastBackupResult");
                m_dtMegaTasksDetails.Columns.Add("LastBackupTime", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("LastSafesetDate", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("ScheduleRetention");
                m_dtMegaTasksDetails.Columns.Add("LastSuccessfulBackupTime", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("LastVerySuccessfulBackupTime", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("LastPerfectBackupTime", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("AgentOnline");
                m_dtMegaTasksDetails.Columns.Add("AccountID");
                m_dtMegaTasksDetails.Columns.Add("PCompanyName");
                m_dtMegaTasksDetails.Columns.Add("LocationName");
                m_dtMegaTasksDetails.Columns.Add("SatelliteGUID");
                m_dtMegaTasksDetails.Columns.Add("SatelliteOperatingMode", typeof(SatelliteOperatingMode));
                m_dtMegaTasksDetails.Columns.Add("TaskOperatingMode", typeof(OperatingModeType));
                m_dtMegaTasksDetails.Columns.Add("DaysSinceLastSuccessfulBackup", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("DaysSinceLastVerySuccessfulBackup", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("DaysSinceLastPerfectBackup", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("DaysSinceLastBackup", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("DaysSinceLastSafeset", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("NativeSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("PoolSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("CompressionRatio", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("UsedPoolSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("DeltaCompressedSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("OriginalSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("StorageSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("TotalCompressedSize", typeof(long));
                m_dtMegaTasksDetails.Columns.Add("NumberOfSafesets", typeof(int));
                m_dtMegaTasksDetails.Columns.Add("FirstSafesetDate", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("MailServer");
                m_dtMegaTasksDetails.Columns.Add("MailServerPort");
                m_dtMegaTasksDetails.Columns.Add("MailServerUsername");
                m_dtMegaTasksDetails.Columns.Add("MailServerPassword");
                m_dtMegaTasksDetails.Columns.Add("MailServerDomain");
                m_dtMegaTasksDetails.Columns.Add("MailServerRecipients");
                m_dtMegaTasksDetails.Columns.Add("MailFrom");
                m_dtMegaTasksDetails.Columns.Add("MailOnError");
                m_dtMegaTasksDetails.Columns.Add("MailOnFailure");
                m_dtMegaTasksDetails.Columns.Add("MailOnSuccess");
                m_dtMegaTasksDetails.Columns.Add("CustomerID", typeof(uint));
                m_dtMegaTasksDetails.Columns.Add("CustomerName");
                m_dtMegaTasksDetails.Columns.Add("PCompanyID");
                m_dtMegaTasksDetails.Columns.Add("DLocationID", typeof(uint));
                m_dtMegaTasksDetails.Columns.Add("PComputerID");
                m_dtMegaTasksDetails.Columns.Add("DComputerID", typeof(uint));
                m_dtMegaTasksDetails.Columns.Add("PTaskID");
                m_dtMegaTasksDetails.Columns.Add("DTaskID", typeof(uint));
                m_dtMegaTasksDetails.Columns.Add("TaskType");
                m_dtMegaTasksDetails.Columns.Add("LastPresent", typeof(DateTime));
                m_dtMegaTasksDetails.Columns.Add("DaysSinceLastPresent", typeof(double));
                m_dtMegaTasksDetails.Columns.Add("AgentConfiguration", typeof(AgentConfiguration));
                m_dtMegaTasksDetails.Columns.Add("JobConfiguration", typeof(JobConfiguration));
                m_dtMegaTasksDetails.Columns.Add("AgentVaultRegistrations", typeof(object));
                m_dtMegaTasksDetails.Columns.Add("PReqStatus");
                m_dtMegaTasksDetails.Columns.Add("PReqMessage");

                m_dtMegaTasksDetails.AcceptChanges();

                DataTable tempTable = new DataTable();
                tempTable = m_dtMegaTasksDetails.Clone();
               
                if ((m_dtPortalTasksDetails != null && m_dtPortalTasksDetails.Rows.Count > 0) &&
                    (m_dtDirectorCustomerDetails != null && m_dtDirectorCustomerDetails.Rows.Count > 0))
                {
                    var result = from p in m_dtPortalTasksDetails.AsEnumerable()
                                 join d in m_dtDirectorTasksDetails.AsEnumerable()
                                 on p.Field<string>("pKey") equals d.Field<string>("pKey") into lj
                                 from subd in lj.DefaultIfEmpty()

                                 select tempTable.LoadDataRow(new object[]
                                    {
                                        p.Field<string>("GroupName"),
                                        p.Field<string>("ComputerName"),
                                        p.Field<string>("AgentVersion"),
                                        p.Field<string>("OSVersion"),
                                        p.Field<string>("TaskName"),
                                        p.Field<string>("LastBackupResult"),
                                        p.Field<DateTime>("LastBackupTime"),
                                        subd == null ? DateTime.MinValue : subd.Field<DateTime>("LastSafesetDate"),
                                        p.Field<string>("ScheduleRetention"),
                                        p.Field<DateTime>("LastSuccessfulBackupTime"),
                                        p.Field<DateTime>("LastVerySuccessfulBackupTime"),
                                        p.Field<DateTime>("LastPerfectBackupTime"),
                                        p.Field<string>("AgentOnline"),
                                        p.Field<string>("AccountID"),
                                        p.Field<string>("PCompanyName"),
                                        subd == null ? string.Empty : subd.Field<string>("LocationName"),
                                        subd == null ? string.Empty : subd.Field<string>("SatelliteGUID"),
                                        subd == null ? SatelliteOperatingMode.ReplicateCustomerOnly : subd.Field<SatelliteOperatingMode>("SatelliteOperatingMode"),
                                        subd == null ? OperatingModeType.Default : subd.Field<OperatingModeType>("TaskOperatingMode"),
                                        p.Field<double>("DaysSinceLastSuccessfulBackup"),
                                        p.Field<double>("DaysSinceLastVerySuccessfulBackup"),
                                        p.Field<double>("DaysSinceLastPerfectBackup"),
                                        p.Field<double>("DaysSinceLastBackup"),
                                        subd == null ? -1.0 : subd.Field<double>("DaysSinceLastSafeset"),
                                        subd == null ? -1 : subd.Field<long>("NativeSize"),
                                        subd == null ? -1 : subd.Field<long>("PoolSize"),
                                        subd == null ? -1.0 : subd.Field<double>("CompressionRatio"),
                                        subd == null ? -1 : subd.Field<long>("UsedPoolSize"),
                                        subd == null ? -1 : subd.Field<long>("DeltaCompressedSize"),
                                        subd == null ? -1 : subd.Field<long>("OriginalSize"),
                                        subd == null ? -1 : subd.Field<long>("StorageSize"),
                                        subd == null ? -1 : subd.Field<long>("TotalCompressedSize"),
                                        subd == null ? -1 : subd.Field<int>("NumberOfSafesets"),
                                        subd == null ? DateTime.MinValue : subd.Field<DateTime>("FirstSafesetDate"),
                                        p.Field<string>("MailServer"),
                                        p.Field<string>("MailServerPort"),
                                        p.Field<string>("MailServerUsername"),
                                        p.Field<string>("MailServerPassword"),
                                        p.Field<string>("MailServerDomain"),
                                        p.Field<string>("MailServerRecipients"),
                                        p.Field<string>("MailFrom"),
                                        p.Field<string>("MailOnError"),
                                        p.Field<string>("MailOnFailure"),
                                        p.Field<string>("MailOnSuccess"),
                                        subd == null ? 0 : subd.Field<uint>("CustomerID"),
                                        subd == null ? string.Empty : subd.Field<string>("CustomerName"),
                                        p.Field<string>("PCompanyID"),
                                        subd == null ? 0 : subd.Field<uint>("LocationID"),
                                        p.Field<string>("PComputerID"),
                                        subd == null ?  0 : subd.Field<uint>("DComputerID"),
                                        p.Field<string>("PTaskID"),
                                        subd == null ? 0 : subd.Field<uint>("DTaskID"),
                                        p.Field<string>("TaskType"),
                                        p.Field<DateTime>("LastPresent"),
                                        p.Field<double>("DaysSinceLastPresent"),
                                        p.Field<AgentConfiguration>("AgentConfiguration"),
                                        p.Field<JobConfiguration>("JobConfiguration"),
                                        p.Field<object>("AgentVaultRegistrations"),
                                        p.Field<string>("PReqStatus"),
                                        p.Field<string>("PReqMessage"),
                                    }, false);

                    if (result != null && result.Count() > 0)
                    {
                        result.CopyToDataTable(m_dtMegaTasksDetails, LoadOption.OverwriteChanges);
                    }

                    m_megaTaskGenerated = DateTime.Now;
                }
            }

            string filter = query;
            string sort = "GroupName ASC, ComputerName ASC, TaskName ASC";

            if (string.IsNullOrEmpty(filter))
            {
                filter = textBoxQuery.Text;
            }

            DataRow[] rows = null;

            try
            {
                if (m_dtMegaTasksDetails.Rows.Count > 0)
                {
                    rows = m_dtMegaTasksDetails.Select(filter, sort);
                }
            }
            catch (Exception ex) { System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error); }

            if (m_dtFilteredMegaTasksDetails != null)
            {
                m_dtFilteredMegaTasksDetails.Dispose();
                m_dtFilteredMegaTasksDetails = null;
            }

            m_dtFilteredMegaTasksDetails = new DataTable();

            foreach (DataColumn dc in m_dtMegaTasksDetails.Columns)
            {
                m_dtFilteredMegaTasksDetails.Columns.Add(dc.ColumnName, dc.DataType);    
            }

            m_dtFilteredMegaTasksDetails.AcceptChanges();

            if (rows != null && rows.Count() > 0)
            {
                foreach (DataRow dr in rows)
                {
                    m_dtFilteredMegaTasksDetails.ImportRow(dr);
                }

                m_dtFilteredMegaTasksDetails.AcceptChanges();

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(() => groupBoxDataGrid.Text = rows.Count().ToString() + " results as of: " + DateTime.Now.ToString()));
                }
            }

            if (m_formShown)
            {
                this.Invoke(new MethodInvoker(() => dataGridViewResults.DataSource = m_dtFilteredMegaTasksDetails));
            }
            else
            {
                dataGridViewResults.DataSource = m_dtFilteredMegaTasksDetails;
            }
        }

        private string generateQuery()
        {
            string query = "";
            string tempString = "";
            bool tempBool = false;

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveLastBackup.Checked));

            if (tempBool)
            {
                query += "LastBackupTime ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveLastBackup.Text));

                switch (tempString)
                {
                    case "before":
                        {
                            query += "<";
                        }
                        break;
                    case "after":
                        {
                            query += ">";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> query += " '" + dateTimePickerLastBackup.Text + "'"));
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxThatChecklisted.Checked));

            if (tempBool)
            {
                string subQuery = "(";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxAreChecklisted.Text));

                string oper = "AND";
                string comparer = "<>";

                if (tempString == "in")
                {
                    oper = "OR";
                    comparer = "=";
                }

                this.Invoke(new MethodInvoker(()=> 
                    {
                        foreach (NameAndID n in checkedListBoxQuery.Items)
	                    {
                            if (n.Type == "Task")
                            {
                                subQuery += "PTaskID " + comparer + " '" + n.ID + "'";
                            }
                            else if (n.Type == "Group")
                            {
                                subQuery += "GroupName " + comparer + " '" + n.Name.Replace("'", "''") +"'";
                            }
                            else if (n.Type == "Site")
                            {
                                subQuery += "PCompanyID " + comparer + " '" + n.ID + "'";
                            }
                            else if (n.Type == "Agent")
                            {
                                subQuery += "PComputerID " + comparer + " '" + n.ID + "'";
                            }
                            else if (n.Type == "DSS")
                            {
                                subQuery += "ADSSystemName " + comparer + " '" + n.Name + "'";
                            }
                            else if (n.Type == "DSCustomer")
                            {
                                subQuery += "ACustomerName " + comparer + " '" + n.Name.Replace("'", "''") + "'";
                            }
                            else if (n.Type == "DSC")
                            {
                                subQuery += "ADSClient " + comparer + " '" + n.ID + "'";
                            }
                            else if (n.Type == "DSCJob")
                            {
                                subQuery += "ASetName " + comparer + " '" + n.Name.Replace("'", "''") + "'";
                            }

                            subQuery += " " + oper + " ";
	                    }
                    }
                ));

                if (subQuery.Length > 1)
                {
                    subQuery = subQuery.Substring(0, subQuery.Length - (2 + oper.Length));
                }

                subQuery += ")";

                if (subQuery.Length > 2)
                {
                    if (!string.IsNullOrEmpty(query))
                        query += " AND ";

                    query += subQuery;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveLastSuccessfulBackup.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "LastSuccessfulBackupTime ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveLastSuccessfulBackup.Text));

                switch (tempString)
                {
                    case "before":
                        {
                            query += "<";
                        }
                        break;
                    case "after":
                        {
                            query += ">";
                        }
                        break;
                    default:
                        break;
                }
                
                this.Invoke(new MethodInvoker(()=> query += " '" + dateTimePickerLastSuccessfulBackup.Text + "'"));
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveLastSafeset.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "LastSafesetDate ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveLastSafeset.Text));

                switch (tempString)
                {
                    case "before":
                        {
                            query += "<";
                        }
                        break;
                    case "after":
                        {
                            query += ">";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> query += " '" + dateTimePickerLastSafeset.Text + "'"));
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveLastBackupResult.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "LastBackupResult ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveLastBackupResultCompare.Text));

                switch (tempString)
                {
                    case "equal to":
                        {
                            query += "=";
                        }
                        break;
                    case "not equal to":
                        {
                            query += "<>";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> query += " '" + comboBoxHaveLastBackupResult.Text + "'"));
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveMailServer.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "MailServer ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveMailServer.Text));

                switch (tempString)
                {
                    case "not specified":
                        {
                            query += "= ''";
                        }
                        break;
                    case "equal to":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "= '" + textBoxHaveMailServer.Text.Trim() + "'"));
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveName.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "TaskName ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveName.Text));

                switch (tempString)
                {
                    case "equal to":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "= '" + textBoxHaveName.Text.Trim() + "'"));
                        }
                        break;
                    case "containing":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "like '%" + textBoxHaveName.Text.Trim() + "%'"));
                        }
                        break;
                    case "not equal to":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "<> '" + textBoxHaveName.Text.Trim() + "'"));
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveSatellite.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "SatelliteGUID <> ''";
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveOperatingMode.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "TaskOperatingMode ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveOperatingModeCompare.Text));

                switch (tempString) // Default = 0
                {                                              // PausedReplication = 2
                    case "equal to":                           // RedirectedBackups = 4
                        {
                            query += "=";
                        }
                        break;
                    case "not equal to":
                        {
                            query += "<>";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveOperatingMode.Text));

                switch (tempString)
                {
                    case "Default":
                        {
                            query += " 0";
                        }
                        break;
                    case "PausedReplication":
                        {
                            query += " 2";
                        }
                        break;
                    case "RedirectedBackups":
                        {
                            query += " 4";
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveSafesetCount.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "NumberOfSafesets ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveSafesetCount.Text));

                switch (tempString)
                {
                    case "equal to":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "= " + textBoxHaveSafesetCount.Text.Trim()));
                        }
                        break;
                    case "less than":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "< " + textBoxHaveSafesetCount.Text.Trim()));
                        }
                        break;
                    case "greater than":
                        {
                            this.Invoke(new MethodInvoker(()=> query += "> " + textBoxHaveSafesetCount.Text.Trim()));
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveAgentOnlineStatus.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "AgentOnline ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveAgentOnlineStatusCompare.Text));

                switch (tempString)
                {
                    case "equal to":
                        {
                            query += "= ";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveAgentOnlineStatus.Text));

                switch (tempString)
                {
                    case "Online":
                        {
                            query += "'True'";
                        }
                        break;
                    case "Offline":
                        {
                            query += "'False'";
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHavePReqStatus.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "PReqStatus ";

                this.Invoke(new MethodInvoker(()=> tempString = checkBoxHavePReqStatus.Text));

                switch (tempString)
                {
                    case "equal to":
                        {
                            query += "= ";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHavePReqStatus.Text));

                switch (tempString)
                {
                    case "Success":
                        {
                            query += "'S'";
                        }
                        break;
                    case "Error":
                        {
                            query += "'E'";
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveSatelliteOperatingMode.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "SatelliteOperatingMode ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveSatelliteOperatingModeCompare.Text));

                switch (tempString) // NormalReplication = 1
                {                                                       // DontReplicate = 2
                    case "equal to":                                    // BypassSatellite = 3
                        {                                               // RestoreOnly = 4
                            query += "=";                               // ReplicateCustomerOnly = 5
                        }
                        break;
                    case "not equal to":
                        {
                            query += "<>";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveSatelliteOperatingMode.Text));

                switch (tempString)
                {
                    case "NormalReplication":
                        {
                            query += " 1";
                        }
                        break;
                    case "DontReplicate":
                        {
                            query += " 2";
                        }
                        break;
                    case "BypassSatellite":
                        {
                            query += " 3";
                        }
                        break;
                    case "RestoreOnly":
                        {
                            query += " 4";
                        }
                        break;
                    case "ReplicateCustomerOnly":
                        {
                            query += " 5";
                        }
                        break;
                    default:
                        break;
                }
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHavePoolSize.Checked));

            if (tempBool)
            {
                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                query += "PoolSize ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHavePoolSize.Text));

                switch (tempString)
                {
                    case "greater than":
                        {
                            query += ">";
                        }
                        break;
                    case "less than":
                        {
                            query += "<";
                        }
                        break;
                    default:
                        break;
                }

                double jigs = 0.0;

                try
                {
                    this.Invoke(new MethodInvoker(()=> jigs = Convert.ToDouble(textBoxHavePoolSize.Text.Trim())));
                }
                catch (Exception ex) { System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error); }               
                
                long bytes = (long)(jigs * 1024 * 1024 * 1024);
                
                query += " " + bytes.ToString();
            }

            this.Invoke(new MethodInvoker(()=> tempBool = checkBoxHaveWeeklySchedule.Checked));

            if (tempBool)
            {
                string weeklyQuery = "";

                if (!string.IsNullOrEmpty(query))
                    query += " AND ";

                string weeklyMatch = "ScheduleRetention ";

                this.Invoke(new MethodInvoker(()=> tempString = comboBoxHaveWeeklySchedule.Text));

                switch (tempString)
                {
                    case "including":
                        {
                            weeklyMatch += "like ";
                        }
                        break;
                    case "not including":
                        {
                            weeklyMatch += "not like ";
                        }
                        break;
                    default:
                        break;
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxSunday.Checked));

                if (tempBool)
                {
                    weeklyQuery = weeklyMatch + "'%Sun%'";
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxMonday.Checked));

                if (tempBool)
                {
                    if (!string.IsNullOrEmpty(weeklyQuery))
                        weeklyQuery += " AND ";

                    weeklyQuery += "((" + weeklyMatch + "'%Mon,%') OR (" + weeklyMatch + "'%Mon#%'))";
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxTuesday.Checked));

                if (tempBool)
                {
                    if (!string.IsNullOrEmpty(weeklyQuery))
                        weeklyQuery += " AND ";

                    weeklyQuery += weeklyMatch + "'%Tue%'";
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxWednesday.Checked));

                if (tempBool)
                {
                    if (!string.IsNullOrEmpty(weeklyQuery))
                        weeklyQuery += " AND ";

                    weeklyQuery += weeklyMatch + "'%Wed%'";
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxThursday.Checked));

                if (tempBool)
                {
                    if (!string.IsNullOrEmpty(weeklyQuery))
                        weeklyQuery += " AND ";

                    weeklyQuery += weeklyMatch + "'%Thu%'";
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxFriday.Checked));

                if (tempBool)
                {
                    if (!string.IsNullOrEmpty(weeklyQuery))
                        weeklyQuery += " AND ";

                    weeklyQuery += weeklyMatch + "'%Fri%'";
                }

                this.Invoke(new MethodInvoker(()=> tempBool = checkBoxSaturday.Checked));

                if (tempBool)
                {
                    if (!string.IsNullOrEmpty(weeklyQuery))
                        weeklyQuery += " AND ";

                    weeklyQuery += weeklyMatch + "'%Sat%'";
                }

                query += weeklyQuery;
            }

            return query;
        }

        private void cleanUpQueryTool()
        {
            checkBoxHaveLastBackup.Checked = false;
            checkBoxHaveLastBackupResult.Checked = false;
            checkBoxHaveLastSuccessfulBackup.Checked = false;
            checkBoxHaveMailServer.Checked = false;
            checkBoxHaveName.Checked = false;
            checkBoxHaveOperatingMode.Checked = false;
            checkBoxHavePoolSize.Checked = false;
            checkBoxHaveSatellite.Checked = false;
            checkBoxHaveSatelliteOperatingMode.Checked = false;
            checkBoxHaveWeeklySchedule.Checked = false;
            checkBoxRestoreLogs.Checked = false;
            checkBoxBackupLogs.Checked = false;
            checkBoxErrorsOnly.Checked = false;
            checkBoxErrorsWarningsOnly.Checked = false;
            checkBoxLimitLogs.Checked = false;
            checkBoxHaveAgentOnlineStatus.Checked = false;
            checkBoxHaveSafesetCount.Checked = false;
            checkBoxHavePReqStatus.Checked = false;
            checkBoxThatChecklisted.Checked = false;
            checkBoxHaveLastSafeset.Checked = false;
        }

        private void getTasksLogs(string logType, string logFilter, string numberOfLines)
        {
            if (m_dtFilteredMegaTasksDetails != null && m_dtFilteredMegaTasksDetails.Rows != null && m_dtFilteredMegaTasksDetails.Rows.Count > 0)
            {
                int numLines = 0;
                LogFilterType filterType = LogFilterType.None;
                JobLogFileType jLogType = JobLogFileType.All;

                try
                {
                    switch (numberOfLines)
                    {
                        case "All":
                            {
                                numLines = 0;
                            }
                            break;
                        default:
                            {
                                if (checkBoxLimitLogs.Checked)
                                {
                                    numLines = Convert.ToInt32(textBoxLimitLogs.Text.Trim());
                                }
                                else
                                {
                                    numLines = Convert.ToInt32(numberOfLines);
                                }
                            }
                            break;
                    }

                    switch (logFilter)
                    {
                        case "ErrorsWarnings":
                            {
                                filterType = LogFilterType.Errors_Warnings;
                            }
                            break;
                        case "Errors":
                            {
                                filterType = LogFilterType.Errors;
                            }
                            break;
                        case "All":
                            {
                                filterType = LogFilterType.None;
                            }
                            break;
                        default:
                            break;
                    }

                    switch (logType)
                    {
                        case "Backup":
                            {
                                jLogType = JobLogFileType.Backup;
                            }
                            break;
                        case "Restore":
                            {
                                jLogType = JobLogFileType.Restore;
                            }
                            break;
                        default:
                            {
                                jLogType = JobLogFileType.All;
                            }
                            break;
                    }

                    foreach (DataRow row in m_dtFilteredMegaTasksDetails.Rows)
                    {
                        string[] errors = null;

                        try
                        {
                            errors = getAgentJobLogMessages(Guid.Parse(row["PComputerID"].ToString()), row["PTaskID"].ToString(),
                                                jLogType, m_uPortalUser.Id, "newest", filterType);
                        }
                        catch { errors = new string[] {row["ComputerName"].ToString() + "_" + row["TaskName"].ToString() + ": Couldn't get logs"}; }

                        if (errors != null && errors.Count() > 0)
                        {
                            int lineCount = errors.Count();
                     
                            if (numLines > 0 && lineCount > numLines)
                            {
                                for (int i = lineCount - numLines; i < lineCount; i++)
                                {
                                    string toAdd = row["GroupName"].ToString() + "," + row["ComputerName"].ToString() + "," +
                                    row["TaskName"].ToString() + ",\"" + errors[i] + "\"";

                                    m_log.Add(toAdd);
                                }
                            }
                            else
                            {
                                foreach (string error in errors)
                                {
                                    string toAdd = row["GroupName"].ToString() + "," + row["ComputerName"].ToString() + "," +
                                            row["TaskName"].ToString() + ",\"" + error + "\"";

                                    m_log.Add(toAdd);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void configureAgents()
        {
            Dictionary<string, AgentConfiguration> agentConfigs = new Dictionary<string, AgentConfiguration>();

            if (m_dtFilteredMegaTasksDetails != null && m_dtFilteredMegaTasksDetails.Rows != null && m_dtFilteredMegaTasksDetails.Rows.Count > 0)
            {
                foreach (DataRow dr in m_dtFilteredMegaTasksDetails.Rows)
                {
                    string agent = dr["GroupName"].ToString() + "><._.><" + dr["ComputerName"].ToString() + "><._.><" + dr["PComputerID"].ToString();

                    if (!agentConfigs.ContainsKey(agent) && dr["AgentConfiguration"] != DBNull.Value)
                    {
                        agentConfigs.Add(agent, (AgentConfiguration)dr["AgentConfiguration"]);
                    }
                }

                string warning = "The following Agents are pending updates:\r\n";
                string settings = "";

                foreach (string a in agentConfigs.Keys)
                {
                    warning += "\t" + a + "\r\n";
                    settings = "";

                    if (checkBoxAgentConfigMailServer.Checked)
                    {
                        settings += "\tNotification Mail Server = " + textBoxAgentConfigMailServer.Text.Trim() + "\r\n";

                        agentConfigs[a].MailServer = textBoxAgentConfigMailServer.Text.Trim();
                    }

                    if (checkBoxAgentConfigMailServerPort.Checked)
                    {
                        settings += "\tNotification Mail Server Port = " + textBoxAgentConfigMailServerPort.Text.Trim() + "\r\n";

                        agentConfigs[a].MailServerPort = Convert.ToUInt32(textBoxAgentConfigMailServerPort.Text.Trim());
                    }

                    if (checkBoxAgentMailServerUsername.Checked)
                    {
                        settings += "\tMail Server Username = " + textBoxcheckBoxAgentMailServerUsername.Text.Trim() + "\r\n";

                        agentConfigs[a].MailServerUserName = textBoxcheckBoxAgentMailServerUsername.Text.Trim();
                    }
                    
                    if (checkBoxAgentMailServerPassword.Checked)
                    {
                        settings += "\tMail Server Password = " + textBoxAgentMailServerPassword.Text.Trim() + "\r\n";

                        agentConfigs[a].MailServerPassword = textBoxAgentMailServerPassword.Text.Trim();
                    }

                    if (checkBoxAgentMailServerDomain.Checked)
                    {
                        settings += "\tMail Server Domain = " + textBoxAgentMailServerDomain.Text.Trim() + "\r\n";

                        agentConfigs[a].MailServerDomain = textBoxAgentMailServerDomain.Text.Trim();
                    }

                    if (checkBoxAgentMailServerFromAddress.Checked)
                    {
                        settings += "\tMail Server From Address = " + textBoxAgentMailServerFromAddress.Text.Trim() + "\r\n";

                        agentConfigs[a].FromAddress = textBoxAgentMailServerFromAddress.Text.Trim();
                    }

                    if (checkBoxAgentConfigRecipients.Checked)
                    {
                        string recipients = textBoxAgentConfigRecipients.Text.Trim();

                        settings += "\tRecipient Addresses  = " + textBoxAgentConfigRecipients.Text.Trim() + "\r\n";

                        agentConfigs[a].RecipientAddresses = recipients.Split(new char[] { ';', ',' });
                    }

                    if (checkBoxAgentConfigNotificationsSuccess.Checked)
                    {
                        settings += "\tNotification on Success = ";

                        if (radioButtonSuccessOn.Checked)
                        {
                            agentConfigs[a].MailOnSuccess = true;

                            settings += "true\r\n";
                        }
                        else
                        {
                            agentConfigs[a].MailOnSuccess = false;

                            settings += "false\r\n";
                        }
                    }

                    if (checkBoxAgentConfigNotificationsError.Checked)
                    {
                        settings += "\tNotification on Error = ";

                        if (radioButtonErrorsOn.Checked)
                        {
                            agentConfigs[a].MailOnError = true;

                            settings += "true\r\n";
                        }
                        else
                        {
                            agentConfigs[a].MailOnError = false;

                            settings += "false\r\n";
                        }
                    }

                    if (checkBoxAgentConfigNotificationsFailure.Checked)
                    {
                        settings += "\tNotification on Failure = ";

                        if (radioButtonFailuresOn.Checked)
                        {
                            agentConfigs[a].MailOnFailure = true;

                            settings += "true\r\n"; 
                        }
                        else
                        {
                            agentConfigs[a].MailOnFailure = false;

                            settings += "false\r\n";
                        }
                    }

                    if (agentConfigs[a].MailServerPort == 0)
                    {
                        agentConfigs[a].MailServerPort = 25;
                    }
                }

                warning += "\r\nwith the following settings:\r\n" + settings;

                DialogResult result = MessageBox.Show("Are you sure about this update?\r\n\r\n" + warning, "Update Warning", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    Thread.Sleep(2000);

                    result = MessageBox.Show("Are you reeaaaallllyyy sure about this update??\r\n\r\n" + warning, "Update Warning", MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        string agentsNotUpdated = "";

                        foreach (string a in agentConfigs.Keys)
                        {
                            string[] s = a.Split(new string[] { "><._.><" }, StringSplitOptions.None);
                            string agentID = s[2];

                            try
                            {
                                m_sacApi.SaveAgentConfiguration(Guid.Parse(agentID), agentConfigs[a], m_uPortalUser.Id);
                            }
                            catch (Exception ex)
                            {
                                agentsNotUpdated += "\t" + a + "\r\n";

                                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                            }
                        }

                        if (agentsNotUpdated == "")
                        {
                            MessageBox.Show("All Agents updated successfully.");
                        }
                        else
                        {
                            MessageBox.Show("All Agents updated successfully, except the following:\r\n" + agentsNotUpdated);
                        }
                    }
                }
            }
        }

        private void cleanUpAgentConfigTool()
        {
            checkBoxAgentConfigMailServer.Checked = false;
            checkBoxAgentConfigNotificationsError.Checked = false;
            checkBoxAgentConfigNotificationsFailure.Checked = false;
            checkBoxAgentConfigNotificationsSuccess.Checked = false;
            checkBoxAgentConfigRecipients.Checked = false;
            checkBoxAgentMailServerUsername.Checked = false;
            checkBoxAgentMailServerDomain.Checked = false;
            checkBoxAgentMailServerFromAddress.Checked = false;
            checkBoxAgentMailServerPassword.Checked = false;
            checkBoxAgentConfigMailServerPort.Checked = false;
        }

        private void handleHiddenRequest(string[] args)
        {
            string configFile = args[0];

            args = null;

            args = File.ReadAllLines(configFile);

            string doWhat = args[0];

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN handleHiddenRequest:" + doWhat, EventLogEntryType.Information);

            switch (doWhat)
            {
                case "ReportEmailer":
                    {
                        reportEmailer(args);
                    }
                    break;
                case "GroupBIEmailer":
                    {
                        groupBIEmailer(args);
                    }
                    break;
                case "ReportCWTicketMaker":
                    {
                        reportCWTicketMaker(args);
                    }
                    break;
                case "ReportDirectorCWTicketMaker":
                    {
                        reportDirectorCWTicketMaker(args);
                    }
                    break;
                case "AsigraCWTicketMaker":
                    {
                        asigraCWTicketMaker(args);
                    }
                    break;
                case "VeeamCWTicketMaker":
                    {
                        veeamCWTicketMaker(args);
                    }
                    break;
                case "BaseVaultsUsageEmailer":
                    {
                        baseVaultsUsageEmailer(args);
                    }
                    break;
                case "PoolQuotaEmailer":
                    {
                        poolQuotaEmailer(args);
                    }
                    break;
                case "PoolLocationEmailer":
                    {
                        poolLocationEmailer(args);
                    }
                    break;
                case "MaintenanceMonitorEmailer":
                    {
                        maintenanceMonitorEmailer(args);
                    }
                    break;
                default:
                    {
                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

                        Application.Exit();
                    }
                    break;
            }
        }

        private void reportEmailer(string[] args)
        {
            BackgroundWorker reportEmailerBackground = new BackgroundWorker();
            reportEmailerBackground.DoWork += reportEmailerBackground_DoWork;
            reportEmailerBackground.RunWorkerCompleted += reportEmailerBackground_RunWorkerCompleted;

            reportEmailerBackground.RunWorkerAsync(args);
        }

        private void groupBIEmailer(string[] args)
        {
            try
            {
                textBoxPortalUsername.Text = args[1];
                textBoxPortalPassword.Text = args[2];

                string groupName = args[3];

                string recipientEmails = args[4];
                string fromEmail = args[5];
                string smtpServer = args[6];
                string smtpDomain = args[7];
                string smtpUsername = args[8];
                string smtpPassword = args[9];

                int smtpPort = Convert.ToInt32(args[10]);

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN groupBIEmailer:\"" + groupName + "\"", EventLogEntryType.Information);

                portalLogin();

                if (m_loggedIntoPortal)
                {
                    int index = -1;

                    for (int i = 0; i < m_dtGroups.Rows.Count; i++)
                    {
                        if (m_dtGroups.Rows[i]["Name"].ToString().ToLower() == groupName.ToLower())
                        {
                            index = i;
                            break;
                        }
                    }

                    if (index > -1)
                    {
                        comboBoxGroups.SelectedIndex = index;

                        generateGroupBI();

                        string fileName = groupName + "_Backup Items_" + DateTime.Now.Month.ToString() + "." +
                                DateTime.Now.Day.ToString() + "." + DateTime.Now.Year.ToString() + ".pdf";

                        MemoryStream pdfStream = new MemoryStream();

                        if (!string.IsNullOrEmpty(textBox1.Text.Trim()))
                        {
                            Document pdfDoc = createBIDocument(groupName);

                            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always);

                            pdfRenderer.Document = pdfDoc;
                            pdfRenderer.RenderDocument();
                            pdfRenderer.PdfDocument.Save(pdfStream, false);
                        }

                        using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                        {
                            smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                            string body = m_groupBIEmailerBody;

                            using (MailMessage message = new MailMessage())
                            {
                                string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                                foreach (string theto in tos)
                                {
                                    message.To.Add(theto);
                                }

                                message.From = new MailAddress(fromEmail);
                                message.Body = body;
                                message.Subject = groupName + " Backup Items Report";

                                Attachment report = new Attachment(pdfStream, fileName);

                                message.Attachments.Add(report);

                                smtp.Send(message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void reportCWTicketMaker(string[] args)
        {
            BackgroundWorker reportCWTicketMakerBackground = new BackgroundWorker();
            reportCWTicketMakerBackground.DoWork += reportCWTicketMakerBackground_DoWork;
            reportCWTicketMakerBackground.RunWorkerCompleted += reportCWTicketMakerBackground_RunWorkerCompleted;

            reportCWTicketMakerBackground.RunWorkerAsync(args);
        }

        private void reportDirectorCWTicketMaker(string[] args)
        {

            BackgroundWorker reportDirectorCWTicketMakerBackground = new BackgroundWorker();
            reportDirectorCWTicketMakerBackground.DoWork += reportDirectorCWTicketMakerBackground_DoWork;
            reportDirectorCWTicketMakerBackground.RunWorkerCompleted += reportDirectorCWTicketMakerBackground_RunWorkerCompleted;

            reportDirectorCWTicketMakerBackground.RunWorkerAsync(args);
        }

        private void baseVaultsUsageEmailer(string[] args)
        {
            try
            {
                textBoxDirectorUsername.Text = args[1];
                textBoxDirectorPassword.Text = args[2];
                textBoxDomain.Text = args[3];
                textBoxWritePath.Text = args[4];
                m_baseVaultsFileName = args[5];

                string recipientEmails = args[6];
                string smtpServer = args[7];
                string smtpDomain = args[8];
                string smtpUsername = args[9];
                string smtpPassword = args[10];

                int smtpPort = Convert.ToInt32(args[11]);

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN baseVaultsUsageEmailer", EventLogEntryType.Information);

                generateBaseVaultsUsageVendor();

                string fileName = "Base Vaults Usage_" + DateTime.Now.Month.ToString() + "." +
                                DateTime.Now.Day.ToString() + "." + DateTime.Now.Year.ToString() + ".csv";

                using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                {
                    smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                    string body = "Greetings,\r\nHere is the Auto-Generated Report.\r\n\r\nThanks,\r\n\r\nDakota Backup";

                    if (textBox1.Text.Contains("Total"))
                    {
                        using (MailMessage message = new MailMessage())
                        {
                            string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string theto in tos)
                            {
                                message.To.Add(theto);
                            }

                            message.From = new MailAddress("donotreply@ktconnections.com");
                            message.Body = body;
                            message.Subject = "Dakota Backup Base Vaults Usage Report";

                            Attachment reportAttachment = Attachment.CreateAttachmentFromString(textBox1.Text, fileName);

                            message.Attachments.Add(reportAttachment);

                            smtp.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void poolQuotaEmailer(string[] args)
        {
            try
            {
                textBoxDirectorUsername.Text = args[1];
                textBoxDirectorPassword.Text = args[2];
                textBoxDomain.Text = args[3];
                textBoxVault.Text = args[4];
        
                string recipientEmails = args[5];
                string smtpServer = args[6];
                string smtpDomain = args[7];
                string smtpUsername = args[8];
                string smtpPassword = args[9];

                int smtpPort = Convert.ToInt32(args[10]);

                string query = args[11];
                string reseller = args[12].Trim();

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN poolQuotaEmailer:" + textBoxVault.Text, EventLogEntryType.Information);

                directorLogin();

                generatePoolQuota(query, reseller);

                string fileName = textBoxVault.Text + " PoolQuota_" + DateTime.Now.Month.ToString() + "." +
                                DateTime.Now.Day.ToString() + "." + DateTime.Now.Year.ToString() + ".csv";

                using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                {
                    smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                    string body = "Greetings,\r\nHere is the Auto-Generated Report.\r\n\r\nThanks,\r\n\r\nDakota Backup";

                    if (textBox1.Text.Contains("Total"))
                    {
                        using (MailMessage message = new MailMessage())
                        {
                            string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string theto in tos)
                            {
                                message.To.Add(theto);
                            }

                            string vault = textBoxVault.Text.Substring(0, textBoxVault.Text.IndexOf("."));

                            message.From = new MailAddress(vault + "@ktconnections.com");
                            message.Body = body;
                            message.Subject = vault + " Pool/Quota Report";

                            Attachment reportAttachment = Attachment.CreateAttachmentFromString(textBox1.Text, fileName);

                            message.Attachments.Add(reportAttachment);

                            smtp.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void poolLocationEmailer(string[] args)
        {
            try
            {
                textBoxDirectorUsername.Text = args[1];
                textBoxDirectorPassword.Text = args[2];
                textBoxDomain.Text = args[3];
                textBoxVault.Text = args[4];

                string recipientEmails = args[5];
                string smtpServer = args[6];
                string smtpDomain = args[7];
                string smtpUsername = args[8];
                string smtpPassword = args[9];

                int smtpPort = Convert.ToInt32(args[10]);

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN poolLocationEmailer:" + textBoxVault.Text, EventLogEntryType.Information);

                directorLogin();

                generatePoolLocation();

                string fileName = textBoxVault.Text + " Pool_" + DateTime.Now.Month.ToString() + "." +
                                DateTime.Now.Day.ToString() + "." + DateTime.Now.Year.ToString() + ".csv";

                using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                {
                    smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                    string body = "Greetings,\r\nHere is the Auto-Generated Report.\r\n\r\nThanks,\r\n\r\nDakota Backup";

                    if (textBox1.Text.Contains("Total"))
                    {
                        using (MailMessage message = new MailMessage())
                        {
                            string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string theto in tos)
                            {
                                message.To.Add(theto);
                            }

                            string vault = textBoxVault.Text.Substring(0, textBoxVault.Text.IndexOf("."));

                            message.From = new MailAddress(vault + "@ktconnections.com");
                            message.Body = body;
                            message.Subject = vault + " Pool Location Report";

                            Attachment reportAttachment = Attachment.CreateAttachmentFromString(textBox1.Text, fileName);

                            message.Attachments.Add(reportAttachment);

                            smtp.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void maintenanceMonitorEmailer(string[] args)
        {
            try
            {
                textBoxDirectorUsername.Text = args[1];
                textBoxDirectorPassword.Text = args[2];
                textBoxDomain.Text = args[3];
                textBoxWritePath.Text = args[4];
                m_baseVaultsFileName = args[5];

                string recipientEmails = args[6];
                string smtpServer = args[7];
                string smtpDomain = args[8];
                string smtpUsername = args[9];
                string smtpPassword = args[10];

                int smtpPort = Convert.ToInt32(args[11]);

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " BEGIN maintenanceMonitorEmailer", EventLogEntryType.Information);

                string[] vaults = File.ReadAllLines(textBoxWritePath.Text + "\\" + m_baseVaultsFileName);

                DataTable vaultData = getBaseVaultsUsageTable(vaults);

                string body = "";

                if (vaultData != null && vaultData.Rows != null && vaultData.Rows.Count > 0)
                {
                    foreach (DataRow dr in vaultData.Rows)
                    {
                        body += dr["VaultName"].ToString() + " - Maintenance: " + dr["Maintenance"].ToString() + "<br>";
                    }    
                }

                using (SmtpClient smtp = new SmtpClient(smtpServer, smtpPort))
                {
                    smtp.Credentials = new NetworkCredential(smtpUsername, smtpPassword, smtpDomain);

                    using (MailMessage message = new MailMessage())
                    {
                        message.IsBodyHtml = true;

                        string[] tos = recipientEmails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string theto in tos)
                        {
                            message.To.Add(theto);
                        }

                        message.From = new MailAddress("donotreply@ktconnections.com");
                        message.Body = body.Replace(": No", ": <font color=red size=6><b>No</b></font>");
                        message.Subject = "EVault Maintenance Monitor";

                        smtp.Send(message);
                    }
                }

                if (body.Contains(": No"))
                {
                    try
                    {
                        using (CWManipulator ticketMaker = new CWManipulator(m_cwCompany, 
                            m_vault2CWLogin["dbvault.dakotabackup.com"], m_cwPassword, m_cwSite))
                        {
                            int ticketID = ticketMaker.CreateTicket("DakotaBackupLLC", "Base Vault Maintenance Monitor Alert", 
                                body, "Helpdesk - DCR", "Backup Alert", 29, "", "", "No SLA");
                        }
                    }
                    catch (Exception ex)
                    {
                        if (m_formShown)
                        {
                            MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                        }

                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                    }  
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + " EXIT Application", EventLogEntryType.Information);

            Application.Exit();
        }

        private void showDefaultCursor()
        {
            this.Invoke(new MethodInvoker(() =>
            {
                Application.UseWaitCursor = false;

                buttonDirectorLogIn.Enabled = true;
                buttonPortalLogIn.Enabled = true;
                buttonDoIt.Enabled = true;

                Cursor.Current = Cursors.Default;
                Cursor.Position = Cursor.Position;

                Application.DoEvents();
            }));
        }

        private void showWaitCursor()
        {
            this.Invoke(new MethodInvoker(() =>
            {
                Application.UseWaitCursor = true;

                buttonDirectorLogIn.Enabled = false;
                buttonPortalLogIn.Enabled = false;
                buttonDoIt.Enabled = false;

                Cursor.Current = Cursors.WaitCursor;
                Cursor.Position = Cursor.Position;

                Application.DoEvents();
            }));
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs ex)
        {
            if (m_formShown)
            {
                MessageBox.Show("Show Dakota Backup: " + ((Exception)(ex.ExceptionObject)).Message + "\r\n\r\n" + ((Exception)(ex.ExceptionObject)).StackTrace);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ((Exception)(ex.ExceptionObject)).Message + "\r\n\r\n" + ((Exception)(ex.ExceptionObject)).StackTrace, EventLogEntryType.Error);

            Application.Exit();
        }

        private void Application_ThreadException(object sender, ThreadExceptionEventArgs ex)
        {
            if (m_formShown)
            {
                MessageBox.Show("Show Dakota Backup: " + ex.Exception.Message + "\r\n\r\n" + ex.Exception.StackTrace);
            }

            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Exception.Message + "\r\n\r\n" + ex.Exception.StackTrace, EventLogEntryType.Error);

            Application.Exit();
        }
        #endregion


        #region WebPortal Methods

        private void portalLogin()
        {
            if (m_dtPortalTasksDetails != null)
            {
                m_dtPortalTasksDetails.Dispose();
                m_dtPortalTasksDetails = null;
            }

            if (m_dtMegaTasksDetails != null)
            {
                m_dtMegaTasksDetails.Dispose();
                m_dtMegaTasksDetails = null;
            }

            if (m_sacApi != null)
            {
                m_sacApi = null;
            }

            m_sacApi = new SoapApiClient();

            try
            {
                m_uPortalUser = m_sacApi.AuthenticateUser(textBoxPortalUsername.Text.Trim(), textBoxPortalPassword.Text.Trim());

                m_loggedIntoPortal = true;

                if (m_formShown)
                {
                    MessageBox.Show("Welcome to the Web Portal, " + m_uPortalUser.FirstName);
                }

                getUserSitesGroups();

                if (m_dtSites != null && m_dtSites.Rows != null && m_dtSites.Rows.Count > 0)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                            comboBoxSites.DataSource = m_dtSites;
                            comboBoxSites.DisplayMember = "Name";
                            comboBoxSites.ValueMember = "Id";
                    }));
                }

                if (m_dtGroups != null && m_dtGroups.Rows != null && m_dtGroups.Rows.Count > 0)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        comboBoxGroups.DataSource = m_dtGroups;
                        comboBoxGroups.DisplayMember = "Name";
                        comboBoxGroups.ValueMember = "Id";
                    }));
                }
            }
            catch (Exception ex)
            {
                m_loggedIntoPortal = false;

                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void getUserSitesGroups()
        {
            if (m_dtSites != null)
            {
                m_dtSites.Dispose();
                m_dtSites = null;
            }

            m_dtSites = new DataTable();

            DataTable dtSites = new DataTable();

            dtSites.Columns.Add("Id");
            dtSites.Columns.Add("Name");

            if (m_dtGroups != null)
            {
                m_dtGroups.Dispose();
                m_dtGroups = null;
            }

            m_dtGroups = new DataTable();

            DataTable dtGroups = new DataTable();

            dtGroups.Columns.Add("Id");
            dtGroups.Columns.Add("Name");

            try
            {
                Company[] companies = m_sacApi.GetUserCompanies(m_uPortalUser.Id, m_uPortalUser.Id);

                AgentGroup[] groups = m_sacApi.GetUserAgentGroupList(m_uPortalUser.Id);

                if (companies != null && companies.Count() > 0)
                {
                    foreach (Company c in companies)
                    {
                        DataRow newRow = dtSites.NewRow();

                        newRow["Id"] = c.Id;
                        newRow["Name"] = c.Name;

                        dtSites.Rows.Add(newRow);
                        dtSites.AcceptChanges();
                    }
                }

                if (groups != null && groups.Count() > 0)
                {
                    foreach (AgentGroup g in groups)
                    {
                        DataRow newRow = dtGroups.NewRow();

                        newRow["Id"] = g.AgentGroupId;
                        newRow["Name"] = g.Name;

                        dtGroups.Rows.Add(newRow);
                        dtGroups.AcceptChanges();
                    }
                }

                if (dtSites.Rows.Count > 0 && dtGroups.Rows.Count > 0)
                {
                    DataView sView = dtSites.DefaultView;
                    sView.Sort = "Name ASC";
                    m_dtSites = sView.ToTable();

                    DataView gView = dtGroups.DefaultView;
                    gView.Sort = "Name ASC";
                    m_dtGroups = gView.ToTable();
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void getBIReady()
        {
            textBox1.Clear();

            textBox1.Text += "Backup Items Report\r\nGenerated on: " + DateTime.Now.ToString() + "\r\n" +
                "Generated by: " + m_uPortalUser.UserName + "\r\n\r\n" +
                "Backup Legend:\r\n" +
                "\t[++]  ::  Include this location and any subfolders\r\n\t[+]  ::  Include this location/item only\r\n" +
                "\t[--]  ::  Exclude this location and any subfolders\r\n\t[-]  ::  Exclude this location/item\r\n" +
                "\t$BMR$  ::  Everything required for a Bare Metal Recovery\r\n\tDoM  ::  Days of Month\r\n" +
                "\tDoW  ::  Days of Week\r\n\t# @ Name  ::  Number of safesets for the given Retention Name\r\n" +
                "\tOffsite Replication  ::\r\n\t\tYes: Backup to Satellite Vault and replicated to Base Vault\r\n" +
                "\t\tNo: Backup to Satellite Vault and not replicated to Base Vault\r\n" +
                "\t\tBlank: Backup to Base Vault (No Satellite Vault involved)\r\n";
        }

        private void generateTaskBI()
        {
            getBIReady();
                        
            try
            {
                Guid agentID = Guid.Parse(((DataRowView)(comboBoxAgents.SelectedItem))["Id"].ToString());

                string agentName = ((DataRowView)(comboBoxAgents.SelectedItem))["Name"].ToString();
                string jobID = ((DataRowView)(comboBoxJobs.SelectedItem))["Id"].ToString();
                string jobName = ((DataRowView)(comboBoxJobs.SelectedItem))["Name"].ToString();

                m_companyName = agentName + "_" + jobName;

                JobConfiguration jConfig = m_sacApi.GetJobConfiguration(agentID, jobID, m_uPortalUser.Id);

                AgentConfiguration aConfig = m_sacApi.GetAgentConfiguration(agentID, m_uPortalUser.Id);

                if (jConfig != null)
                {
                    writeJobDetails(agentName, jobName, aConfig, jConfig);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cannot communicate with the agent. The agent might be offline."))
                {
                    string agentName = ((DataRowView)(comboBoxAgents.SelectedItem))["Name"].ToString();
                    string jobName = ((DataRowView)(comboBoxJobs.SelectedItem))["Name"].ToString();

                    writeJobDetails(agentName, jobName, null, null);
                }
                else
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        private void generateGroupBI()
        {
            getBIReady();

            try
            {
                Guid groupID = Guid.Parse(((DataRowView)(comboBoxGroups.SelectedItem))["Id"].ToString());
                string groupName = ((DataRowView)(comboBoxGroups.SelectedItem))["Name"].ToString();

                m_companyName = groupName;

                Agent[] agents = m_sacApi.GetUserAgents(m_uPortalUser.Id, m_uPortalUser.Id);

                if (agents != null && agents.Count() > 0)
                {
                    foreach (Agent a in agents)
                    {
                        if (a.AssignedAgentGroups.Contains(groupName))
                        {
                            Job[] jobs = null;
                            AgentConfiguration aConfig = null;

                            try
                            {
                                jobs = m_sacApi.GetAgentJobs(a.Id, m_uPortalUser.Id);
                                aConfig = m_sacApi.GetAgentConfiguration(a.Id, m_uPortalUser.Id);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Contains("Cannot communicate with the agent. The agent might be offline."))
                                {
                                    writeJobDetails(a.Name, null, null, null);
                                }
                                else
                                {
                                    if (m_formShown)
                                    {
                                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                                    }

                                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                                }
                            }

                            if (jobs != null && jobs.Count() > 0)
                            {
                                foreach (Job j in jobs)
                                {
                                    try
                                    {
                                        JobConfiguration jConfig = (j.Type != JobType.SharePoint && j.Type != JobType.HyperV) ? m_sacApi.GetJobConfiguration(a.Id, j.Id, m_uPortalUser.Id) : null;

                                        writeJobDetails(a.Name, j.Name, aConfig, jConfig);                                        
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.Message.Contains("Cannot communicate with the agent. The agent might be offline."))
                                        {
                                            writeJobDetails(a.Name, j.Name, null, null);
                                        }
                                        else
                                        {
                                            if (m_formShown)
                                            {
                                                MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                                            }

                                            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void generateSubsiteBI()
        {
            getBIReady();

            try
            {
                Guid companyID = Guid.Parse(((DataRowView)(comboBoxSubsites.SelectedItem))["Id"].ToString());

                m_companyName = ((DataRowView)(comboBoxSubsites.SelectedItem))["Name"].ToString();

                Agent[] agents = m_sacApi.GetCompanyAgents(companyID, true, m_uPortalUser.Id);

                AgentGroup[] groups = m_sacApi.GetUserAgentGroupList(m_uPortalUser.Id);

                List<string> groupsWithEmpty = new List<string>();

                groupsWithEmpty.Add("");

                foreach (AgentGroup group in groups)
                {
                    groupsWithEmpty.Add(group.Name);
                }

                if (agents != null && groups != null && agents.Count() > 0 && groups.Count() > 0)
                {
                    foreach (string groupName in groupsWithEmpty)
                    {
                        textBox1.Text += "\r\n\r\nGroup:\t\t\t\t" + groupName;

                        foreach (Agent a in agents)
                        {
                            if (a.AssignedAgentGroups == groupName)
                            {
                                Job[] jobs = null;
                                AgentConfiguration aConfig = null;

                                try
                                {
                                    jobs = m_sacApi.GetAgentJobs(a.Id, m_uPortalUser.Id);
                                    aConfig = m_sacApi.GetAgentConfiguration(a.Id, m_uPortalUser.Id);
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("Cannot communicate with the agent. The agent might be offline."))
                                    {
                                        writeJobDetails(a.Name, null, null, null);
                                    }
                                    else
                                    {
                                        if (m_formShown)
                                        {
                                            MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                                        }

                                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                                    }
                                }

                                if (jobs != null && jobs.Count() > 0)
                                {
                                    foreach (Job j in jobs)
                                    {
                                        try
                                        {// or else you get jobtype is not supported? wtf is this?
                                            JobConfiguration jConfig = (j.Type != JobType.SharePoint && j.Type != JobType.HyperV) ? m_sacApi.GetJobConfiguration(a.Id, j.Id, m_uPortalUser.Id) : null;

                                            writeJobDetails(a.Name, j.Name, aConfig, jConfig);
                                        }
                                        catch (Exception ex)
                                        {
                                            if (ex.Message.Contains("Cannot communicate with the agent. The agent might be offline."))
                                            {
                                                writeJobDetails(a.Name, j.Name, null, null);
                                            }
                                            else
                                            {
                                                if (m_formShown)
                                                {
                                                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                                                }

                                                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void writeJobDetails(string agentName, string jobName, AgentConfiguration aConfig, JobConfiguration jConfig)
        {
            string recipients = "";

            if (aConfig != null && aConfig.RecipientAddresses.Count() > 0)
            {
                foreach (string recipient in aConfig.RecipientAddresses)
                {
                    recipients += recipient + ";";
                }
            }

            if (recipients.EndsWith(";"))
            {
                recipients = recipients.Substring(0, recipients.Length - 1);
            }

            textBox1.Text += "\r\n\r\nAgent:\t\t\t\t" + agentName + "\r\n";

            if (jConfig != null)
            {
                textBox1.Text += "Backup Task:\t\t\t" + jobName + "\r\n" +
                "Encryption Method:\t\t" + jConfig.EncryptionMethod.ToString() + "\r\n" +
                "Notification Recipients:\t" + recipients + "\r\nOffsite Replication:\t\t\r\nSchedule / Retention:\r\n";

                JobType jt = jConfig.Type;

                BackupSelectionItem[] items = jConfig.SelectionItems;
                
                Schedule[] schedules = jConfig.Schedules;

                if (schedules != null && schedules.Count() > 0)
                {
                    foreach (Schedule s in schedules)
                    {
                        if (s.Enabled)
                        {
                            switch (s.CycleType)
                            {
                                case CycleType.Weekly:
                                    {
                                        textBox1.Text += "\tDoW: ";

                                        string days = "";

                                        if (s.SelectedWeekDays != null && s.SelectedWeekDays.Count() > 0)
                                        {
                                            foreach (WeekDay day in s.SelectedWeekDays)
                                            {
                                                days += day.ToString().Substring(0, 3) + ",";
                                            }
                                        }

                                        if (days.Length > 0)
                                            days = days.Substring(0, days.Length - 1);

                                        textBox1.Text += days + " @ " + s.Time.ToString();
                                    }
                                    break;
                                case CycleType.Monthly:
                                    {
                                        textBox1.Text += "\tDoM: ";

                                        string days = "";

                                        if (s.SelectedMonthDays != null && s.SelectedMonthDays.Count() > 0)
                                        {
                                            foreach (MonthDay day in s.SelectedMonthDays)
                                            {
                                                days += day.ToString() + ",";
                                            }
                                        }

                                        if (days.Length > 0)
                                            days = days.Substring(0, days.Length - 1);

                                        textBox1.Text += days + " @ " + s.Time.ToString();
                                    }
                                    break;
                                case CycleType.Custom:
                                    {
                                        textBox1.Text += "\tCustom: " + s.CustomCycle;
                                    }
                                    break;
                                default:
                                    break;
                            }

                            textBox1.Text += " / " + s.Retention.OnlineCopies + " @ " + s.Retention.Name + "\r\n";
                        }
                    }
                }

                textBox1.Text += "Inclusions/Exclusions:\r\n";

                if (items != null && items.Count() > 0)
                {
                    foreach (BackupSelectionItem item in items)
                    {
                        string line = "-";

                        if (item.Type == SelectionItemType.Include)
                            line = "+";

                        if (item.Recursive)
                            line += line;

                        line = "\t[" + line + "] ";

                        string switchLine = "";

                        switch (jt)
                        {
                            case JobType.LocalFile:
                                {
                                    switchLine = item.StartLocation + item.Name;
                                }
                                break;
                            case JobType.Exchange2003:
                                {
                                    switchLine = item.DisplayContainer + "\\*";
                                }
                                break;
                            case JobType.VMware:
                                {
                                    switchLine = item.ObjectType.ToString() + "\\" + item.DisplayName;
                                }
                                break;
                            case JobType.Exchange2007:
                                {
                                    switchLine = item.DisplayContainer + "\\*";
                                }
                                break;
                            case JobType.Exchange2010_2013:
                                {
                                    switchLine = item.DisplayContainer + "\\*";
                                }
                                break;
                            case JobType.VSphere:
                                {
                                    switchLine = item.ObjectType.ToString() + "\\" + item.DisplayName;
                                }
                                break;
                            case JobType.SqlServerVss:
                                {
                                    switchLine = "[" + item.DisplayContainer + "]\\" + item.DisplayName;
                                }
                                break;
                            case JobType.UncFile:
                                {
                                    switchLine = item.DisplayContainer + item.DisplayName;
                                }
                                break;
                            default:
                                switchLine = item.Name;
                                break;
                        }

                        line += switchLine;

                        textBox1.Text += line + "\r\n";
                    }
                }
            }
            else
            {
                textBox1.Text += "The agent might be offline\r\n";
            }
        }

        private Document createBIDocument(string name)
        {
            Document bi = new Document();

            Section section = bi.AddSection();

            Paragraph nameParagaph = section.AddParagraph();

            nameParagaph.Format.Font.Size = Unit.FromPoint(12.0);
            nameParagaph.Format.Font.Bold = true;

            nameParagaph.AddFormattedText(name);

            string[] lines = textBox1.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            if (lines != null && lines.Count() > 0)
            {
                foreach (string line in lines)
                {
                    Paragraph paragraph = section.AddParagraph();

                    if (line.Contains("Agent:\t\t\t") ||
                        line.Contains("Backup Task:\t\t\t") ||
                        line.Contains("Offsite Replication:\t\t") ||
                        line.Contains("Notification Recipients:\t") ||
                        line.Contains("Encryption Method:\t\t"))
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(12.0);
                        paragraph.Format.Font.Bold = true;
                    }
                    else if (line.Contains("Schedule / Retention") ||
                        line.Contains("\tDoW") || line.Contains("\tDoM") || line.Contains("\tCustom:") || line.Contains("# @"))
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(10.0);
                        paragraph.Format.Font.Color = MigraDoc.DocumentObjectModel.Colors.Blue;
                    }
                    else if (line.Contains("Inclusions/Exclusions") ||
                        line.Contains("\t[+") || line.Contains("$BMR$"))
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(10.0);
                        paragraph.Format.Font.Color = MigraDoc.DocumentObjectModel.Colors.Green;
                    }
                    else if (line.Contains("\t[-"))
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(10.0);
                        paragraph.Format.Font.Color = MigraDoc.DocumentObjectModel.Colors.Red;
                    }
                    else if (line.Contains("Backup Legend:"))
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(10.0);
                        paragraph.Format.Font.Bold = true;
                    }
                    else if (line.Contains("Group:\t\t\t\t"))
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(14.0);
                        paragraph.Format.Font.Bold = true;
                    }
                    else
                    {
                        paragraph.Format.Font.Size = Unit.FromPoint(10.0);
                    }

                    paragraph.AddFormattedText(line);
                }
            }

            return bi;
        }

        private void getPortalTasksDetailsTable()
        {
            if (m_dtPortalTasksDetails == null)
            {
                m_dtPortalTasksDetails = new DataTable();

                DataColumn pKey = new DataColumn("pKey");

                m_dtPortalTasksDetails.Columns.Add("PReqStatus");
                m_dtPortalTasksDetails.Columns.Add("AgentOnline");
                m_dtPortalTasksDetails.Columns.Add("AccountID");
                m_dtPortalTasksDetails.Columns.Add("PCompanyID"); // guid
                m_dtPortalTasksDetails.Columns.Add("PCompanyName");
                m_dtPortalTasksDetails.Columns.Add("GroupName");
                m_dtPortalTasksDetails.Columns.Add("GroupID");
                m_dtPortalTasksDetails.Columns.Add("ComputerName");
                m_dtPortalTasksDetails.Columns.Add("PComputerID");
                m_dtPortalTasksDetails.Columns.Add("AgentVersion");
                m_dtPortalTasksDetails.Columns.Add("OSVersion");
                m_dtPortalTasksDetails.Columns.Add("TaskName");
                m_dtPortalTasksDetails.Columns.Add("TaskType");
                m_dtPortalTasksDetails.Columns.Add("PTaskID");
                m_dtPortalTasksDetails.Columns.Add("MailServer");
                m_dtPortalTasksDetails.Columns.Add("MailServerPort");
                m_dtPortalTasksDetails.Columns.Add("MailServerRecipients");
                m_dtPortalTasksDetails.Columns.Add("MailFrom");
                m_dtPortalTasksDetails.Columns.Add("MailServerUsername");
                m_dtPortalTasksDetails.Columns.Add("MailServerPassword");
                m_dtPortalTasksDetails.Columns.Add("MailServerDomain");
                m_dtPortalTasksDetails.Columns.Add("MailOnError");
                m_dtPortalTasksDetails.Columns.Add("MailOnFailure");
                m_dtPortalTasksDetails.Columns.Add("MailOnSuccess");
                m_dtPortalTasksDetails.Columns.Add("LastBackupTime", typeof(DateTime));
                m_dtPortalTasksDetails.Columns.Add("DaysSinceLastBackup", typeof(double));
                m_dtPortalTasksDetails.Columns.Add("LastSuccessfulBackupTime", typeof(DateTime));
                m_dtPortalTasksDetails.Columns.Add("DaysSinceLastSuccessfulBackup", typeof(double));
                m_dtPortalTasksDetails.Columns.Add("LastVerySuccessfulBackupTime", typeof(DateTime));
                m_dtPortalTasksDetails.Columns.Add("DaysSinceLastVerySuccessfulBackup", typeof(double));
                m_dtPortalTasksDetails.Columns.Add("LastPerfectBackupTime", typeof(DateTime));
                m_dtPortalTasksDetails.Columns.Add("DaysSinceLastPerfectBackup", typeof(double));
                m_dtPortalTasksDetails.Columns.Add("LastBackupErrorNumber", typeof(int));
                m_dtPortalTasksDetails.Columns.Add("LastBackupResult");
                m_dtPortalTasksDetails.Columns.Add("ScheduleRetention");
                m_dtPortalTasksDetails.Columns.Add("LastPresent", typeof(DateTime));
                m_dtPortalTasksDetails.Columns.Add("DaysSinceLastPresent", typeof(double));
                m_dtPortalTasksDetails.Columns.Add("AgentConfiguration", typeof(AgentConfiguration));
                m_dtPortalTasksDetails.Columns.Add("JobConfiguration", typeof(JobConfiguration));
                m_dtPortalTasksDetails.Columns.Add("AgentVaultRegistrations", typeof(object));
                m_dtPortalTasksDetails.Columns.Add("PReqMessage");

                m_dtPortalTasksDetails.Columns.Add(pKey);

                m_dtPortalTasksDetails.PrimaryKey = new DataColumn[] { pKey };

                m_dtPortalTasksDetails.AcceptChanges();
                
                fillAgentData();

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(()=> textBox1.Text += DateTime.Now.ToLongTimeString() + "\tFinished Getting Agent Data\r\n"));
                    this.Invoke(new MethodInvoker(()=> textBox1.Update()));
                }

                if (_agentData != null && _agentData.Count() > 0)
                {
                    foreach (AgentData ad in _agentData)
                    {
                        if (ad.JobConfigurations != null && ad.JobConfigurations.Count() > 0)
                        {
                            for (int i = 0; i < ad.JobConfigurations.Count(); i++)
                            {       
                                DataRow newRow = m_dtPortalTasksDetails.NewRow();
                                
                                newRow["AgentOnline"] = ad.Agent.Online;
                                newRow["PReqStatus"] = ad.ReqStatus;
                                newRow["PReqMessage"] = ad.ReqMessage;
                                newRow["GroupName"] = string.IsNullOrEmpty(ad.GroupName) ? (ad.Company != null && !string.IsNullOrEmpty(ad.Company.Name) ? ad.Company.Name : ad.GroupName) : ad.GroupName;
                                newRow["GroupID"] = ad.GroupID;
                                newRow["PCompanyName"] = ad.Company != null ? ad.Company.Name : string.Empty;
                                newRow["PCompanyID"] = ad.Company != null ? ad.Company.Id : Guid.Empty;
                                newRow["PComputerID"] = ad.Agent.Id;
                                newRow["ComputerName"] = ad.Agent.Name;
                                newRow["AgentVersion"] = ad.Agent.AgentVersion;
                                newRow["OSVersion"] = ad.Agent.OsVersion;
                                newRow["LastPresent"] = ad.Agent.LastPresent.ToLocalTime();
                                newRow["DaysSinceLastPresent"] = ((TimeSpan)DateTime.Now.Subtract(ad.Agent.LastPresent.ToLocalTime())).TotalDays;
                                
                                if (ad.Jobs[i] != null)
                                {
                                    DateTime lbt = ad.Jobs[i].LastBackupTime == DateTime.MinValue ? DateTime.MinValue : ad.Jobs[i].LastBackupTime.ToLocalTime();
                                    DateTime lsbt = ad.Jobs[i].LastSuccessfulBackupTime == DateTime.MinValue ? DateTime.MinValue : ad.Jobs[i].LastSuccessfulBackupTime.ToLocalTime();
                                    DateTime lvsbt = ad.LastBackupJobsVerySuccessfulBackupTime[i] == DateTime.MinValue ? DateTime.MinValue : ad.LastBackupJobsVerySuccessfulBackupTime[i].ToLocalTime();
                                    DateTime lpbt = ad.LastBackupJobsPerfectBackupTime[i] == DateTime.MinValue ? DateTime.MinValue : ad.LastBackupJobsPerfectBackupTime[i].ToLocalTime();

                                    int indexF = 0;
                                    int indexE = 0;
                                    string server = "";

                                    if (ad.Jobs[i].Id.Contains("~"))
                                    {
                                        indexF = ad.Jobs[i].Id.IndexOf("~");
                                        indexE = ad.Jobs[i].Id.LastIndexOf("~");

                                        if (indexE - indexF > 1)
                                        {
                                            server = ad.Jobs[i].Id.Substring(indexF + 1, indexE - indexF - 1);
                                        }
                                    }

                                    newRow["TaskName"] = ad.Jobs[i].Id.Contains("~") ? ad.Jobs[i].Name + " (from " + server + ")" : ad.Jobs[i].Name;  
                                    newRow["PTaskID"] = ad.Jobs[i].Id;
                                    newRow["TaskType"] = ad.Jobs[i].Type.ToString();
                                    newRow["LastBackupTime"] = lbt;
                                    newRow["DaysSinceLastBackup"] = DateTime.Now.Subtract(lbt).TotalDays;
                                    newRow["LastSuccessfulBackupTime"] = lsbt;
                                    newRow["DaysSinceLastSuccessfulBackup"] = DateTime.Now.Subtract(lsbt).TotalDays;
                                    newRow["LastVerySuccessfulBackupTime"] = lvsbt;
                                    newRow["DaysSinceLastVerySuccessfulBackup"] = DateTime.Now.Subtract(lvsbt).TotalDays;
                                    newRow["LastPerfectBackupTime"] = lpbt;
                                    newRow["DaysSinceLastPerfectBackup"] = DateTime.Now.Subtract(lpbt).TotalDays;
                                    newRow["LastBackupErrorNumber"] = ad.Jobs[i].LastRestoreErrorNumber;
                                    newRow["LastBackupResult"] = ad.Jobs[i].LastBackupResult.ToString();
                                }
                                else
                                {
                                    newRow["LastBackupTime"] = DateTime.MinValue;
                                    newRow["DaysSinceLastBackup"] = -1.0;
                                    newRow["LastSuccessfulBackupTime"] = DateTime.MinValue;
                                    newRow["DaysSinceLastSuccessfulBackup"] = -1.0;
                                    newRow["LastVerySuccessfulBackupTime"] = DateTime.MinValue;
                                    newRow["DaysSinceLastVerySuccessfulBackup"] = -1.0;
                                    newRow["LastPerfectBackupTime"] = DateTime.MinValue;
                                    newRow["DaysSinceLastPerfectBackup"] = -1.0;
                                }

                                if (ad.VaultRegistrations != null && ad.VaultRegistrations.Count() > 0)
                                {
                                    newRow["AccountID"] = ad.VaultRegistrations[0].Account.ToLower();
                                }
                                else
                                {
                                    newRow["AccountID"] = "n/a_" + ad.GroupName; //possible probs here 
                                }

                                if (ad.AgentConfiguration != null)
                                {
                                    newRow["MailServer"] = ad.AgentConfiguration.MailServer;
                                    newRow["MailServerPort"] = ad.AgentConfiguration.MailServerPort;
                                    newRow["MailServerUsername"] = ad.AgentConfiguration.MailServerUserName;
                                    newRow["MailServerPassword"] = ad.AgentConfiguration.MailServerPassword;
                                    newRow["MailServerDomain"] = ad.AgentConfiguration.MailServerDomain;
                                    newRow["MailOnSuccess"] = ad.AgentConfiguration.MailOnSuccess;
                                    newRow["MailOnFailure"] = ad.AgentConfiguration.MailOnFailure;
                                    newRow["MailOnError"] = ad.AgentConfiguration.MailOnError;
                                    newRow["MailFrom"] = ad.AgentConfiguration.FromAddress;

                                    if (ad.AgentConfiguration.RecipientAddresses != null && ad.AgentConfiguration.RecipientAddresses.Count() > 0)
                                    {
                                        foreach (string recip in ad.AgentConfiguration.RecipientAddresses)
                                        {
                                            newRow["MailServerRecipients"] += recip + ",";
                                        }
                                    }
                                }

                                if (ad.JobConfigurations[i] != null)
                                {
                                    string schedule = "";

                                    if (ad.JobConfigurations[i].Schedules.Count() > 0)
                                    {
                                        foreach (Schedule s in ad.JobConfigurations[i].Schedules)
                                        {
                                            if (s.Enabled)
                                            {
                                                string days = "";

                                                switch (s.CycleType)
                                                {
                                                    case CycleType.Weekly:
                                                        {
                                                            if (s.SelectedWeekDays != null && s.SelectedWeekDays.Count() > 0)
                                                            {
                                                                foreach (WeekDay day in s.SelectedWeekDays)
                                                                {
                                                                    days += day.ToString().Substring(0, 3) + ",";
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case CycleType.Monthly:
                                                        {
                                                            if (s.SelectedMonthDays != null && s.SelectedMonthDays.Count() > 0)
                                                            {
                                                                foreach (MonthDay day in s.SelectedMonthDays)
                                                                {
                                                                    days += day.ToString() + ",";
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case CycleType.Custom:
                                                        {
                                                            days += "C:" + s.CustomCycle + " ";
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }

                                                if (days.Length > 0)
                                                    days = days.Substring(0, days.Length - 1);

                                                schedule += days + "#" + s.Retention.OnlineCopies + "@" + s.Retention.Name + "\r\n";
                                            }
                                        }
                                    }

                                    if (schedule.EndsWith("\r\n"))
                                    {
                                        schedule = schedule.Substring(0, schedule.Length - 2);
                                    }

                                    newRow["ScheduleRetention"] = schedule;
                                }

                                newRow["JobConfiguration"] = ad.JobConfigurations[i];
                                newRow["AgentConfiguration"] = ad.AgentConfiguration;
                                newRow["AgentVaultRegistrations"] = ad.VaultRegistrations;

                                newRow["pKey"] = newRow["AccountID"].ToString() + "_" + newRow["ComputerName"].ToString().ToLower() + "_" + newRow["TaskName"].ToString().ToLower();

                                m_dtPortalTasksDetails.Rows.Add(newRow);
                                m_dtPortalTasksDetails.AcceptChanges();
                            }
                        }
                        else
                        {
                            DataRow newRow = m_dtPortalTasksDetails.NewRow();

                            if (ad.VaultRegistrations != null && ad.VaultRegistrations.Count() > 0)
                            {
                                newRow["AccountID"] = ad.VaultRegistrations[0].Account.ToLower();
                            }
                            else
                            {
                                newRow["AccountID"] = "n/a_" + ad.GroupName; //possible probs here 
                            }
                            
                            newRow["AgentOnline"] = ad.Agent.Online;
                            newRow["PReqStatus"] = ad.ReqStatus;
                            newRow["PReqMessage"] = ad.ReqMessage;
                            newRow["GroupName"] = string.IsNullOrEmpty(ad.GroupName) ? (ad.Company != null && !string.IsNullOrEmpty(ad.Company.Name) ? ad.Company.Name : ad.GroupName) : ad.GroupName;
                            newRow["GroupID"] = ad.GroupID;
                            newRow["PCompanyName"] = ad.Company != null ? ad.Company.Name : string.Empty;
                            newRow["PCompanyID"] = ad.Company != null ? ad.Company.Id : Guid.Empty;
                            newRow["PComputerID"] = ad.Agent.Id;
                            newRow["ComputerName"] = ad.Agent.Name;
                            newRow["AgentVersion"] = ad.Agent.AgentVersion;
                            newRow["OSVersion"] = ad.Agent.OsVersion;
                            newRow["LastPresent"] = ad.Agent.LastPresent;
                            newRow["DaysSinceLastPresent"] = -1.0;
                            newRow["LastBackupTime"] = DateTime.MinValue;
                            newRow["DaysSinceLastBackup"] = -1.0;
                            newRow["LastSuccessfulBackupTime"] = DateTime.MinValue;
                            newRow["DaysSinceLastSuccessfulBackup"] = -1.0;
                            newRow["LastVerySuccessfulBackupTime"] = DateTime.MinValue;
                            newRow["DaysSinceLastVerySuccessfulBackup"] = -1.0;
                            newRow["LastPerfectBackupTime"] = DateTime.MinValue;
                            newRow["DaysSinceLastPerfectBackup"] = -1.0;

                            newRow["pKey"] = newRow["AccountID"].ToString() + "_" + newRow["ComputerName"].ToString().ToLower() + "_" + Guid.NewGuid().ToString();

                            m_dtPortalTasksDetails.Rows.Add(newRow);
                            m_dtPortalTasksDetails.AcceptChanges();
                        }
                    }
                }
            }
        }

        private void fillAgentData()
        {
            if (_agentData != null)
            {
                _agentData = null;
            }

            try
            {
                Agent[] agents = m_sacApi.GetUserAgents(m_uPortalUser.Id, m_uPortalUser.Id);
                
                if (agents != null && agents.Count() > 0)
                {
                    int agentCount = agents.Count();

                    if (_agentData != null)
                    {
                        _agentData.Clear();
                        _agentData = null;
                    }

                    _agentData = new List<AgentData>(agents.Count());
                    
                    long threadsSent = 0;
                    _threadsReceived = 0;

                    for (int i = 0; i < agentCount; i++)
                    {
                        long threadsActive = _maxActiveThreads + 1;

                        while (threadsActive >= _maxActiveThreads)
                        {
                            threadsActive = threadsSent - Interlocked.Read(ref _threadsReceived);

                            if (threadsActive >= _maxActiveThreads)
                                Thread.Sleep(2000);
                        }

                        SoapApiClient sac = new SoapApiClient();

                        PortalApi.User u = null;

                        lock (_locker)
                        {
                            u = m_uPortalUser;
                        }

                        Task<AgentData> taskAD = null;

                        AgentData ad = new AgentData(agents[i], sac, u, m_dtGroups);

                        Thread newThread = new Thread(async delegate ()
                        {
                            taskAD = ad.FindAgentDataAsync();
                            ///MLM - don't forget to handle this exception!!!!!!!!!!!!
                            ad = await taskAD;

                            Interlocked.Increment(ref _threadsReceived);

                            lock (_locker)
                            {
                                _agentData.Add(ad);
                            }
                        });

                        newThread.Start();

                        threadsSent++;
                    }

                    while (true)
                    {
                        long threadsReceived = Interlocked.Read(ref _threadsReceived);

                        if (threadsReceived == agentCount)
                            break;

                        Thread.Sleep(15000);
                    }
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private string[] getAgentJobLogMessages(Guid agentID, string taskID, JobLogFileType fileType, Guid userID, string whichLogFile, LogFilterType filterType)
        {
            LogFileCollection fileCollection = null;

            string[] messages = null;

            try
            {
                fileCollection = m_sacApi.GetJobLogFiles(agentID, taskID, fileType, userID);
            }
            catch (Exception ex)
            {
                messages = new string[] { "Error: Couldn't Get Job Log Files,\"" + ex.Message + "\"" };

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);

                return messages;
            }

            DataTable logTable = new DataTable();

            logTable.Columns.Add("Name");
            logTable.Columns.Add("DateTime", typeof(DateTime));

            logTable.AcceptChanges();

            if (fileCollection != null && fileCollection.LogFiles!= null && fileCollection.LogFiles.Count() > 0)
            {
                foreach (LogFile file in fileCollection.LogFiles)
                {
                    DataRow newRow = logTable.NewRow();

                    newRow["Name"] = file.Name;
                    newRow["DateTime"] = file.Date;

                    logTable.Rows.Add(newRow);
                    logTable.AcceptChanges();
                }
            }

            string filterExpression = "";
            string sortExpression = "DateTime DESC";

            DataRow[] sortedLog = logTable.Select(filterExpression, sortExpression);

            if (sortedLog != null && sortedLog.Count() > 0)
            {
                try
                {
                    switch (whichLogFile)
                    {
                        case "newest":
                            {
                                messages = m_sacApi.GetJobLogFileSummary(agentID, taskID, sortedLog[0]["Name"].ToString(), filterType, userID);
                            }
                            break;
                        default:
                            {
                                messages = new string[] { "wtf?" };
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    messages = new string[] { "Error: Couldn't Get Job Log File Summary,\"" + ex.Message + "\"" };

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }

            return messages;
        }
        #endregion


        #region Director Methods


        private void directorLogin()
        {
            if (m_cmManager != null)
                m_cmManager = null;

            m_cmManager = new CManager();

            m_cmManager.LoggingMode = 2;

            if (m_cvcConnector != null)
                m_cvcConnector = null;

            m_cvcConnector = new CVaultConnection();

            m_cvcConnector.Address = textBoxVault.Text.Trim();
            m_cvcConnector.AuthenticationMode = AuthenticationModeEnum.AmeCredentials;
            m_cvcConnector.Domain = textBoxDomain.Text.Trim();
            m_cvcConnector.Password = textBoxDirectorPassword.Text.Trim();
            m_cvcConnector.userName = textBoxDirectorUsername.Text.Trim();

            try
            {
                bool maintActive = m_cmManager.getMaintenanceStatus(m_cvcConnector);

                string maintStatus = "Maintenance: ";

                if (maintActive)
                    maintStatus += "ON";
                else
                    maintStatus += "OFF";

                string repTypeStatus = null;

                foreach (VaultReplicationType repType in (VaultReplicationType[])Enum.GetValues(typeof(VaultReplicationType)))
                {
                    try
                    {
                        bool active = m_cmManager.getReplServiceStatus(m_cvcConnector, repType);

                        repTypeStatus = repType.ToString() + ": ";

                        if (active)
                            repTypeStatus += "ON";
                        else
                            repTypeStatus += "OFF";
                    }
                    catch { }
                }

                if (m_formShown)
                {
                    MessageBox.Show("Welcome to the " + m_cvcConnector.Address + " Director, " + m_cvcConnector.userName + ".\r\n" +
                        repTypeStatus + "\r\n" + maintStatus);
                }

                if (m_dtDirectorCustomerDetails != null)
                {
                    m_dtDirectorCustomerDetails.Dispose();
                    m_dtDirectorCustomerDetails = null;
                }

                if (m_dtDirectorTasksDetails != null)
                {
                    m_dtDirectorTasksDetails.Dispose();
                    m_dtDirectorTasksDetails = null;
                }

                if (m_dtMegaTasksDetails != null)
                {
                    m_dtMegaTasksDetails.Dispose();
                    m_dtMegaTasksDetails = null;
                }

                m_vaultAddress = m_cvcConnector.Address.ToLower();

                m_loggedIntoDirector = true;
            }
            catch (Exception ex)
            {
                m_loggedIntoDirector = false;

                string show = ex.Message;

                if (ex.Message.Contains("Make sure the provided credentials are correct"))
                    show = "Invalid vault username or password.";
                else if (ex.Message.Contains("Could not translate host"))
                    show = "Invalid vault address.";
    
                if (m_formShown)
                {
                    MessageBox.Show(show);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + show + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void generatePoolQuota(string query, string reseller)
        {
            syncEVaultWithCW();

            if (m_dtDirectorCustomerDetails != null)
            {
                m_dtDirectorCustomerDetails.Dispose();
                m_dtDirectorCustomerDetails = null;
            }

            if (m_dtDirectorTasksDetails != null)
            {
                m_dtDirectorTasksDetails.Dispose();
                m_dtDirectorTasksDetails = null;
            }
           
            getDirectorCustomerDetailsTable(query);

            getDirectorTasksDetailsTable(query);
            
            textBox1.Clear();

            textBox1.Text += "Customer,Account ID,Billing Code,Quota (GB),Pool Size (GB),% of Quota,Sales Person,Managed Service," +
                "Signed Contract,Satellite Appliance,Single-Tenant,Partner,Current Billing (GB),Monthly Charge ($),over(+) | under(-) Quota (GB),Native Size (GB)," + 
                "Compression Ratio\r\n\r\n";

            textBox1.Text += "Multi-Tenant Customers\r\n";

            if (m_dtDirectorCustomerDetails != null && m_dtDirectorCustomerDetails.Rows != null && m_dtDirectorCustomerDetails.Rows.Count > 0)
            {
                double totalNativeGBs = 0.0;
                double totalPoolGBs = 0.0;
                double totalQuotaGBs = 0.0;
                double totalMonthlyCharge = 0.0;

                foreach (DataRow custRow in m_dtDirectorCustomerDetails.Rows)
                {
                    if (string.IsNullOrEmpty(reseller) || custRow["Partner"].ToString() == reseller)
                    {
                        if (custRow["SingleTenant"].ToString().ToLower() == "no")
                        {
                            double nativeSize = Convert.ToDouble(custRow["NativeSize"].ToString());
                            double poolSize = Convert.ToDouble(custRow["PoolSize"].ToString());
                            double monthlyCharge = Convert.ToDouble(custRow["MonthlyAmt"].ToString());
                            string cycle = custRow["BillingCycle"].ToString();

                            monthlyCharge = cycle.ToLower().Equals("yearly") ? monthlyCharge / 12 : monthlyCharge;

                            totalNativeGBs += nativeSize;
                            totalPoolGBs += poolSize;
                            totalMonthlyCharge += monthlyCharge;

                            totalQuotaGBs += poolSize > 0.0 ? Convert.ToDouble(custRow["Quota"].ToString()) : 0.0;

                            textBox1.Text += "\"" + custRow["CustomerName"].ToString() + "\"," +
                                custRow["CWID"].ToString() + ",\"" +
                                custRow["LocationCode"].ToString() + "\"," +
                                custRow["Quota"].ToString() + "," +
                                custRow["PoolSize"].ToString() + "," +
                                custRow["PercentOfQuota"].ToString() + "," +
                                custRow["SalesPerson"].ToString() + "," +
                                custRow["ManagedServiceCustomer"].ToString() + "," +
                                custRow["SignedContract"].ToString() + "," +
                                custRow["SatelliteAppliance"].ToString() + "," +
                                custRow["SingleTenant"].ToString() + "," +
                                custRow["Partner"].ToString() + "," +
                                custRow["BillingSize"].ToString() + "," +
                                monthlyCharge.ToString("0.00") + "," +
                                custRow["OverUnder"].ToString() + "," +
                                custRow["NativeSize"].ToString() + "," +
                                custRow["CompressionRatio"].ToString() + "\r\n";
                        }
                    }
                }
                
                double diff = totalPoolGBs - totalQuotaGBs;
                double compressionRatio = totalPoolGBs > 0 ? totalNativeGBs / totalPoolGBs : 0.0;

                textBox1.Text += "\r\n,,Total GB," + totalQuotaGBs.ToString("0.0") + "," + totalPoolGBs.ToString("0.0") + ",,,,,,,,," + totalMonthlyCharge + "," + 
                    diff.ToString("0.0") + "," + totalNativeGBs.ToString("0.0") + "," + compressionRatio.ToString("0.0") +  
                    "\r\n,,Total TB," + (totalQuotaGBs / 1024).ToString("0.0") + "," + (totalPoolGBs / 1024).ToString("0.0") + ",,,,,,,,," + totalMonthlyCharge + "," + 
                    (diff / 1024).ToString("0.0") + "," + (totalNativeGBs / 1024).ToString("0.0") + "," + compressionRatio.ToString("0.0") + "\r\n";

                textBox1.Text += "\r\n\r\n" + "Single-Tenant Customers\r\n";

                totalNativeGBs = 0.0;
                totalPoolGBs = 0.0;
                totalQuotaGBs = 0.0;
                totalMonthlyCharge = 0.0;

                foreach (DataRow custRow in m_dtDirectorCustomerDetails.Rows)
                {
                    if (string.IsNullOrEmpty(reseller) || custRow["Partner"].ToString() == reseller)
                    {
                        if (custRow["SingleTenant"].ToString().ToLower() == "yes")
                        {
                            double nativeSize = Convert.ToDouble(custRow["NativeSize"].ToString());
                            double poolSize = Convert.ToDouble(custRow["PoolSize"].ToString());
                            double monthlyCharge = Convert.ToDouble(custRow["MonthlyAmt"].ToString());
                            string cycle = custRow["BillingCycle"].ToString();

                            monthlyCharge = cycle.ToLower().Equals("yearly") ? monthlyCharge / 12 : monthlyCharge;

                            totalNativeGBs += nativeSize;
                            totalPoolGBs += poolSize;
                            totalMonthlyCharge += monthlyCharge;

                            totalQuotaGBs += poolSize > 0.0 ? Convert.ToDouble(custRow["Quota"].ToString()) : 0.0;

                            textBox1.Text += "\"" + custRow["CustomerName"].ToString() + "\"," +
                                custRow["CWID"].ToString() + ",\"" +
                                custRow["LocationCode"].ToString() + "\"," +
                                custRow["Quota"].ToString() + "," +
                                custRow["PoolSize"].ToString() + "," +
                                custRow["PercentOfQuota"].ToString() + "," +
                                custRow["SalesPerson"].ToString() + "," +
                                custRow["ManagedServiceCustomer"].ToString() + "," +
                                custRow["SignedContract"].ToString() + "," +
                                custRow["SatelliteAppliance"].ToString() + "," +
                                custRow["SingleTenant"].ToString() + "," +
                                custRow["Partner"].ToString() + "," +
                                custRow["BillingSize"].ToString() + "," +
                                monthlyCharge.ToString("0.00") + "," +
                                custRow["OverUnder"].ToString() + "," +
                                custRow["NativeSize"].ToString() + "," +
                                custRow["CompressionRatio"].ToString() + "\r\n";
                        }
                    }
                }

                diff = totalPoolGBs - totalQuotaGBs;
                compressionRatio = totalPoolGBs > 0 ? totalNativeGBs / totalPoolGBs : 0.0;

                textBox1.Text += "\r\n,,Total GB," + totalQuotaGBs.ToString("0.0") + "," + totalPoolGBs.ToString("0.0") + ",,,,,,,,," + totalMonthlyCharge + "," +
                    diff.ToString("0.0") + "," + totalNativeGBs.ToString("0.0") + "," + compressionRatio.ToString("0.0") +
                    "\r\n,,Total TB," + (totalQuotaGBs / 1024).ToString("0.0") + "," + (totalPoolGBs / 1024).ToString("0.0") + ",,,,,,,,," + totalMonthlyCharge + "," +
                    (diff / 1024).ToString("0.0") + "," + (totalNativeGBs / 1024).ToString("0.0") + "," + compressionRatio.ToString("0.0") + "\r\n";
            }
        }

        private void generatePoolLocation()
        {
            if (m_dtDirectorCustomerLocationDetailsBasic != null)
            {
                m_dtDirectorCustomerLocationDetailsBasic.Dispose();
                m_dtDirectorCustomerLocationDetailsBasic = null;
            }
        
            getDirectorCustomerLocationDetailsBasicTable();

            textBox1.Clear();

            textBox1.Text += "Customer,Location,Pool Size (GB)\r\n\r\n";

            if (m_dtDirectorCustomerLocationDetailsBasic != null && m_dtDirectorCustomerLocationDetailsBasic.Rows != null && m_dtDirectorCustomerLocationDetailsBasic.Rows.Count > 0)
            {
                double totalPoolGB = 0.0;

                foreach (DataRow custRow in m_dtDirectorCustomerLocationDetailsBasic.Rows)
                {                    
                    totalPoolGB += Convert.ToDouble(custRow["PoolSize"]);

                    textBox1.Text += "\"" + custRow["CustomerName"].ToString() + "\",\"" +
                        custRow["LocationName"].ToString() + "\"," +
                        custRow["PoolSize"].ToString() + "\r\n";             
                }

                textBox1.Text += "\r\n,Total GB," + totalPoolGB.ToString("0.0") + "\r\n,Total TB," + (totalPoolGB / 1024).ToString("0.0");
            }
        }

        private DataTable getBaseVaultsUsageTable(string[] vaults)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("VaultName");
            dt.Columns.Add("GBNativeUsage", typeof(double));
            dt.Columns.Add("GBPoolUsage", typeof(double));
            dt.Columns.Add("CompressionRatio", typeof(double));
            dt.Columns.Add("TotalPoolUsagePercent", typeof(double));
            dt.Columns.Add("TBPoolUsage", typeof(double));
            dt.Columns.Add("Customers", typeof(int));
            dt.Columns.Add("CustomersPoolAverage", typeof(double));
            dt.Columns.Add("Maintenance", typeof(string));

            dt.AcceptChanges();

            if (vaults != null && vaults.Count() > 0)
            {
                foreach (String vault in vaults)
                {
                    Thread.Sleep(m_slowMyAssDownSon);

                    long vaultPoolBytes = 0;
                    long vaultNativeBytes = 0;

                    DataRow dr = dt.NewRow();

                    textBoxVault.Text = vault;
                    directorLogin();

                    CVault firstVault = m_cmManager.getFirstVault(m_cvcConnector);

                    dr["VaultName"] = firstVault.HostName;

                    dr["Maintenance"] = m_cmManager.getMaintenanceStatus(m_cvcConnector) ? "Yes" : "No";

                    dynamic customerList = m_cmManager.getCustomerList(m_cvcConnector);

                    CCustomerCollection customerCollection = (CCustomerCollection)customerList;

                    if (customerCollection != null && customerCollection.Count > 0)
                    {
                        foreach (CCustomer customer in customerCollection)
                        {
                            Thread.Sleep(m_slowMyAssDownSon);

                            ICollection lColl = m_cmManager.getLocationList(m_cvcConnector, customer.Id);

                            CLocationCollection locations = (CLocationCollection)lColl;

                            if (locations != null && locations.Count > 0)
                            {
                                foreach (CLocation location in locations)
                                {
                                    Thread.Sleep(m_slowMyAssDownSon);

                                    ICollection cColl = m_cmManager.getComputerList(m_cvcConnector, location.Id);

                                    CComputerCollection computers = (CComputerCollection)cColl;

                                    if (computers != null && computers.Count > 0)
                                    {
                                        foreach (CComputer computer in computers)
                                        {
                                            Thread.Sleep(m_slowMyAssDownSon);

                                            ICollection tColl = m_cmManager.getTaskList(m_cvcConnector, computer.Id);

                                            CTaskCollection tasks = (CTaskCollection)tColl;

                                            if (tasks != null && tasks.Count > 0)
                                            {
                                                Thread.Sleep(m_slowMyAssDownSon);

                                                foreach (CTask task in tasks)
                                                {
                                                    Thread.Sleep(m_slowMyAssDownSon);
                                                 
                                                    vaultPoolBytes += task.PhysicalPoolSize;
                                                    
                                                    ICollection sColl = m_cmManager.getSafesetList(m_cvcConnector, task.Id);

                                                    CSafesetCollection safesets = (CSafesetCollection)sColl;
                                                                                                      
                                                    if (safesets != null && safesets.Count > 0)
                                                    {
                                                        for (int i = safesets.Count - 1; i > -1; i--)
                                                        {
                                                            CSafeset safeset = (CSafeset)safesets[i];

                                                            if (safeset.IsOnline)
                                                            {
                                                                vaultNativeBytes += safeset.OriginalBytes;

                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    double poolGB = vaultPoolBytes / (1024 * 1024 * 1024);
                    double nativeGB = vaultNativeBytes / (1024 * 1024 * 1024);
                    double compressionRatio = poolGB > 0.0 ? nativeGB / poolGB : 0.0;

                    dr["GBNativeUsage"] = nativeGB;
                    dr["GBPoolUsage"] = poolGB;
                    dr["CompressionRatio"] = compressionRatio;
                    dr["TBPoolUsage"] = poolGB / 1024;
                    dr["Customers"] = customerCollection.Count;
                    dr["CustomersPoolAverage"] = customerCollection.Count > 0 ? poolGB / customerCollection.Count : 0.0;

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
            }

            return dt;
        }

        private void generateBaseVaultsUsageExtended()
        {
            m_formShown = false;

            try
            {
                string[] vaults = File.ReadAllLines(textBoxWritePath.Text + "\\" + m_baseVaultsFileName);

                textBox1.Clear();
                textBox1.Text = "Dakota Backup: Base Vaults Usage Report\r\n" +
                    "Report Generated: " + DateTime.Now.ToString() + "\r\n" + 
                    "Note: Does not include privately implemented Base Vaults at customer locations\r\n\r\n" +
                    "Vault Name,Native Usage (GB),Pool Usage (GB),Comp. Ratio,% Total Pool Usage,Pool Usage (TB),Customers,Avg. Customer Pool Usage (GB)\r\n\r\n";

                DataTable dt = getBaseVaultsUsageTable(vaults);

                double totalPoolBytes = 0.0;
                double totalNativeBytes = 0.0;
                double totalPoolGB = 0.0;
                double totalNativeGB = 0.0;
                double totalPoolTB = 0.0;
                double totalCustomersPoolAverage = 0.0;
                int totalCustomers = 0;

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        totalPoolGB += Convert.ToDouble(dr["GBPoolUsage"]);
                        totalNativeGB += Convert.ToDouble(dr["GBNativeUsage"]);
                        totalCustomers += (Convert.ToInt32(dr["Customers"]));
                    }
                }

                totalPoolBytes = totalPoolGB * 1024 * 1024 * 1024;
                totalNativeBytes = totalNativeGB * 1024 * 1024 * 1024;
                totalPoolTB += totalPoolGB / 1024;
                totalCustomersPoolAverage = totalCustomers > 0 ? totalPoolGB / totalCustomers : 0.0;

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        double percent = ((double)dr["GBPoolUsage"] / totalPoolGB) * 100;

                        textBox1.Text +=
                            dr["VaultName"].ToString().Replace("'", "''") + ",\"" +
                            ((double)dr["GBNativeUsage"]).ToString("0.0") + "\",\"" +
                            ((double)dr["GBPoolUsage"]).ToString("0.0") + "\",\"" +
                            ((double)dr["CompressionRatio"]).ToString("0.0") + "\",\"" +
                            percent.ToString("0.0") + "\",\"" +
                            ((double)dr["TBPoolUsage"]).ToString("0.0") + "\"," +
                            dr["Customers"].ToString() + ",\"" +
                            ((double)dr["CustomersPoolAverage"]).ToString("0.0") + "\"\r\n";
                    }

                    double totalCompressionRatio = totalPoolGB > 0.0 ? totalNativeGB / totalPoolGB : 0.0;

                    textBox1.Text += 
                        "\r\nTotal,\"" + 
                        totalNativeGB.ToString("0.0") + "\",\"" + 
                        totalPoolGB.ToString("0.0") + "\",\"" + 
                        totalCompressionRatio.ToString("0.0") + "\",\"" +
                        "100.0\",\"" + 
                        totalPoolTB.ToString("0.0") + "\"," + 
                        totalCustomers.ToString() + ",\"" + 
                        totalCustomersPoolAverage.ToString("0.0") + "\"";
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            m_formShown = true;
        }

        private void generateBaseVaultsUsageVendor()
        {
            m_formShown = false;

            try
            {
                string[] vaults = File.ReadAllLines(textBoxWritePath.Text + "\\" + m_baseVaultsFileName);

                textBox1.Clear();
                textBox1.Text = "Dakota Backup: Base Vaults Usage Report\r\n" +
                    "Report Generated: " + DateTime.Now.ToString() + "\r\n" +
                    "Note: Does not include privately implemented Base Vaults at customer locations\r\n\r\n" +
                    "Vault Name,Pool Usage (GB),Pool Usage (TB),\r\n\r\n";

                DataTable dt = getBaseVaultsUsageTable(vaults);

                if (dt.Rows.Count > 0)
                {
                    double totalPoolGB = 0.0;
                    double totalPoolTB = 0.0;

                    foreach (DataRow dr in dt.Rows)
                    {
                        textBox1.Text +=
                            dr["VaultName"].ToString().Replace("'", "''") + ",\"" +
                            ((double)dr["GBPoolUsage"]).ToString("0.0") + "\",\"" +
                            ((double)dr["TBPoolUsage"]).ToString("0.0") + "\"\r\n";

                        totalPoolGB += ((double)dr["GBPoolUsage"]);
                    }

                    totalPoolTB = totalPoolGB > 0.0 ? totalPoolGB / 1024 : 0.0;

                    textBox1.Text += 
                        "\r\nTotal,\"" + 
                        totalPoolGB.ToString("0.0") + "\",\"" +  
                        totalPoolTB.ToString("0.0") + "\"";
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            m_formShown = true;
        }

        private void getDirectorCustomerDetailsTable(string query)
        {
            m_dtDirectorCustomerDetails = new DataTable();

            m_dtDirectorCustomerDetails.Columns.Add("CustomerName");
            m_dtDirectorCustomerDetails.Columns.Add("CustomerID", typeof(uint));
            m_dtDirectorCustomerDetails.Columns.Add("CWID");
            m_dtDirectorCustomerDetails.Columns.Add("ManagedServiceCustomer");
            m_dtDirectorCustomerDetails.Columns.Add("SignedContract");
            m_dtDirectorCustomerDetails.Columns.Add("SalesPerson");
            m_dtDirectorCustomerDetails.Columns.Add("BillingSize", typeof(int));
            m_dtDirectorCustomerDetails.Columns.Add("MonthlyAmt", typeof(double));
            m_dtDirectorCustomerDetails.Columns.Add("BillingCycle");
            m_dtDirectorCustomerDetails.Columns.Add("SatelliteAppliance");
            m_dtDirectorCustomerDetails.Columns.Add("SingleTenant");
            m_dtDirectorCustomerDetails.Columns.Add("Partner");
            m_dtDirectorCustomerDetails.Columns.Add("Quota", typeof(uint));
            m_dtDirectorCustomerDetails.Columns.Add("NativeSize", typeof(double));
            m_dtDirectorCustomerDetails.Columns.Add("PoolSize", typeof(double));
            m_dtDirectorCustomerDetails.Columns.Add("CompressionRatio", typeof(double));
            m_dtDirectorCustomerDetails.Columns.Add("PercentOfQuota", typeof(double));
            m_dtDirectorCustomerDetails.Columns.Add("OverUnder", typeof(double));
            m_dtDirectorCustomerDetails.Columns.Add("SatelliteOperatingMode", typeof(SatelliteOperatingMode));
            m_dtDirectorCustomerDetails.Columns.Add("SatelliteGUID");
            m_dtDirectorCustomerDetails.Columns.Add("LocationCode");

            try
            {
                dynamic customerList = m_cmManager.getCustomerList(m_cvcConnector);

                CCustomerCollection customerCollection = (CCustomerCollection)customerList;

                if (customerCollection != null && customerCollection.Count > 0)
                {
                    foreach (CCustomer customer in customerCollection)
                    {                    
                        dynamic satConfig = null;
                        
                        CReplicationSatelliteConfig config = null;

                        try 
                        {
                            satConfig = m_cmManager.getSatelliteConfiguration(m_cvcConnector, customer.Id);

                            config = (CReplicationSatelliteConfig)satConfig; 
                        }
                        catch { }

                        DataRow newRow = m_dtDirectorCustomerDetails.NewRow();
                      
                        newRow["CustomerName"] = customer.Name;
                        newRow["CustomerID"] = customer.Id;
                        newRow["SatelliteOperatingMode"] = config == null ? SatelliteOperatingMode.RestoreOnly : config.BackupOption;
                        newRow["SatelliteGUID"] = config == null ? Guid.Empty.ToString() : config.GUID.ToString();

                        string[] noteInfos = customer.Notes.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                        int index = noteInfos[0].IndexOf(":");

                        if (index > 0)
                        {
                            string billingCycle = "";
                            double monthlyAmt = 0.0;

                            newRow["CWID"] = noteInfos[0].Substring(index + 2).Trim();

                            string cwFilter = "AccountID = '" + newRow["CWID"].ToString() + "'";

                            DataRow[] cwRows = null;

                            if (m_dtCWTable != null && m_dtCWTable.Rows.Count > 0)
                            {
                                cwRows = m_dtCWTable.Select(cwFilter);
                            }

                            if (cwRows != null && cwRows.Count() > 0)
                            {
                                billingCycle = cwRows[0]["BillingCycle"].ToString();
                                monthlyAmt = Convert.ToDouble(cwRows[0]["MonthlyAmt"].ToString());
                            }

                            newRow["BillingCycle"] = billingCycle;
                            newRow["MonthlyAmt"] = monthlyAmt;

                            index = noteInfos[1].IndexOf(":");

                            newRow["SignedContract"] = noteInfos[1].Substring(index + 2).Trim();

                            index = noteInfos[2].IndexOf(":");

                            newRow["BillingSize"] = Convert.ToInt32(noteInfos[2].Substring(index + 2).Trim());

                            index = noteInfos[3].IndexOf(":");

                            newRow["ManagedServiceCustomer"] = noteInfos[3].Substring(index + 2).Trim();

                            index = noteInfos[4].IndexOf(":");

                            newRow["SalesPerson"] = noteInfos[4].Substring(index + 2).Trim();

                            index = noteInfos[5].IndexOf(":");

                            newRow["SatelliteAppliance"] = noteInfos[5].Substring(index + 2).Trim();

                            index = noteInfos[6].IndexOf(":");

                            newRow["SingleTenant"] = noteInfos[6].Substring(index + 2).Trim();

                            if (noteInfos.Count() > 7)
                            {
                                index = noteInfos[7].IndexOf(":");

                                newRow["Partner"] = noteInfos[7].Substring(index + 2).Trim();
                            }
                            else
                            {
                                newRow["Partner"] = "";
                            }

                            uint q = m_cmManager.GetCustomerQuota(m_cvcConnector, customer, "STORAGE");

                            if (q == uint.MaxValue)
                            {
                                q = 0;
                            }

                            newRow["Quota"] = q;
                        }
                        else
                        {
                            newRow["Quota"] = 0;
                        }

                        ICollection lColl = m_cmManager.getLocationList(m_cvcConnector, customer.Id);

                        CLocationCollection locations = (CLocationCollection)lColl;

                        if (locations != null && locations.Count > 0)
                        {
                            newRow["LocationCode"] = ((CLocation)locations[0]).BillingCode;
                        }
                        
                        m_dtDirectorCustomerDetails.Rows.Add(newRow);
                        m_dtDirectorCustomerDetails.AcceptChanges();
                    }
                }

                if (!string.IsNullOrEmpty(query) && m_dtDirectorCustomerDetails != null && m_dtDirectorCustomerDetails.Rows != null)
                {
                    DataRow[] selected = m_dtDirectorCustomerDetails.Select(query);

                    DataTable temp = m_dtDirectorCustomerDetails.Clone();

                    foreach (DataRow row in selected)
                    {
                        temp.ImportRow(row);
                    }

                    temp.AcceptChanges();

                    m_dtDirectorCustomerDetails.Rows.Clear();
                    m_dtDirectorCustomerDetails.AcceptChanges();

                    m_dtDirectorCustomerDetails = temp.Copy();
                    m_dtDirectorCustomerDetails.AcceptChanges();

                    temp.Dispose();
                    temp = null;
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void getDirectorTasksDetailsTable(string query)
        {
            if (m_dtDirectorTasksDetails == null)
            {
                m_dtDirectorTasksDetails = new DataTable();

                DataColumn pKey = new DataColumn("pKey");
               
                m_dtDirectorTasksDetails.Columns.Add("AccountID");                
                m_dtDirectorTasksDetails.Columns.Add("CustomerID", typeof(uint));
                m_dtDirectorTasksDetails.Columns.Add("CustomerName");
                m_dtDirectorTasksDetails.Columns.Add("LocationName");
                m_dtDirectorTasksDetails.Columns.Add("LocationID", typeof(uint));
                m_dtDirectorTasksDetails.Columns.Add("ComputerName");
                m_dtDirectorTasksDetails.Columns.Add("DComputerID", typeof(uint));
                m_dtDirectorTasksDetails.Columns.Add("TaskName");
                m_dtDirectorTasksDetails.Columns.Add("DTaskID", typeof(uint));
                m_dtDirectorTasksDetails.Columns.Add("NativeSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("PoolSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("CompressionRatio", typeof(double));
                m_dtDirectorTasksDetails.Columns.Add("UsedPoolSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("DeltaCompressedSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("OriginalSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("StorageSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("TotalCompressedSize", typeof(long));
                m_dtDirectorTasksDetails.Columns.Add("TaskPaths");
                m_dtDirectorTasksDetails.Columns.Add("TaskOperatingMode", typeof(OperatingModeType));
                m_dtDirectorTasksDetails.Columns.Add("NumberOfSafesets", typeof(int));
                m_dtDirectorTasksDetails.Columns.Add("LastSafesetDate", typeof(DateTime));
                m_dtDirectorTasksDetails.Columns.Add("DaysSinceLastSafeset", typeof(double));
                m_dtDirectorTasksDetails.Columns.Add("FirstSafesetDate", typeof(DateTime));
                m_dtDirectorTasksDetails.Columns.Add("SatelliteGUID");
                m_dtDirectorTasksDetails.Columns.Add("SatelliteOperatingMode", typeof(SatelliteOperatingMode));
                m_dtDirectorTasksDetails.Columns.Add(pKey);

                m_dtDirectorTasksDetails.PrimaryKey = new DataColumn[] { pKey };

                m_dtDirectorTasksDetails.AcceptChanges();

                if (m_dtDirectorCustomerDetails != null && m_dtDirectorCustomerDetails.Rows != null && m_dtDirectorCustomerDetails.Rows.Count > 0)
                {
                    try
                    {
                        foreach (DataRow custRow in m_dtDirectorCustomerDetails.Rows)
                        {
                            Thread.Sleep(m_slowMyAssDownSon);

                            double nativeSize = 0.0;
                            double poolSize = 0.0;
                            
                            ICollection lColl = m_cmManager.getLocationList(m_cvcConnector, (uint)custRow["CustomerID"]);
                            
                            CLocationCollection locations = (CLocationCollection)lColl;

                            if (locations != null && locations.Count > 0)
                            {
                                Thread.Sleep(m_slowMyAssDownSon);

                                foreach (CLocation location in locations)
                                {
                                    Thread.Sleep(m_slowMyAssDownSon);

                                    string accountName = null;

                                    ICollection aColl = m_cmManager.getAccountList(m_cvcConnector, location.Id);

                                    CAccountCollection accounts = (CAccountCollection)aColl;

                                    if (accounts != null && accounts.Count > 0)
                                    {
                                        accountName = ((CAccount)accounts[0]).Name.ToLower();
                                    }

                                    Thread.Sleep(m_slowMyAssDownSon);

                                    ICollection cColl = m_cmManager.getComputerList(m_cvcConnector, location.Id);

                                    CComputerCollection computers = (CComputerCollection)cColl;

                                    if (computers != null && computers.Count > 0)
                                    {
                                        foreach (CComputer computer in computers)
                                        {
                                            Thread.Sleep(m_slowMyAssDownSon);

                                            ICollection tColl = m_cmManager.getTaskList(m_cvcConnector, computer.Id);

                                            CTaskCollection tasks = (CTaskCollection)tColl;

                                            if (tasks != null && tasks.Count > 0)
                                            {
                                                foreach (CTask task in tasks)
                                                {
                                                    Thread.Sleep(m_slowMyAssDownSon);

                                                    long nativeBytes = 0;

                                                    DataRow newRow = m_dtDirectorTasksDetails.NewRow();
                                                    
                                                    ICollection sColl = m_cmManager.getSafesetList(m_cvcConnector, task.Id);

                                                    CSafesetCollection safesets = (CSafesetCollection)sColl;

                                                    if (safesets != null && safesets.Count > 0)
                                                    {
                                                        newRow["NumberOfSafesets"] = safesets.Count;
                                                        newRow["FirstSafesetDate"] = ((CSafeset)safesets[0]).BackupTime.ToLocalTime();
                                                        newRow["LastSafesetDate"] = ((CSafeset)safesets[safesets.Count - 1]).BackupTime.ToLocalTime();
                                                        newRow["DaysSinceLastSafeset"] = DateTime.Now.Subtract((DateTime)newRow["LastSafesetDate"]).TotalDays;

                                                        long deltaCompressedSize = 0;
                                                        long originalSize = 0;
                                                        long storageSize = 0;
                                                        long totalCompressedSize = 0;

                                                        for (int i = 0; i < safesets.Count; i++)
                                                        {
                                                            CSafeset safeset = (CSafeset)safesets[i];

                                                            deltaCompressedSize += safeset.DeltaComprBytes;
                                                            originalSize += safeset.OriginalBytes;
                                                            storageSize += safeset.StorageBytes;
                                                            totalCompressedSize += safeset.TotalComprBytes;

                                                            if (safeset.IsOnline)
                                                            {
                                                                nativeBytes = safeset.OriginalBytes;
                                                            }
                                                        }

                                                        newRow["DeltaCompressedSize"] = deltaCompressedSize;
                                                        newRow["OriginalSize"] = originalSize;
                                                        newRow["StorageSize"] = storageSize;
                                                        newRow["TotalCompressedSize"] = totalCompressedSize;    
                                                    }
                                                    else
                                                    {
                                                        newRow["NumberOfSafesets"] = 0;
                                                        newRow["FirstSafesetDate"] = DateTime.MinValue;
                                                        newRow["LastSafesetDate"] = DateTime.MinValue;
                                                        newRow["DaysSinceLastSafeset"] = -1.0;

                                                        newRow["DeltaCompressedSize"] = 0;
                                                        newRow["OriginalSize"] = 0;
                                                        newRow["StorageSize"] = 0;
                                                        newRow["TotalCompressedSize"] = 0;
                                                    }

                                                    OperatingModeType taskMode = OperatingModeType.Default;
                                                    try
                                                    {
                                                        taskMode = task.OperatingMode;
                                                    }
                                                    catch { }

                                                    newRow["AccountID"] = accountName;
                                                    newRow["CustomerID"] = custRow["CustomerID"];
                                                    newRow["CustomerName"] = custRow["CustomerName"];
                                                    newRow["LocationName"] = location.Name;
                                                    newRow["LocationID"] = location.Id;
                                                    newRow["ComputerName"] = computer.OsType == "Unix" ? computer.Domain : computer.Name;
                                                    newRow["DComputerID"] = computer.Id;
                                                    newRow["TaskName"] = task.Name;
                                                    newRow["DTaskID"] = task.Id;
                                                    newRow["NativeSize"] = nativeBytes;
                                                    newRow["PoolSize"] = task.PhysicalPoolSize;
                                                    newRow["CompressionRatio"] = task.PhysicalPoolSize > 0 ? (double)nativeBytes / (double)task.PhysicalPoolSize : 0.0;
                                                    newRow["UsedPoolSize"] = task.UsedPoolSize;
                                                    newRow["TaskOperatingMode"] = taskMode;
                                                    newRow["SatelliteGUID"] = custRow["SatelliteGUID"];
                                                    newRow["SatelliteOperatingMode"] = custRow["SatelliteOperatingMode"];

                                                    nativeSize += nativeBytes;
                                                    poolSize += task.PhysicalPoolSize;

                                                    Thread.Sleep(m_slowMyAssDownSon);

                                                    ICollection pColl = m_cmManager.getTaskPath(m_cvcConnector, task.Id);

                                                    CTaskPathCollection paths = (CTaskPathCollection)pColl;

                                                    if (paths != null && paths.Count > 0)
                                                    {
                                                        foreach (CTaskPath path in paths)
                                                        {
                                                            newRow["TaskPaths"] += path.Path + ",";
                                                        }

                                                        string taskPaths = newRow["TaskPaths"].ToString();

                                                        if (taskPaths.Length > 0)
                                                            newRow["TaskPaths"] = taskPaths.Substring(0, taskPaths.Length - 1);
                                                    }

                                                    newRow["pKey"] = newRow["AccountID"].ToString() + "_" + newRow["ComputerName"].ToString().ToLower() + "_" + newRow["TaskName"].ToString().ToLower();

                                                    m_dtDirectorTasksDetails.Rows.Add(newRow);
                                                    m_dtDirectorTasksDetails.AcceptChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            double nativeGB = nativeSize / (1024 * 1024 * 1024);
                            double poolGB = poolSize / (1024 * 1024 * 1024);
                            double compressionRatio = poolGB > 0.0 ? nativeGB / poolGB : 0.0;

                            custRow["NativeSize"] = nativeGB.ToString("0.0");
                            custRow["PoolSize"] = poolGB.ToString("0.0");
                            custRow["CompressionRatio"] = compressionRatio.ToString("0.0");
                            custRow["PercentOfQuota"] = (((double)custRow["PoolSize"] / (uint)custRow["Quota"]) * 100).ToString("0.0");
                            custRow["OverUnder"] = ((double)custRow["PoolSize"] - (int)custRow["BillingSize"]).ToString("0.0");

                            m_dtDirectorCustomerDetails.AcceptChanges();
                        }


                        

                        /////////////- - added 2/2/15
                        //
                        if (m_dtFilteredMegaTasksDetails != null)
                        {
                            m_dtFilteredMegaTasksDetails.Dispose();
                            m_dtFilteredMegaTasksDetails = null;
                        }

                        m_dtFilteredMegaTasksDetails = new DataTable();

                        foreach (DataColumn dc in m_dtDirectorTasksDetails.Columns)
                        {
                            m_dtFilteredMegaTasksDetails.Columns.Add(dc.ColumnName, dc.DataType);
                        }

                        m_dtFilteredMegaTasksDetails.AcceptChanges();

                        DataRow[] rows = null;

                        if (!string.IsNullOrEmpty(query))
                        {
                            rows = m_dtDirectorTasksDetails.Select(query);
                        }
                        else
                        {
                            rows = m_dtDirectorTasksDetails.Select("");
                        }

                        if (rows != null && rows.Count() > 0)
                        {
                            foreach (DataRow dr in rows)
                            {
                                m_dtFilteredMegaTasksDetails.ImportRow(dr);
                            }

                            m_dtFilteredMegaTasksDetails.AcceptChanges();

                            if (m_formShown)
                            {
                                this.Invoke(new MethodInvoker(() => groupBoxDataGrid.Text = rows.Count().ToString() + " results as of: " + DateTime.Now.ToString()));
                            }
                        }

                        if (m_formShown)
                        {
                            this.Invoke(new MethodInvoker(() => dataGridViewResults.DataSource = m_dtFilteredMegaTasksDetails));
                        }
  


                    }
                    catch (Exception ex)
                    {
                        if (m_formShown)
                        {
                            MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                        }

                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                    }
                }
            }
        }

        private void getDirectorCustomerLocationDetailsBasicTable()
        {
            m_dtDirectorCustomerLocationDetailsBasic = new DataTable();

            m_dtDirectorCustomerLocationDetailsBasic.Columns.Add("CustomerName");
            m_dtDirectorCustomerLocationDetailsBasic.Columns.Add("LocationName");
            m_dtDirectorCustomerLocationDetailsBasic.Columns.Add("PoolSize", typeof(double));

            try
            {
                dynamic customerList = m_cmManager.getCustomerList(m_cvcConnector);

                CCustomerCollection customerCollection = (CCustomerCollection)customerList;

                if (customerCollection != null && customerCollection.Count > 0)
                {
                    foreach (CCustomer customer in customerCollection)
                    {                                             
                        ICollection lColl = m_cmManager.getLocationList(m_cvcConnector, customer.Id);

                        CLocationCollection locations = (CLocationCollection)lColl;

                        if (locations != null && locations.Count > 0)
                        {
                            foreach (CLocation location in locations)
                            {
                                double poolSize = 0.0;

                                DataRow newRow = m_dtDirectorCustomerLocationDetailsBasic.NewRow();

                                newRow["CustomerName"] = customer.Name;
                                newRow["LocationName"] = location.Name;

                                ICollection cColl = m_cmManager.getComputerList(m_cvcConnector, location.Id);

                                CComputerCollection computers = (CComputerCollection)cColl;

                                if (computers != null && computers.Count > 0)
                                {
                                    foreach (CComputer computer in computers)
                                    {
                                        ICollection tColl = m_cmManager.getTaskList(m_cvcConnector, computer.Id);

                                        CTaskCollection tasks = (CTaskCollection)tColl;

                                        if (tasks != null && tasks.Count > 0)
                                        {
                                            foreach (CTask task in tasks)
                                            {
                                                poolSize += task.PhysicalPoolSize;
                                            }
                                        }
                                    }
                                }

                                double poolGB = poolSize / (1024 * 1024 * 1024);

                                newRow["PoolSize"] = poolGB.ToString("0.0");

                                m_dtDirectorCustomerLocationDetailsBasic.Rows.Add(newRow);
                                m_dtDirectorCustomerLocationDetailsBasic.AcceptChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }

        private void fillCWTable()
        {
            if (m_dtCWTable != null)
            {
                m_dtCWTable.Dispose();
                m_dtCWTable = null;
            }

            m_dtCWTable = new DataTable();

            using (SqlConnection conn = new SqlConnection(m_cwConnStr))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM KTC_Backup_Billing2", conn))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(m_dtCWTable);
                    }
                }
            }
        }

        private void syncEVaultWithCW()
        {
            try
            {
                fillCWTable();

                dynamic customerList = m_cmManager.getCustomerList(m_cvcConnector);

                CCustomerCollection customerCollection = (CCustomerCollection)customerList;

                if (customerCollection != null && customerCollection.Count > 0)
                {
                    foreach (CCustomer customer in customerCollection)
                    {
                        string notes = customer.Notes;

                        if (!string.IsNullOrEmpty(notes))
                        {
                            string[] noteInfos = notes.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                            if (noteInfos != null && noteInfos.Count() > 5)
                            {
                                int index = 0;

                                index = noteInfos[0].IndexOf(":");

                                if (index > -1)
                                {
                                    string accountID = noteInfos[0].Substring(index + 2).Trim();

                                    string hasContract = "No";

                                    try
                                    {
                                        index = noteInfos[1].IndexOf(":");
                                        hasContract = noteInfos[1].Substring(index + 2).Trim();
                                    }
                                    catch { }

                                    string billingGB = "";

                                    try
                                    {
                                        index = noteInfos[2].IndexOf(":");
                                        billingGB = noteInfos[2].Substring(index + 2).Trim();
                                    }
                                    catch { }

                                    string isManaged = "No"; 

                                    try
                                    {
                                        index = noteInfos[3].IndexOf(":");
                                        isManaged = noteInfos[3].Substring(index + 2).Trim();
                                    }
                                    catch { }

                                    string salesID = "";

                                    try
                                    {
                                        index = noteInfos[4].IndexOf(":");
                                        salesID = noteInfos[4].Substring(index + 2).Trim();
                                    }
                                    catch { }

                                    string hasAppliance = "No";

                                    try
                                    {
                                        index = noteInfos[5].IndexOf(":");
                                        hasAppliance = noteInfos[5].Substring(index + 2).Trim();
                                    }
                                    catch { }

                                    string isSingleTenant = "No";
                     
                                    try
                                    {
                                        index = noteInfos[6].IndexOf(":");
                                        isSingleTenant = noteInfos[6].Substring(index + 2).Trim(); 
                                    }
                                    catch { }

                                    string partner = "";

                                    try
                                    {
                                        index = noteInfos[7].IndexOf(":");
                                        partner = noteInfos[7].Substring(index + 2).Trim();
                                    }
                                    catch { }

                                    string expression = "AccountID = '" + accountID + "'";

                                    DataRow[] foundIt = m_dtCWTable.Select(expression);

                                    if (foundIt != null && foundIt.Count() > 0)
                                    {
                                        billingGB = foundIt[0]["BillingGB"].ToString();
                                        salesID = foundIt[0]["SalesID"].ToString();
                                    }

                                    billingGB = string.IsNullOrEmpty(billingGB) ? "0" : billingGB;

                                    customer.Notes =
                                        "CW Company ID: " + accountID + "\r\n" +
                                        "Signed Contract on File: " + hasContract + "\r\n" +
                                        "Current Billing Block Size: " + billingGB + "\r\n" +
                                        "Managed Service Customer: " + isManaged + "\r\n" +
                                        "Account Sales Person: " + salesID + "\r\n" +
                                        "Satellite Appliance: " + hasAppliance + "\r\n" +
                                        "Single-Tenant: " + isSingleTenant + "\r\n" + 
                                        "Partner: " + partner;
                                }
                            }
                        }
                        else
                        {
                            customer.Notes =
                                "CW Company ID: \r\n" +
                                "Signed Contract on File: No\r\n" +
                                "Current Billing Block Size: 0\r\n" +
                                "Managed Service Customer: No\r\n" +
                                "Acount Sales Person: \r\n" +
                                "Satellite Appliance: No\r\n" +
                                "Single-Tenant: No\r\n" + 
                                "Partner: ";
                        }

                        customer.update(m_cvcConnector);
                    }
                }
            }
            catch (Exception ex)
            {
                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        } 
        #endregion


        #region Asigra Methods
        private void nocLogin()
        {
            m_nocApi = new NOCApiService();
            
            m_nocLogin = new LoginInfo();

            m_nocApi.Url = textBoxNOC.Text + "/services/PortalApiService";

            m_nocLogin.language = "English";
            m_nocLogin.username = textBoxNOCUsername.Text.Trim();
            m_nocLogin.password = textBoxNOCPassword.Text;

            string response = "";

            try
            {
                response = m_nocApi.testConnection(m_nocLogin, "Welcome to the NOC");
            }
            catch (Exception ex)
            {
                m_loggedIntoNOC = false;

                if (m_formShown)
                {
                    MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                }

                System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            if (response.CompareTo("Welcome to the NOC") == 0)
            {
                m_loggedIntoNOC = true;

                if (m_formShown)
                {
                    MessageBox.Show("Welcome to the NOC, " + m_nocLogin.username);
                }

                if (m_dtNOCSetsDetails != null)
                {
                    m_dtNOCSetsDetails.Dispose();
                    m_dtNOCSetsDetails = null;
                }

                fillSystems();
            }
        }
        private void fillSystems()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Id");
            dt.Columns.Add("Name");

            DSSystem[] systems = null;

            try
            {
                systems = m_nocApi.getDSSystemList(m_nocLogin, 0, true);
            }
            catch { }

            if (systems != null && systems.Count() > 0)
            {
                foreach (DSSystem system in systems)
                {
                    DataRow dr = dt.NewRow();

                    dr["Id"] = system.systemId;
                    dr["Name"] = system.systemName;

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
            }

            comboBoxSystems.DataSource = dt;
            comboBoxSystems.DisplayMember = "Name";
        }
        private void getNOCSetsDetailsTable()
        {
            if (m_dtNOCSetsDetails == null)
            {
                m_dtNOCSetsDetails = new DataTable();

                m_dtNOCSetsDetails.Columns.Add("ADSSystemName");
                m_dtNOCSetsDetails.Columns.Add("ACustomerName");
                m_dtNOCSetsDetails.Columns.Add("ADSClient");
                m_dtNOCSetsDetails.Columns.Add("ADSCDescription");
                m_dtNOCSetsDetails.Columns.Add("ASetName");
                m_dtNOCSetsDetails.Columns.Add("LastBackupTime", typeof(DateTime));
                m_dtNOCSetsDetails.Columns.Add("DaysSinceLastBackup", typeof(double));
                m_dtNOCSetsDetails.Columns.Add("LastBackupResult");
                m_dtNOCSetsDetails.Columns.Add("LastBackupStopReason");

                m_dtNOCSetsDetails.AcceptChanges();

                DSSystem[] systems = m_nocApi.getDSSystemList(m_nocLogin, 51, true);

                if (systems != null)
                {
                    foreach (DSSystem system in systems)
                    {
                        Customer[] customers = null;
                        
                        try 
                        {
                            customers = m_nocApi.getAccountList(m_nocLogin, 0, true, system.systemId, system.systemIdSpecified);
                        }
                        catch {}

                        if (customers != null)
                        {
                            foreach (Customer customer in customers)
                            {
                                DSClient[] clients = m_nocApi.getDSClientList(m_nocLogin, 0, true, system.systemId, system.systemIdSpecified,
                                    customer.accountNum, true, true, true, true, true, true);

                                if (clients != null)
                                {
                                    foreach (DSClient client in clients)
                                    {
                                        CustomerBackupStatus[] statuses = m_nocApi.getCustomerBackupStatus(m_nocLogin, system.systemId, system.systemIdSpecified,
                                            null, client.DSBoxNumber, null, false, null, false, true, true, true, true, false, true, 0, true);

                                        if (statuses != null)
                                        {
                                            foreach (CustomerBackupStatus status in statuses)
                                            {
                                                if (!string.IsNullOrEmpty(status.BSetName))
                                                {
                                                    DataRow newRow = m_dtNOCSetsDetails.NewRow();

                                                    newRow["ADSSystemName"] = system.systemName;
                                                    newRow["ACustomerName"] = customer.accountName;
                                                    newRow["ADSClient"] = client.DSBoxNumber;
                                                    newRow["ADSCDescription"] = client.description;
                                                    newRow["ASetName"] = status.BSetName;
                                                    newRow["LastBackupTime"] = (DateTime)status.lastBackup;
                                                    newRow["DaysSinceLastBackup"] = DateTime.Now.Subtract((DateTime)status.lastBackup).TotalDays;

                                                    string completion = "";
                                                    string stopReason = "";

                                                    switch (status.completion)
                                                    {
                                                        case 0:
                                                            completion = "Successful";
                                                            break;
                                                        case -1:
                                                            completion = "Premature";
                                                            break;
                                                        case -2:
                                                            completion = "Running";
                                                            break;
                                                        case -3:
                                                            completion = "Unexpected stop";
                                                            break;
                                                        case -10:
                                                            completion = "Completed with errors";
                                                            break;
                                                        case -11:
                                                            completion = "Completed with warnings";
                                                            break;
                                                        case -12:
                                                            completion = "Completed with errors and warnings";
                                                            break;
                                                        default:
                                                            completion = "With Errors";
                                                            break;
                                                    }

                                                    switch (status.clientStopReason)
                                                    {
                                                        case -1:
                                                            stopReason = "Unknown";
                                                            break;
                                                        case 0:
                                                            stopReason = "Successful";
                                                            break;
                                                        case 1:
                                                            stopReason = "User stopped backup";
                                                            break;
                                                        case 2:
                                                            stopReason = "'Stop before' time reached";
                                                            break;
                                                        case 3:
                                                            stopReason = "Too many errors";
                                                            break;
                                                        case 4:
                                                            stopReason = "Online limit reached";
                                                            break;
                                                        case 5:
                                                            stopReason = "System shutting down";
                                                            break;
                                                        case 6:
                                                            stopReason = "Lost connection to DS-System";
                                                            break;
                                                        case 7:
                                                            stopReason = "Lost connection to Host";
                                                            break;
                                                        case 8:
                                                            stopReason = "Internal error (exception)";
                                                            break;
                                                        case 9:
                                                            stopReason = "Failed to connect to DS-System";
                                                            break;
                                                        case 10:
                                                            stopReason = "Pre-execution failure";
                                                            break;
                                                        case 11:
                                                            stopReason = "Scheduled backup out of sync";
                                                            break;
                                                        case 12:
                                                            stopReason = "The shared backup set was deleted";
                                                            break;
                                                        case 13:
                                                            stopReason = "Oracle mounted or open - cannot restore the controlfile";
                                                            break;
                                                        case 14:
                                                            stopReason = "Oracle database not open - cannot backup";
                                                            break;
                                                        case 15:
                                                            stopReason = "Could not allocate space for database";
                                                            break;
                                                        case 16:
                                                            stopReason = "Could not find catroot directory";
                                                            break;
                                                        case 17:
                                                            stopReason = "Could not open catalog database";
                                                            break;
                                                        case 18:
                                                            stopReason = "The backup set was locked";
                                                            break;
                                                        case 19:
                                                            stopReason = "Auto upgrade in process";
                                                            break;
                                                        case 20:
                                                            stopReason = "Customer storage quota reached";
                                                            break;
                                                        case 21:
                                                            stopReason = "Client storage quota reached";
                                                            break;
                                                        case 22:
                                                            stopReason = "DS-Client failed to retrieve the boot drive of target computer";
                                                            break;
                                                        case 23:
                                                            stopReason = "Unexpected shutdown";
                                                            break;
                                                        case 24:
                                                            stopReason = "Sync is not allowed";
                                                            break;
                                                        case 25:
                                                            stopReason = "Not enough disk space on DS-Client machine";
                                                            break;
                                                        case 26:
                                                            stopReason = "No local storage path";
                                                            break;
                                                        case 27:
                                                            stopReason = "Failed to connect to backup set source";
                                                            break;
                                                        case 28:
                                                            stopReason = "Yield to another activity";
                                                            break;
                                                        case 29:
                                                            stopReason = "Incorrect initial backup buffer";
                                                            break;
                                                        case 30:
                                                            stopReason = "DS-System requested stop";
                                                            break;
                                                        case 31:
                                                            stopReason = "The required metadata is not available";
                                                            break;
                                                        default:
                                                            break;
                                                    }

                                                    newRow["LastBackupResult"] = completion;
                                                    newRow["LastBackupStopReason"] = stopReason;

                                                    m_dtNOCSetsDetails.Rows.Add(newRow);
                                                    m_dtNOCSetsDetails.AcceptChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void getMegaSetsDetailsTable(string query)
        {
            if (m_dtMegaSetsDetails == null)
            {
                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(() => textBox1.Clear()));

                    this.Invoke(new MethodInvoker(() => textBox1.Text += DateTime.Now.ToLongTimeString() + "\tStart\r\n"));
                    this.Invoke(new MethodInvoker(() => textBox1.Update()));
                }

                if (m_dtNOCSetsDetails == null)
                {
                    getNOCSetsDetailsTable();
                }

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(() => textBox1.Text += DateTime.Now.ToLongTimeString() + "\tFinished Getting NOC Sets Details\r\n"));
                    this.Invoke(new MethodInvoker(() => textBox1.Update()));
                }

                m_dtMegaSetsDetails = new DataTable();
                m_dtMegaSetsDetails = m_dtNOCSetsDetails.Copy();

                DataTable tempTable = new DataTable();
                tempTable = m_dtMegaSetsDetails.Clone();
            }

            string filter = query;
            string sort = "ADSSystemName ASC, ADSClient ASC, ASetName ASC";

            if (string.IsNullOrEmpty(filter))
            {
                filter = textBoxQuery.Text;
            }

            DataRow[] rows = null;

            try
            {
                if (m_dtMegaSetsDetails.Rows.Count > 0)
                {
                    rows = m_dtMegaSetsDetails.Select(filter, sort);
                }
            }
            catch (Exception ex) { System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error); }

            if (m_dtFilteredMegaSetsDetails != null)
            {
                m_dtFilteredMegaSetsDetails.Dispose();
                m_dtFilteredMegaSetsDetails = null;
            }

            m_dtFilteredMegaSetsDetails = new DataTable();

            foreach (DataColumn dc in m_dtMegaSetsDetails.Columns)
            {
                m_dtFilteredMegaSetsDetails.Columns.Add(dc.ColumnName, dc.DataType);
            }

            m_dtFilteredMegaSetsDetails.AcceptChanges();

            if (rows != null && rows.Count() > 0)
            {
                foreach (DataRow dr in rows)
                {
                    m_dtFilteredMegaSetsDetails.ImportRow(dr);
                }

                m_dtFilteredMegaSetsDetails.AcceptChanges();

                if (m_formShown)
                {
                    this.Invoke(new MethodInvoker(() => groupBoxDataGrid.Text = rows.Count().ToString() + " results as of: " + DateTime.Now.ToString()));
                }
            }

            this.Invoke(new MethodInvoker(() => dataGridViewResults.DataSource = m_dtFilteredMegaSetsDetails));
        }
        private void createAsigraTasksFromHighlightedRows(bool skipExistingSummary, string priority, string board, string serviceType, string login, string defaultCompanyID, int team)
        {
            textBox1.Clear();

            DataGridViewSelectedRowCollection rows = dataGridViewResults.SelectedRows;

            if (rows != null && rows.Count > 0)
            {
                if (board == null || serviceType == null || login == null || team < 0)
                {
                    try
                    {
                        try
                        {
                            board = m_vault2CWBoard[m_vaultAddress];
                        }
                        catch { }

                        try
                        {
                            serviceType = m_vault2CWServiceType[m_vaultAddress];
                        }
                        catch { }

                        try
                        {
                            login = m_vault2CWLogin[m_vaultAddress];
                        }
                        catch { }

                        try
                        {
                            team = m_vault2CWTeam[m_vaultAddress];
                        }
                        catch { }
                    }
                    catch (Exception ex)
                    {
                        if (m_formShown)
                        {
                            MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                        }

                        System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);

                        board = "Dakota Backup Alerts";
                        serviceType = "Backup Alert";
                        login = "Evault-DBA";
                        team = 29;
                    }
                }

                foreach (DataGridViewRow row in rows)
                {
                    string system = row.Cells["ADSSystemName"].Value.ToString();
                    string customer = row.Cells["ACustomerName"].Value.ToString();
                    string dsc = row.Cells["ADSClient"].Value.ToString();
                    string set = row.Cells["ASetName"].Value.ToString();
                    string lbr = row.Cells["LastBackupResult"].Value.ToString();
                    string lbsr = row.Cells["LastBackupStopReason"].Value.ToString();

                    DateTime lbt = (DateTime)row.Cells["LastBackupTime"].Value;

                    string summary = "";
                    string description = "";
                    string duplicateCheckCompare = "";

                    set = set.Substring(0, set.LastIndexOf("\\"));

                    summary = set + "  LBR: " + lbr + "  LBD: " + lbt.ToShortDateString();

                    description = "Backup:\t\t" + system + ": " + dsc + ": " + set + "\r\n" +
                        "Last Backup Result:\t\t" + lbr + "\r\n" +
                        "Last Backup Stop Reason:\t" + lbsr + "\r\n" +
                        "Last Backup Time:\t\t" + lbt + "\r\n";

                    /*
                        a = set in summary
                        c = lbr in summary
                        d = lbd in summary
                    
                        duplicate check:
                        a*d 
                     */

                    string a = "Summary like '" + set.Replace("\\", "%") + "%'";
                    string c = "Summary like '%LBR: " + lbr + "%'";
                    string d = "Summary like '%LBD: " + lbt.ToShortDateString() + "%'";

                    duplicateCheckCompare = "(" + a + " AND " + d + ")";

                    if (!string.IsNullOrEmpty(defaultCompanyID))
                    {
                        customer = defaultCompanyID;
                    }

                    this.Invoke(new MethodInvoker(() =>
                    {
                        try
                        {
                            int ticketID = 0;

                            using (CWManipulator ticketMaker = new CWManipulator(m_cwCompany, login, m_cwPassword, m_cwSite))
                            {
                                ticketID = ticketMaker.CreateTicket(customer, summary, description, board, serviceType, team, duplicateCheckCompare, null, priority);
                            }

                            string comp = string.IsNullOrEmpty(customer) ? "CatchAll" : customer;

                            string textBoxText = "<font color=red>Ticket ID: " + ticketID.ToString() + "  Summary: " + comp + "\\" + summary + "</font>";

                            switch (ticketID)
                            {
                                case -1:
                                    {
                                        textBoxText = "No New Ticket. DupeCheck on Summary: " + comp + "\\" + summary;
                                    }
                                    break;
                                case -2:
                                    {
                                        textBoxText = "UNDEFINED RETVAL: -2";
                                    }
                                    break;
                                case -3:
                                    {
                                        textBoxText = customer + " is in a status that does not allow creating Service Tickets.";
                                    }
                                    break;
                                default:
                                    break;
                            }

                            textBox1.Text += textBoxText + "\r\n";
                        }
                        catch (Exception ex)
                        {
                            if (m_formShown)
                            {
                                MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                            }

                            System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                        }
                    }));
                }
            }
        }
        private void asigraCWTicketMaker(string[] args)
        {
            BackgroundWorker asigraCWTicketMakerBackground = new BackgroundWorker();
            asigraCWTicketMakerBackground.DoWork += asigraCWTicketMakerBackground_DoWork;
            asigraCWTicketMakerBackground.RunWorkerCompleted += asigraCWTicketMakerBackground_RunWorkerCompleted;

            asigraCWTicketMakerBackground.RunWorkerAsync(args);
        }
        #endregion


        #region Veeam Methods
        private void veeamCWTicketMaker(string[] args)
        {
            BackgroundWorker veeamCWTicketMakerBackground = new BackgroundWorker();
            veeamCWTicketMakerBackground.DoWork += veeamCWTicketMakerBackground_DoWork;
            veeamCWTicketMakerBackground.RunWorkerCompleted += veeamCWTicketMakerBackground_RunWorkerCompleted;

            veeamCWTicketMakerBackground.RunWorkerAsync(args);
        }

        private void fillVeeamTable(ref DataTable dt, string query)
        {
            if (dt != null)
            {
                dt.Dispose();
                dt = null;
            }

            dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(m_cwConnStr))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, new SqlConnection(m_vConnStr)))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(dt);
                    }
                }
            }
        }

        private void createVeeamTasks(DataRow[] jobSessions, bool skipExistingSummary, string priority, string board, string serviceType, string login, string companyID, int team)
        {
            string summary = "";
            string description = "";
            string duplicateCheckCompare = "";

            string server_name = jobSessions[0]["server_name"].ToString();
            string job_name = jobSessions[0]["job_name"].ToString();

            summary = server_name + "\\" + job_name + ": VOOD";

            description = "";

            foreach (DataRow session in jobSessions)
            {
                description += "Session Start:\t\t" + Convert.ToDateTime(session["start_time"].ToString()).ToLocalTime().ToString() + "\r\n" + 
                               "Session Result:\t" + session["session_result"].ToString() + "\r\n" +
                               "Session Failure:\t" + session["failure_message"].ToString() + "\r\n\r\n";
            }

            /*
                a = backup in summary
                    
                duplicate check:
                a
             */

            string a = "Summary like '%" + server_name + "%" + job_name + ": VOOD'";
            string b = "(StatusName != 'Completed' AND StatusName != 'Completed~' AND StatusName != 'Closed' AND StatusName != 'Closed-Silent' AND StatusName != 'Pending Closure~')";

            duplicateCheckCompare = "(" + a + " AND " + b + ")";

            this.Invoke(new MethodInvoker(() =>
            {
                try
                {
                    int ticketID = 0;

                    using (CWManipulator ticketMaker = new CWManipulator(m_cwCompany, login, m_cwPassword, m_cwSite))
                    {
                        ticketID = ticketMaker.CreateTicket(companyID, summary, description, board, serviceType, team, duplicateCheckCompare, null, priority);
                    }

                    string textBoxText = "<font color=red>Ticket ID: " + ticketID.ToString() + "  Summary: " + companyID + "\\" + summary + "</font>";

                    switch (ticketID)
                    {
                        case -1:
                            {
                                textBoxText = "No New Ticket. DupeCheck on Summary: " + companyID + "\\" + summary;
                            }
                            break;
                        case -2:
                            {
                                textBoxText = "UNDEFINED RETVAL: -2";
                            }
                            break;
                        case -3:
                            {
                                textBoxText = companyID + " is in a status that does not allow creating Service Tickets.";
                            }
                            break;
                        default:
                            break;
                    }

                    textBox1.Text += textBoxText + "\r\n";
                }
                catch (Exception ex)
                {
                    if (m_formShown)
                    {
                        MessageBox.Show("Show Dakota Backup: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
                    }

                    System.Diagnostics.EventLog.WriteEntry(m_source, m_program + "\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }));
        }
        #endregion

        #endregion


    }
}
