﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnectWiseSDK;

namespace Director
{
    class CWManipulator : IDisposable
    {
        CompanyApi m_companyApi = null;
        ServiceTicketApi m_serviceTicketApi = null;
        
        public CWManipulator(string company, string username, string password, string site)
        {
            Credentials creds = new Credentials();
            creds.Company = company;
            creds.IntegratorLoginUsername = username;
            creds.IntegratorLoginPassword = password;
            creds.Site = site;
            creds.Cookie = Guid.NewGuid().ToString();

            m_serviceTicketApi = new ServiceTicketApi(creds);
            m_companyApi = new CompanyApi(creds);
            
            //ServiceTicket tick = m_serviceTicketApi.GetServiceTicket(54584);
            //string condition = "Summary like 'Western Dakota Technical Institute%ROME_3b777b5b-6087-458e-a53a-7cd614fe549a OFFLINE Since: 8/21/2014'";
            //List<Ticket> tickets = m_serviceTicketApi.FindServiceTickets(condition, null, null, null);
            //string hi = tick.ToString();
            //Ticket t = new Ticket();
            //string hi = tickets.ToString();
        }



        public int CreateTicket(string companyName, string summary, string description, string board, string serviceType, int team, string duplicateSummaryCheck, 
            string exactDuplicateSummaryCheckAndClosed, string priority)
        {
            int retVal = -1;
            int companyID = 21801; //21801 is the CatchAllCompany - (do not delete) customer



            // find the customer's ID in order to find its name to make the ticket under.
            if (!string.IsNullOrEmpty(companyName))
            {
                string condition = "(Type = 'Partner' OR Type = 'Client' OR Type = 'MS Client') AND (CompanyName LIKE '" + companyName.Replace("'", "%") +
                    "' OR CompanyIdentifier LIKE '" + companyName.Replace("'", "%") + "')";

                List<CompanyFindResult> companies = m_companyApi.FindCompanies(condition, null, null, null, null);

                if (companies != null && companies.Count() > 0)
                    companyID = companies[0].Id;
            }

            Company comp = m_companyApi.GetCompany(companyID);



            if (!string.IsNullOrEmpty(exactDuplicateSummaryCheckAndClosed))
            {
                exactDuplicateSummaryCheckAndClosed += " AND CompanyId = " + companyID.ToString();
            }

            if (!string.IsNullOrEmpty(duplicateSummaryCheck))
            {
                duplicateSummaryCheck += " AND CompanyId = " + companyID.ToString();
            }


            // check for exact summary match and ticket being in a status containing closed. if found, re-open it and return.
            if (!string.IsNullOrEmpty(exactDuplicateSummaryCheckAndClosed))
            {
                List<TicketFindResult> tickets = m_serviceTicketApi.FindServiceTickets(exactDuplicateSummaryCheckAndClosed, null, null, null, null, null);

                if (tickets != null && tickets.Count() > 0 && (tickets[0].StatusName.Contains("Clos") || tickets[0].StatusName.Contains("Complete")))
                {
                    try
                    {
                        ServiceTicket oldTicket = m_serviceTicketApi.GetServiceTicket(tickets[0].Id);

                        oldTicket.StatusName = "Scheduling Required";

                        oldTicket = m_serviceTicketApi.AddOrUpdateServiceTicketViaCompanyIdentifier(comp.CompanyIdentifier, oldTicket);

                        retVal = oldTicket.Id;

                        return retVal;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("This company is in a status that does not allow creating Service Tickets"))
                        {
                            retVal = -3;

                            return retVal;
                        }
                        else
                            throw new Exception(ex.Message, ex.InnerException);
                    }
                }
            }






            // check for partial summary match. if found, return. 
            if (!string.IsNullOrEmpty(duplicateSummaryCheck))
            {
                List<TicketFindResult> tickets = m_serviceTicketApi.FindServiceTickets(duplicateSummaryCheck, null, null, null, null, null);

                if (tickets != null && tickets.Count() > 0)
                {
                    return retVal;
                }
            }

      





            //////////// done dupe checking. get on with making a new ticket!


            ServiceTicket st = new ServiceTicket();

            if (comp.Addresses != null && comp.Addresses.Count() > 0)
            {
                if (comp.Addresses[0].StreetLines != null)
                {
                    if (comp.Addresses[0].StreetLines.Count() > 1)
                    {
                        st.AddressLine1 = comp.Addresses[0].StreetLines[0];
                        st.AddressLine2 = comp.Addresses[0].StreetLines[1];
                    }
                    else if (comp.Addresses[0].StreetLines.Count() > 0)
                    {
                        st.AddressLine1 = comp.Addresses[0].StreetLines[0];
                    }
                }

                st.City = comp.Addresses[0].City;
                st.StateIdentifier = comp.Addresses[0].State;
                st.Zip = comp.Addresses[0].Zip;
                st.Country = comp.Addresses[0].Country;
            }

            st.Board = board;
            st.ClosedFlag = false;
            st.Id = 0;
            st.Impact = "Low";
            st.Location = "In-house";
            st.DetailDescription = description;
            st.Priority = string.IsNullOrEmpty(priority) ? "No SLA" : priority;
            st.ServiceType = serviceType;
            st.Severity = "Low";
            st.SiteName = "PRIMARY";
            st.Source = "Internal";
            st.StatusName = "New";
            st.Summary = string.IsNullOrEmpty(summary) ? " " : summary;
            st.ServiceTeamId = team;

            ServiceTicket rt = null;

            try
            {
                rt = m_serviceTicketApi.AddOrUpdateServiceTicketViaCompanyIdentifier(comp.CompanyIdentifier, st);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("This company is in a status that does not allow"))
                {
                    retVal = -3;
                }
                else
                    throw new Exception(ex.Message);
            }
            
            retVal = rt.Id;
            
            return retVal;
        }

        public void Dispose()
        {
            if (m_companyApi != null)
            {
                m_companyApi = null;
            }

            if (m_serviceTicketApi != null)
            {
                m_serviceTicketApi = null;
            }
        }
    }
}
