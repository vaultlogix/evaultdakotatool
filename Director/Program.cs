﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Director
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool showWinform = true;

            if (args.Count() > 0)
            {
                showWinform = false;
            }

            if (showWinform)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
            }

            Form f = new Form1(showWinform, args);

            if (!showWinform)
            {
                f.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                f.ShowInTaskbar = false;
                f.StartPosition = FormStartPosition.Manual;
                f.Location = new System.Drawing.Point(-20000, -20000);
                f.Size = new System.Drawing.Size(1, 1);
            }
   
            Application.Run(f);
        }
    }
}
