﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SbeAccountManager;
using Director.PortalApi;
using System.Data;
using System.Diagnostics;

namespace Director
{
    class AgentData
    {
        #region variables

        string m_source = "Application Error";

        private SoapApiClient _sac = null;

        private PortalApi.User _user = null;

        private Agent _agent = null;
        public Agent Agent
        {
            get { return _agent; }
        }

        object _locker = null;
        int _threadsReceived = 0;

        private AgentVaultRegistration[] _vaultRegs = null;
        public AgentVaultRegistration[] VaultRegistrations
        {
            get { return _vaultRegs; }
        }

        private AgentConfiguration _agentConfig = null;
        public AgentConfiguration AgentConfiguration
        {
            get { return _agentConfig; }
        }

        private Job[] _jobs = null;
        public Job[] Jobs
        {
            get { return _jobs; }
        }

        private Company _company = null;
        public Company Company
        {
            get { return _company; }
        }

        private JobConfiguration[] _jobConfigs = null;
        public JobConfiguration[] JobConfigurations
        {
            get { return _jobConfigs; }
        }

        private JobStatusLog[][] _backupJobsStatusLogs = null;
        public JobStatusLog[][] BackupJobsStatusLogs
        {
            get { return _backupJobsStatusLogs; }
        }

        private DateTime[] _lastBackupJobsVerySuccessfulBackupTime = null;
        public DateTime[] LastBackupJobsVerySuccessfulBackupTime
        {
            get { return _lastBackupJobsVerySuccessfulBackupTime; }
        }

        private DateTime[] _lastBackupJobsPerfectBackupTime = null;
        public DateTime[] LastBackupJobsPerfectBackupTime
        {
            get { return _lastBackupJobsPerfectBackupTime; }
        }

        private string _groupID = "";
        public string GroupID
        {
            get { return _groupID; }
        }

        private string _groupName = "";
        public string GroupName
        {
            get { return _groupName; }
        }

        private string _reqStatus = "";
        public string ReqStatus
        {
            get { return _reqStatus; }
        }

        private string _reqMessage = "";
        public string ReqMessage
        {
            get { return _reqMessage; }
        }

        private DataTable _dt = null;

        #endregion

        public AgentData(Agent a, SoapApiClient sac, PortalApi.User u, DataTable dt)
        {
            _agent = a;
            _sac = sac;
            _user = u;
            _dt = dt;
        }

        public async Task<AgentData> FindAgentDataAsync()
        {
            AgentData ad = new AgentData(_agent, _sac, _user, _dt);
          
            Task<AgentVaultRegistration[]> taskAVR = null;
            Task<Job[]> taskJ = null;
            Task<AgentConfiguration> taskAC = null;
            Task<Company> taskC = null;

            ad._reqStatus = "S";
            ad._reqMessage = "";

            if (ad._dt != null && ad._dt.Rows.Count > 0)
            {
                foreach (DataRow dr in ad._dt.Rows)
                {
                    if (ad._agent.AssignedAgentGroups.Contains(dr["Name"].ToString()))
                    {
                        ad._groupID = dr["ID"].ToString();
                        ad._groupName = dr["Name"].ToString();

                        break;
                    }
                }
            }
            
            taskC = ad._sac.GetCompanyAsync(ad._agent.CompanyId, ad._user.Id);
            taskAC = ad._sac.GetAgentConfigurationAsync(ad._agent.Id, ad._user.Id);
            taskJ = ad._sac.GetAgentJobsAsync(ad._agent.Id, ad._user.Id);

            if (_agent.Online)
            {
                taskAVR = ad._sac.GetAgentVaultRegistrationsAsync(ad._agent.Id, ad._user.Id);
            }

            try { ad._company = await taskC; }
            catch (Exception ex)
            {
                ad._reqStatus = "E";
                ad._reqMessage = ex.Message;

                System.Diagnostics.EventLog.WriteEntry(m_source, ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            if (ad._agent.Online)
            {
                try { ad._vaultRegs = await taskAVR; }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("The agent might be offline."))
                    {
                        ad._agent.Online = false;
                    }
                    else
                    {
                        ad._reqStatus = "E";
                        ad._reqMessage = ex.Message;

                        System.Diagnostics.EventLog.WriteEntry(m_source, ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                    }
                }
            }

            try { ad._agentConfig = await taskAC; }
            catch (Exception ex)
            {
                if (ex.Message.Contains("The agent might be offline."))
                {
                    ad._agent.Online = false;
                }
                else
                {
                    ad._reqStatus = "E";
                    ad._reqMessage = ex.Message;

                    System.Diagnostics.EventLog.WriteEntry(m_source, ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }

            try { ad._jobs = await taskJ; }
            catch (Exception ex)
            {
                if (ex.Message.Contains("The agent might be offline."))
                {
                    ad._agent.Online = false;
                }
                else
                {
                    ad._reqStatus = "E";
                    ad._reqMessage = ex.Message;

                    System.Diagnostics.EventLog.WriteEntry(m_source, ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }

            if (ad._jobs != null && ad._jobs.Count() > 0)
            {
                int jobCount = ad._jobs.Count();

                ad._jobConfigs = new JobConfiguration[jobCount];
                Task<JobConfiguration>[] taskJCs = new Task<JobConfiguration>[jobCount];

                ad._backupJobsStatusLogs = new JobStatusLog[jobCount][];
                Task<JobStatusLog[]>[] taskBJSLs = new Task<JobStatusLog[]>[jobCount];

                ad._lastBackupJobsVerySuccessfulBackupTime = new DateTime[jobCount];
                ad._lastBackupJobsPerfectBackupTime = new DateTime[jobCount];

                for (int i = 0; i < jobCount; i++)
                {
                    ad._jobConfigs[i] = null;
                    ad._backupJobsStatusLogs[i] = null;
                    ad._lastBackupJobsVerySuccessfulBackupTime[i] = DateTime.MinValue;
                    ad._lastBackupJobsPerfectBackupTime[i] = DateTime.MinValue;

                    taskJCs[i] = _sac.GetJobConfigurationAsync(ad._agent.Id, ad._jobs[i].Id, ad._user.Id);
                    taskBJSLs[i] = _sac.GetJobStatusLogsAsync(ad._jobs[i].Id, ad._agent.Id, JobStatusType.Backup, ad._user.Id);
                }
                
                for (int i = 0; i < jobCount; i++)
                {
                    try { ad._jobConfigs[i] = await taskJCs[i]; }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("The agent might be offline."))
                        {
                            ad._agent.Online = false;
                        }
                        else
                        {
                            ad._reqStatus = "E";
                            ad._reqMessage = ex.Message;

                            System.Diagnostics.EventLog.WriteEntry(m_source, ex.Message + "\r\n\r\n" + ex.StackTrace, EventLogEntryType.Error);
                        }
                    }

                    try 
                    { 
                        ad._backupJobsStatusLogs[i] = await taskBJSLs[i];

                        foreach (JobStatusLog log in ad._backupJobsStatusLogs[i])
                        {
                            if (log.Result == JobResult.Completed || log.Result == JobResult.CompletedWithWarnings)
                            {
                                if (log.DateTime > ad._lastBackupJobsVerySuccessfulBackupTime[i])
                                {
                                    ad._lastBackupJobsVerySuccessfulBackupTime[i] = log.DateTime;
                                }

                                if (log.Result == JobResult.Completed)
                                {
                                    if (log.DateTime > ad._lastBackupJobsPerfectBackupTime[i])
                                    {
                                        ad._lastBackupJobsPerfectBackupTime[i] = log.DateTime;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception bex)
                    {
                        if (bex.Message.Contains("The agent might be offline."))
                        {
                            ad._agent.Online = false;
                        }
                        else
                        {
                            ad._reqStatus = "E";
                            ad._reqMessage = bex.Message;

                            System.Diagnostics.EventLog.WriteEntry(m_source, bex.Message + "\r\n\r\n" + bex.StackTrace, EventLogEntryType.Error);
                        }
                    }

                    if (!ad._agent.Online)
                        break;
                }
            }

            ad._sac.Close();

            return ad;
        }
    }
}
