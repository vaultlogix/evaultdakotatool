﻿namespace Director
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxDirectorUsername = new System.Windows.Forms.TextBox();
            this.textBoxDirectorPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDomain = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxVault = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonTextToCSV = new System.Windows.Forms.Button();
            this.buttonPortalLogIn = new System.Windows.Forms.Button();
            this.buttonGroupAgents = new System.Windows.Forms.Button();
            this.buttonGetSiteAgents = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxGroups = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxJobs = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAgents = new System.Windows.Forms.ComboBox();
            this.buttonGetTasks = new System.Windows.Forms.Button();
            this.buttonGetSubsiteAgents = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxSubsites = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxSites = new System.Windows.Forms.ComboBox();
            this.buttonGetSubsites = new System.Windows.Forms.Button();
            this.buttonDirectorLogIn = new System.Windows.Forms.Button();
            this.comboBoxDoWhat = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonDoIt = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPortalUsername = new System.Windows.Forms.TextBox();
            this.textBoxPortalPassword = new System.Windows.Forms.TextBox();
            this.buttonTextToPDF = new System.Windows.Forms.Button();
            this.groupBoxThatWhat = new System.Windows.Forms.GroupBox();
            this.groupBoxQuery = new System.Windows.Forms.GroupBox();
            this.textBoxQuery = new System.Windows.Forms.TextBox();
            this.buttonGenerateQuery = new System.Windows.Forms.Button();
            this.buttonResetQuery = new System.Windows.Forms.Button();
            this.dateTimePickerLastSafeset = new System.Windows.Forms.DateTimePicker();
            this.comboBoxHaveLastSafeset = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveLastSafeset = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxAreChecklisted = new System.Windows.Forms.ComboBox();
            this.checkBoxThatChecklisted = new System.Windows.Forms.CheckBox();
            this.comboBoxHavePReqStatus = new System.Windows.Forms.ComboBox();
            this.comboBoxHavePReqStatusCompare = new System.Windows.Forms.ComboBox();
            this.checkBoxHavePReqStatus = new System.Windows.Forms.CheckBox();
            this.textBoxHaveSafesetCount = new System.Windows.Forms.TextBox();
            this.comboBoxHaveSafesetCount = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveSafesetCount = new System.Windows.Forms.CheckBox();
            this.comboBoxHaveAgentOnlineStatus = new System.Windows.Forms.ComboBox();
            this.comboBoxHaveAgentOnlineStatusCompare = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveAgentOnlineStatus = new System.Windows.Forms.CheckBox();
            this.checkBoxSaturday = new System.Windows.Forms.CheckBox();
            this.checkBoxThursday = new System.Windows.Forms.CheckBox();
            this.groupBoxLogOptions = new System.Windows.Forms.GroupBox();
            this.textBoxLimitLogs = new System.Windows.Forms.TextBox();
            this.checkBoxLimitLogs = new System.Windows.Forms.CheckBox();
            this.checkBoxErrorsWarningsOnly = new System.Windows.Forms.CheckBox();
            this.checkBoxErrorsOnly = new System.Windows.Forms.CheckBox();
            this.checkBoxBackupLogs = new System.Windows.Forms.CheckBox();
            this.checkBoxRestoreLogs = new System.Windows.Forms.CheckBox();
            this.checkBoxFriday = new System.Windows.Forms.CheckBox();
            this.checkBoxWednesday = new System.Windows.Forms.CheckBox();
            this.checkBoxTuesday = new System.Windows.Forms.CheckBox();
            this.checkBoxSunday = new System.Windows.Forms.CheckBox();
            this.checkBoxMonday = new System.Windows.Forms.CheckBox();
            this.comboBoxHaveWeeklySchedule = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveWeeklySchedule = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxHavePoolSize = new System.Windows.Forms.TextBox();
            this.comboBoxHavePoolSize = new System.Windows.Forms.ComboBox();
            this.dateTimePickerLastBackup = new System.Windows.Forms.DateTimePicker();
            this.comboBoxHaveLastBackup = new System.Windows.Forms.ComboBox();
            this.dateTimePickerLastSuccessfulBackup = new System.Windows.Forms.DateTimePicker();
            this.comboBoxHaveLastSuccessfulBackup = new System.Windows.Forms.ComboBox();
            this.comboBoxHaveLastBackupResult = new System.Windows.Forms.ComboBox();
            this.comboBoxHaveLastBackupResultCompare = new System.Windows.Forms.ComboBox();
            this.checkBoxHavePoolSize = new System.Windows.Forms.CheckBox();
            this.checkBoxHaveLastBackup = new System.Windows.Forms.CheckBox();
            this.checkBoxHaveLastSuccessfulBackup = new System.Windows.Forms.CheckBox();
            this.checkBoxHaveLastBackupResult = new System.Windows.Forms.CheckBox();
            this.comboBoxHaveSatelliteOperatingMode = new System.Windows.Forms.ComboBox();
            this.comboBoxHaveSatelliteOperatingModeCompare = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveSatelliteOperatingMode = new System.Windows.Forms.CheckBox();
            this.comboBoxHaveOperatingMode = new System.Windows.Forms.ComboBox();
            this.comboBoxHaveOperatingModeCompare = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveOperatingMode = new System.Windows.Forms.CheckBox();
            this.textBoxHaveMailServer = new System.Windows.Forms.TextBox();
            this.comboBoxHaveMailServer = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveMailServer = new System.Windows.Forms.CheckBox();
            this.checkBoxHaveName = new System.Windows.Forms.CheckBox();
            this.textBoxHaveName = new System.Windows.Forms.TextBox();
            this.comboBoxHaveName = new System.Windows.Forms.ComboBox();
            this.checkBoxHaveSatellite = new System.Windows.Forms.CheckBox();
            this.dataGridViewResults = new System.Windows.Forms.DataGridView();
            this.textBoxWritePath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageTaskQuery = new System.Windows.Forms.TabPage();
            this.tabControlQueryTypes = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonAddDSCJob = new System.Windows.Forms.Button();
            this.buttonAddDSC = new System.Windows.Forms.Button();
            this.buttonAddDSCustomer = new System.Windows.Forms.Button();
            this.buttonAddDSS = new System.Windows.Forms.Button();
            this.buttonChecklistAddSite = new System.Windows.Forms.Button();
            this.buttonChecklistAddAgent = new System.Windows.Forms.Button();
            this.buttonChecklistAddGroup = new System.Windows.Forms.Button();
            this.buttonChecklistAddSubsite = new System.Windows.Forms.Button();
            this.textBoxSaveLoadList = new System.Windows.Forms.TextBox();
            this.buttonLoadList = new System.Windows.Forms.Button();
            this.buttonSaveTaskList = new System.Windows.Forms.Button();
            this.buttonChecklistAddTask = new System.Windows.Forms.Button();
            this.buttonChecklistRemoveSelected = new System.Windows.Forms.Button();
            this.buttonChecklistSelectAll = new System.Windows.Forms.Button();
            this.checkedListBoxQuery = new System.Windows.Forms.CheckedListBox();
            this.tabPageAgentConfig = new System.Windows.Forms.TabPage();
            this.groupBoxAgentWithWhat = new System.Windows.Forms.GroupBox();
            this.textBoxAgentConfigMailServerPort = new System.Windows.Forms.TextBox();
            this.checkBoxAgentConfigMailServerPort = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonSuccessOn = new System.Windows.Forms.RadioButton();
            this.radioButtonSuccessOff = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonFailuresOn = new System.Windows.Forms.RadioButton();
            this.radioButtonFailuresOff = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonErrorsOn = new System.Windows.Forms.RadioButton();
            this.radioButtonErrorsOff = new System.Windows.Forms.RadioButton();
            this.textBoxAgentMailServerFromAddress = new System.Windows.Forms.TextBox();
            this.textBoxAgentMailServerPassword = new System.Windows.Forms.TextBox();
            this.textBoxAgentMailServerDomain = new System.Windows.Forms.TextBox();
            this.checkBoxAgentMailServerFromAddress = new System.Windows.Forms.CheckBox();
            this.checkBoxAgentMailServerPassword = new System.Windows.Forms.CheckBox();
            this.checkBoxAgentMailServerDomain = new System.Windows.Forms.CheckBox();
            this.textBoxcheckBoxAgentMailServerUsername = new System.Windows.Forms.TextBox();
            this.checkBoxAgentMailServerUsername = new System.Windows.Forms.CheckBox();
            this.checkBoxAgentConfigNotificationsSuccess = new System.Windows.Forms.CheckBox();
            this.checkBoxAgentConfigNotificationsFailure = new System.Windows.Forms.CheckBox();
            this.checkBoxAgentConfigNotificationsError = new System.Windows.Forms.CheckBox();
            this.textBoxAgentConfigRecipients = new System.Windows.Forms.TextBox();
            this.checkBoxAgentConfigRecipients = new System.Windows.Forms.CheckBox();
            this.textBoxAgentConfigMailServer = new System.Windows.Forms.TextBox();
            this.checkBoxAgentConfigMailServer = new System.Windows.Forms.CheckBox();
            this.tabControlResults = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBoxDataGrid = new System.Windows.Forms.GroupBox();
            this.tabPageTextBoxResults = new System.Windows.Forms.TabPage();
            this.richTextBoxResults = new System.Windows.Forms.RichTextBox();
            this.tabControlSystem = new System.Windows.Forms.TabControl();
            this.tabPageEVault = new System.Windows.Forms.TabPage();
            this.tabPageAsigra = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxNOC = new System.Windows.Forms.TextBox();
            this.buttonNOCLogin = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxNOCUsername = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxNOCPassword = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.comboBoxSets = new System.Windows.Forms.ComboBox();
            this.comboBoxClients = new System.Windows.Forms.ComboBox();
            this.comboBoxCustomers = new System.Windows.Forms.ComboBox();
            this.comboBoxSystems = new System.Windows.Forms.ComboBox();
            this.buttonReadWritePath = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBoxThatWhat.SuspendLayout();
            this.groupBoxQuery.SuspendLayout();
            this.groupBoxLogOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResults)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageTaskQuery.SuspendLayout();
            this.tabControlQueryTypes.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPageAgentConfig.SuspendLayout();
            this.groupBoxAgentWithWhat.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControlResults.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBoxDataGrid.SuspendLayout();
            this.tabPageTextBoxResults.SuspendLayout();
            this.tabControlSystem.SuspendLayout();
            this.tabPageEVault.SuspendLayout();
            this.tabPageAsigra.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(531, 30);
            this.textBox1.MaxLength = 128000;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(546, 266);
            this.textBox1.TabIndex = 990;
            // 
            // textBoxDirectorUsername
            // 
            this.textBoxDirectorUsername.Location = new System.Drawing.Point(107, 3);
            this.textBoxDirectorUsername.Name = "textBoxDirectorUsername";
            this.textBoxDirectorUsername.Size = new System.Drawing.Size(137, 20);
            this.textBoxDirectorUsername.TabIndex = 0;
            this.textBoxDirectorUsername.TextChanged += new System.EventHandler(this.textBoxDirectorInfos_TextChanged);
            // 
            // textBoxDirectorPassword
            // 
            this.textBoxDirectorPassword.Location = new System.Drawing.Point(309, 3);
            this.textBoxDirectorPassword.Name = "textBoxDirectorPassword";
            this.textBoxDirectorPassword.PasswordChar = '*';
            this.textBoxDirectorPassword.Size = new System.Drawing.Size(106, 20);
            this.textBoxDirectorPassword.TabIndex = 1;
            this.textBoxDirectorPassword.TextChanged += new System.EventHandler(this.textBoxDirectorInfos_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Director Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // textBoxDomain
            // 
            this.textBoxDomain.Location = new System.Drawing.Point(72, 55);
            this.textBoxDomain.Name = "textBoxDomain";
            this.textBoxDomain.Size = new System.Drawing.Size(210, 20);
            this.textBoxDomain.TabIndex = 6;
            this.textBoxDomain.Text = "dakotabackup";
            this.textBoxDomain.TextChanged += new System.EventHandler(this.textBoxDirectorInfos_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Domain";
            // 
            // textBoxVault
            // 
            this.textBoxVault.Location = new System.Drawing.Point(72, 81);
            this.textBoxVault.Name = "textBoxVault";
            this.textBoxVault.Size = new System.Drawing.Size(210, 20);
            this.textBoxVault.TabIndex = 7;
            this.textBoxVault.Text = "vault.dakotabackup.com";
            this.textBoxVault.TextChanged += new System.EventHandler(this.textBoxDirectorInfos_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Vault";
            // 
            // buttonTextToCSV
            // 
            this.buttonTextToCSV.Location = new System.Drawing.Point(531, 4);
            this.buttonTextToCSV.Name = "buttonTextToCSV";
            this.buttonTextToCSV.Size = new System.Drawing.Size(75, 23);
            this.buttonTextToCSV.TabIndex = 12;
            this.buttonTextToCSV.Text = "Write CSV";
            this.buttonTextToCSV.UseVisualStyleBackColor = true;
            this.buttonTextToCSV.Click += new System.EventHandler(this.buttonTextToCSV_Click);
            // 
            // buttonPortalLogIn
            // 
            this.buttonPortalLogIn.Location = new System.Drawing.Point(421, 27);
            this.buttonPortalLogIn.Name = "buttonPortalLogIn";
            this.buttonPortalLogIn.Size = new System.Drawing.Size(48, 23);
            this.buttonPortalLogIn.TabIndex = 5;
            this.buttonPortalLogIn.Text = "Login";
            this.buttonPortalLogIn.UseVisualStyleBackColor = true;
            this.buttonPortalLogIn.Click += new System.EventHandler(this.buttonPortalLogIn_Click);
            // 
            // buttonGroupAgents
            // 
            this.buttonGroupAgents.Location = new System.Drawing.Point(288, 160);
            this.buttonGroupAgents.Name = "buttonGroupAgents";
            this.buttonGroupAgents.Size = new System.Drawing.Size(75, 23);
            this.buttonGroupAgents.TabIndex = 40;
            this.buttonGroupAgents.Text = "Get Agents";
            this.buttonGroupAgents.UseVisualStyleBackColor = true;
            this.buttonGroupAgents.Click += new System.EventHandler(this.buttonGroupAgents_Click);
            // 
            // buttonGetSiteAgents
            // 
            this.buttonGetSiteAgents.Location = new System.Drawing.Point(369, 105);
            this.buttonGetSiteAgents.Name = "buttonGetSiteAgents";
            this.buttonGetSiteAgents.Size = new System.Drawing.Size(75, 23);
            this.buttonGetSiteAgents.TabIndex = 39;
            this.buttonGetSiteAgents.Text = "Get Agents";
            this.buttonGetSiteAgents.UseVisualStyleBackColor = true;
            this.buttonGetSiteAgents.Click += new System.EventHandler(this.buttonGetSiteAgents_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Group";
            // 
            // comboBoxGroups
            // 
            this.comboBoxGroups.FormattingEnabled = true;
            this.comboBoxGroups.Location = new System.Drawing.Point(73, 162);
            this.comboBoxGroups.Name = "comboBoxGroups";
            this.comboBoxGroups.Size = new System.Drawing.Size(209, 21);
            this.comboBoxGroups.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Task";
            // 
            // comboBoxJobs
            // 
            this.comboBoxJobs.FormattingEnabled = true;
            this.comboBoxJobs.Location = new System.Drawing.Point(73, 216);
            this.comboBoxJobs.Name = "comboBoxJobs";
            this.comboBoxJobs.Size = new System.Drawing.Size(209, 21);
            this.comboBoxJobs.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Agent";
            // 
            // comboBoxAgents
            // 
            this.comboBoxAgents.FormattingEnabled = true;
            this.comboBoxAgents.Location = new System.Drawing.Point(73, 189);
            this.comboBoxAgents.Name = "comboBoxAgents";
            this.comboBoxAgents.Size = new System.Drawing.Size(209, 21);
            this.comboBoxAgents.TabIndex = 31;
            // 
            // buttonGetTasks
            // 
            this.buttonGetTasks.Location = new System.Drawing.Point(288, 187);
            this.buttonGetTasks.Name = "buttonGetTasks";
            this.buttonGetTasks.Size = new System.Drawing.Size(75, 23);
            this.buttonGetTasks.TabIndex = 30;
            this.buttonGetTasks.Text = "Get Tasks";
            this.buttonGetTasks.UseVisualStyleBackColor = true;
            this.buttonGetTasks.Click += new System.EventHandler(this.buttonGetTasks_Click);
            // 
            // buttonGetSubsiteAgents
            // 
            this.buttonGetSubsiteAgents.Location = new System.Drawing.Point(288, 132);
            this.buttonGetSubsiteAgents.Name = "buttonGetSubsiteAgents";
            this.buttonGetSubsiteAgents.Size = new System.Drawing.Size(75, 23);
            this.buttonGetSubsiteAgents.TabIndex = 29;
            this.buttonGetSubsiteAgents.Text = "Get Agents";
            this.buttonGetSubsiteAgents.UseVisualStyleBackColor = true;
            this.buttonGetSubsiteAgents.Click += new System.EventHandler(this.buttonGetSubsiteAgents_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Subsite";
            // 
            // comboBoxSubsites
            // 
            this.comboBoxSubsites.FormattingEnabled = true;
            this.comboBoxSubsites.Location = new System.Drawing.Point(72, 134);
            this.comboBoxSubsites.Name = "comboBoxSubsites";
            this.comboBoxSubsites.Size = new System.Drawing.Size(210, 21);
            this.comboBoxSubsites.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Site";
            // 
            // comboBoxSites
            // 
            this.comboBoxSites.FormattingEnabled = true;
            this.comboBoxSites.Location = new System.Drawing.Point(72, 107);
            this.comboBoxSites.Name = "comboBoxSites";
            this.comboBoxSites.Size = new System.Drawing.Size(210, 21);
            this.comboBoxSites.TabIndex = 9;
            // 
            // buttonGetSubsites
            // 
            this.buttonGetSubsites.Location = new System.Drawing.Point(288, 105);
            this.buttonGetSubsites.Name = "buttonGetSubsites";
            this.buttonGetSubsites.Size = new System.Drawing.Size(75, 23);
            this.buttonGetSubsites.TabIndex = 24;
            this.buttonGetSubsites.Text = "Get Subsites";
            this.buttonGetSubsites.UseVisualStyleBackColor = true;
            this.buttonGetSubsites.Click += new System.EventHandler(this.buttonGetSubsites_Click);
            // 
            // buttonDirectorLogIn
            // 
            this.buttonDirectorLogIn.Location = new System.Drawing.Point(421, 1);
            this.buttonDirectorLogIn.Name = "buttonDirectorLogIn";
            this.buttonDirectorLogIn.Size = new System.Drawing.Size(48, 23);
            this.buttonDirectorLogIn.TabIndex = 2;
            this.buttonDirectorLogIn.Text = "Login";
            this.buttonDirectorLogIn.UseVisualStyleBackColor = true;
            this.buttonDirectorLogIn.Click += new System.EventHandler(this.buttonDirectorLogIn_Click);
            // 
            // comboBoxDoWhat
            // 
            this.comboBoxDoWhat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDoWhat.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboBoxDoWhat.FormattingEnabled = true;
            this.comboBoxDoWhat.Items.AddRange(new object[] {
            "",
            "Configure Agents with..",
            "Create CW Tickets from EVault Tasks",
            "Create CW Tickets from Asigra Sets",
            "Director: Base Vaults Usage Report",
            "Director: Quota/Pool Report",
            "Portal: Task: Backup Items Report",
            "Portal: Group: Backup Items Report",
            "Portal: Subsite: Backup Items Report",
            "Show EVault Tasks that..",
            "Show Asigra Sets that.."});
            this.comboBoxDoWhat.Location = new System.Drawing.Point(152, 289);
            this.comboBoxDoWhat.Name = "comboBoxDoWhat";
            this.comboBoxDoWhat.Size = new System.Drawing.Size(291, 21);
            this.comboBoxDoWhat.TabIndex = 47;
            this.comboBoxDoWhat.SelectedIndexChanged += new System.EventHandler(this.comboBoxDoWhat_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(96, 292);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 48;
            this.label11.Text = "Do What";
            // 
            // buttonDoIt
            // 
            this.buttonDoIt.Location = new System.Drawing.Point(449, 287);
            this.buttonDoIt.Name = "buttonDoIt";
            this.buttonDoIt.Size = new System.Drawing.Size(75, 23);
            this.buttonDoIt.TabIndex = 49;
            this.buttonDoIt.Text = "Do It";
            this.buttonDoIt.UseVisualStyleBackColor = true;
            this.buttonDoIt.Click += new System.EventHandler(this.buttonDoIt_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 993;
            this.label13.Text = "Portal Username";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(250, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 994;
            this.label14.Text = "Password";
            // 
            // textBoxPortalUsername
            // 
            this.textBoxPortalUsername.Location = new System.Drawing.Point(107, 29);
            this.textBoxPortalUsername.Name = "textBoxPortalUsername";
            this.textBoxPortalUsername.Size = new System.Drawing.Size(137, 20);
            this.textBoxPortalUsername.TabIndex = 3;
            this.textBoxPortalUsername.TextChanged += new System.EventHandler(this.textBoxPortalInfos_TextChanged);
            // 
            // textBoxPortalPassword
            // 
            this.textBoxPortalPassword.Location = new System.Drawing.Point(309, 29);
            this.textBoxPortalPassword.Name = "textBoxPortalPassword";
            this.textBoxPortalPassword.PasswordChar = '*';
            this.textBoxPortalPassword.Size = new System.Drawing.Size(106, 20);
            this.textBoxPortalPassword.TabIndex = 4;
            this.textBoxPortalPassword.TextChanged += new System.EventHandler(this.textBoxPortalInfos_TextChanged);
            // 
            // buttonTextToPDF
            // 
            this.buttonTextToPDF.Location = new System.Drawing.Point(612, 4);
            this.buttonTextToPDF.Name = "buttonTextToPDF";
            this.buttonTextToPDF.Size = new System.Drawing.Size(75, 23);
            this.buttonTextToPDF.TabIndex = 997;
            this.buttonTextToPDF.Text = "Write PDF";
            this.buttonTextToPDF.UseVisualStyleBackColor = true;
            this.buttonTextToPDF.Click += new System.EventHandler(this.buttonTextToPDF_Click);
            // 
            // groupBoxThatWhat
            // 
            this.groupBoxThatWhat.Controls.Add(this.groupBoxQuery);
            this.groupBoxThatWhat.Controls.Add(this.buttonGenerateQuery);
            this.groupBoxThatWhat.Controls.Add(this.buttonResetQuery);
            this.groupBoxThatWhat.Controls.Add(this.dateTimePickerLastSafeset);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveLastSafeset);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveLastSafeset);
            this.groupBoxThatWhat.Controls.Add(this.label12);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxAreChecklisted);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxThatChecklisted);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHavePReqStatus);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHavePReqStatusCompare);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHavePReqStatus);
            this.groupBoxThatWhat.Controls.Add(this.textBoxHaveSafesetCount);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveSafesetCount);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveSafesetCount);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveAgentOnlineStatus);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveAgentOnlineStatusCompare);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveAgentOnlineStatus);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxSaturday);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxThursday);
            this.groupBoxThatWhat.Controls.Add(this.groupBoxLogOptions);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxFriday);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxWednesday);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxTuesday);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxSunday);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxMonday);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveWeeklySchedule);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveWeeklySchedule);
            this.groupBoxThatWhat.Controls.Add(this.label15);
            this.groupBoxThatWhat.Controls.Add(this.textBoxHavePoolSize);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHavePoolSize);
            this.groupBoxThatWhat.Controls.Add(this.dateTimePickerLastBackup);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveLastBackup);
            this.groupBoxThatWhat.Controls.Add(this.dateTimePickerLastSuccessfulBackup);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveLastSuccessfulBackup);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveLastBackupResult);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveLastBackupResultCompare);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHavePoolSize);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveLastBackup);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveLastSuccessfulBackup);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveLastBackupResult);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveSatelliteOperatingMode);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveSatelliteOperatingModeCompare);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveSatelliteOperatingMode);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveOperatingMode);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveOperatingModeCompare);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveOperatingMode);
            this.groupBoxThatWhat.Controls.Add(this.textBoxHaveMailServer);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveMailServer);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveMailServer);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveName);
            this.groupBoxThatWhat.Controls.Add(this.textBoxHaveName);
            this.groupBoxThatWhat.Controls.Add(this.comboBoxHaveName);
            this.groupBoxThatWhat.Controls.Add(this.checkBoxHaveSatellite);
            this.groupBoxThatWhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxThatWhat.Location = new System.Drawing.Point(0, 0);
            this.groupBoxThatWhat.Name = "groupBoxThatWhat";
            this.groupBoxThatWhat.Size = new System.Drawing.Size(497, 527);
            this.groupBoxThatWhat.TabIndex = 998;
            this.groupBoxThatWhat.TabStop = false;
            this.groupBoxThatWhat.Text = "that have a(n)..";
            // 
            // groupBoxQuery
            // 
            this.groupBoxQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxQuery.Controls.Add(this.textBoxQuery);
            this.groupBoxQuery.Location = new System.Drawing.Point(6, 366);
            this.groupBoxQuery.Name = "groupBoxQuery";
            this.groupBoxQuery.Size = new System.Drawing.Size(482, 85);
            this.groupBoxQuery.TabIndex = 1022;
            this.groupBoxQuery.TabStop = false;
            this.groupBoxQuery.Text = "Query to apply:";
            // 
            // textBoxQuery
            // 
            this.textBoxQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxQuery.Location = new System.Drawing.Point(3, 16);
            this.textBoxQuery.Multiline = true;
            this.textBoxQuery.Name = "textBoxQuery";
            this.textBoxQuery.Size = new System.Drawing.Size(476, 66);
            this.textBoxQuery.TabIndex = 1016;
            // 
            // buttonGenerateQuery
            // 
            this.buttonGenerateQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenerateQuery.Location = new System.Drawing.Point(358, 8);
            this.buttonGenerateQuery.Name = "buttonGenerateQuery";
            this.buttonGenerateQuery.Size = new System.Drawing.Size(78, 20);
            this.buttonGenerateQuery.TabIndex = 1021;
            this.buttonGenerateQuery.Text = "Generate Query";
            this.buttonGenerateQuery.UseVisualStyleBackColor = true;
            this.buttonGenerateQuery.Click += new System.EventHandler(this.buttonGenerateQuery_Click);
            // 
            // buttonResetQuery
            // 
            this.buttonResetQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonResetQuery.Location = new System.Drawing.Point(442, 8);
            this.buttonResetQuery.Name = "buttonResetQuery";
            this.buttonResetQuery.Size = new System.Drawing.Size(40, 20);
            this.buttonResetQuery.TabIndex = 1020;
            this.buttonResetQuery.Text = "Reset";
            this.buttonResetQuery.UseVisualStyleBackColor = true;
            this.buttonResetQuery.Click += new System.EventHandler(this.buttonResetQuery_Click);
            // 
            // dateTimePickerLastSafeset
            // 
            this.dateTimePickerLastSafeset.CustomFormat = "MM/dd/yy hh:mm tt";
            this.dateTimePickerLastSafeset.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLastSafeset.Location = new System.Drawing.Point(281, 202);
            this.dateTimePickerLastSafeset.Name = "dateTimePickerLastSafeset";
            this.dateTimePickerLastSafeset.Size = new System.Drawing.Size(173, 20);
            this.dateTimePickerLastSafeset.TabIndex = 1019;
            // 
            // comboBoxHaveLastSafeset
            // 
            this.comboBoxHaveLastSafeset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveLastSafeset.FormattingEnabled = true;
            this.comboBoxHaveLastSafeset.Items.AddRange(new object[] {
            "before",
            "after"});
            this.comboBoxHaveLastSafeset.Location = new System.Drawing.Point(154, 201);
            this.comboBoxHaveLastSafeset.Name = "comboBoxHaveLastSafeset";
            this.comboBoxHaveLastSafeset.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveLastSafeset.TabIndex = 1018;
            // 
            // checkBoxHaveLastSafeset
            // 
            this.checkBoxHaveLastSafeset.AutoSize = true;
            this.checkBoxHaveLastSafeset.Location = new System.Drawing.Point(6, 203);
            this.checkBoxHaveLastSafeset.Name = "checkBoxHaveLastSafeset";
            this.checkBoxHaveLastSafeset.Size = new System.Drawing.Size(85, 17);
            this.checkBoxHaveLastSafeset.TabIndex = 1017;
            this.checkBoxHaveLastSafeset.Text = "Last Safeset";
            this.checkBoxHaveLastSafeset.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(278, 342);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 1015;
            this.label12.Text = "the Checklist";
            // 
            // comboBoxAreChecklisted
            // 
            this.comboBoxAreChecklisted.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAreChecklisted.FormattingEnabled = true;
            this.comboBoxAreChecklisted.Items.AddRange(new object[] {
            "in",
            "not in"});
            this.comboBoxAreChecklisted.Location = new System.Drawing.Point(154, 339);
            this.comboBoxAreChecklisted.Name = "comboBoxAreChecklisted";
            this.comboBoxAreChecklisted.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAreChecklisted.TabIndex = 1014;
            // 
            // checkBoxThatChecklisted
            // 
            this.checkBoxThatChecklisted.AutoSize = true;
            this.checkBoxThatChecklisted.Location = new System.Drawing.Point(6, 341);
            this.checkBoxThatChecklisted.Name = "checkBoxThatChecklisted";
            this.checkBoxThatChecklisted.Size = new System.Drawing.Size(68, 17);
            this.checkBoxThatChecklisted.TabIndex = 1013;
            this.checkBoxThatChecklisted.Text = "..that are";
            this.checkBoxThatChecklisted.UseVisualStyleBackColor = true;
            // 
            // comboBoxHavePReqStatus
            // 
            this.comboBoxHavePReqStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHavePReqStatus.FormattingEnabled = true;
            this.comboBoxHavePReqStatus.Items.AddRange(new object[] {
            "Success",
            "Error"});
            this.comboBoxHavePReqStatus.Location = new System.Drawing.Point(281, 316);
            this.comboBoxHavePReqStatus.Name = "comboBoxHavePReqStatus";
            this.comboBoxHavePReqStatus.Size = new System.Drawing.Size(173, 21);
            this.comboBoxHavePReqStatus.TabIndex = 1012;
            // 
            // comboBoxHavePReqStatusCompare
            // 
            this.comboBoxHavePReqStatusCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHavePReqStatusCompare.FormattingEnabled = true;
            this.comboBoxHavePReqStatusCompare.Items.AddRange(new object[] {
            "equal to"});
            this.comboBoxHavePReqStatusCompare.Location = new System.Drawing.Point(154, 316);
            this.comboBoxHavePReqStatusCompare.Name = "comboBoxHavePReqStatusCompare";
            this.comboBoxHavePReqStatusCompare.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHavePReqStatusCompare.TabIndex = 1011;
            // 
            // checkBoxHavePReqStatus
            // 
            this.checkBoxHavePReqStatus.AutoSize = true;
            this.checkBoxHavePReqStatus.Location = new System.Drawing.Point(6, 318);
            this.checkBoxHavePReqStatus.Name = "checkBoxHavePReqStatus";
            this.checkBoxHavePReqStatus.Size = new System.Drawing.Size(86, 17);
            this.checkBoxHavePReqStatus.TabIndex = 1010;
            this.checkBoxHavePReqStatus.Text = "PReq Status";
            this.checkBoxHavePReqStatus.UseVisualStyleBackColor = true;
            // 
            // textBoxHaveSafesetCount
            // 
            this.textBoxHaveSafesetCount.Location = new System.Drawing.Point(281, 293);
            this.textBoxHaveSafesetCount.Name = "textBoxHaveSafesetCount";
            this.textBoxHaveSafesetCount.Size = new System.Drawing.Size(173, 20);
            this.textBoxHaveSafesetCount.TabIndex = 1009;
            // 
            // comboBoxHaveSafesetCount
            // 
            this.comboBoxHaveSafesetCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveSafesetCount.FormattingEnabled = true;
            this.comboBoxHaveSafesetCount.Items.AddRange(new object[] {
            "less than",
            "equal to",
            "greater than"});
            this.comboBoxHaveSafesetCount.Location = new System.Drawing.Point(154, 293);
            this.comboBoxHaveSafesetCount.Name = "comboBoxHaveSafesetCount";
            this.comboBoxHaveSafesetCount.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveSafesetCount.TabIndex = 1008;
            // 
            // checkBoxHaveSafesetCount
            // 
            this.checkBoxHaveSafesetCount.AutoSize = true;
            this.checkBoxHaveSafesetCount.Location = new System.Drawing.Point(6, 295);
            this.checkBoxHaveSafesetCount.Name = "checkBoxHaveSafesetCount";
            this.checkBoxHaveSafesetCount.Size = new System.Drawing.Size(119, 17);
            this.checkBoxHaveSafesetCount.TabIndex = 1007;
            this.checkBoxHaveSafesetCount.Text = "Number of Safesets";
            this.checkBoxHaveSafesetCount.UseVisualStyleBackColor = true;
            // 
            // comboBoxHaveAgentOnlineStatus
            // 
            this.comboBoxHaveAgentOnlineStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveAgentOnlineStatus.FormattingEnabled = true;
            this.comboBoxHaveAgentOnlineStatus.Items.AddRange(new object[] {
            "Online",
            "Offline"});
            this.comboBoxHaveAgentOnlineStatus.Location = new System.Drawing.Point(281, 270);
            this.comboBoxHaveAgentOnlineStatus.Name = "comboBoxHaveAgentOnlineStatus";
            this.comboBoxHaveAgentOnlineStatus.Size = new System.Drawing.Size(173, 21);
            this.comboBoxHaveAgentOnlineStatus.TabIndex = 1006;
            // 
            // comboBoxHaveAgentOnlineStatusCompare
            // 
            this.comboBoxHaveAgentOnlineStatusCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveAgentOnlineStatusCompare.FormattingEnabled = true;
            this.comboBoxHaveAgentOnlineStatusCompare.Items.AddRange(new object[] {
            "equal to"});
            this.comboBoxHaveAgentOnlineStatusCompare.Location = new System.Drawing.Point(154, 270);
            this.comboBoxHaveAgentOnlineStatusCompare.Name = "comboBoxHaveAgentOnlineStatusCompare";
            this.comboBoxHaveAgentOnlineStatusCompare.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveAgentOnlineStatusCompare.TabIndex = 1005;
            // 
            // checkBoxHaveAgentOnlineStatus
            // 
            this.checkBoxHaveAgentOnlineStatus.AutoSize = true;
            this.checkBoxHaveAgentOnlineStatus.Location = new System.Drawing.Point(6, 272);
            this.checkBoxHaveAgentOnlineStatus.Name = "checkBoxHaveAgentOnlineStatus";
            this.checkBoxHaveAgentOnlineStatus.Size = new System.Drawing.Size(120, 17);
            this.checkBoxHaveAgentOnlineStatus.TabIndex = 1004;
            this.checkBoxHaveAgentOnlineStatus.Text = "Agent Online Status";
            this.checkBoxHaveAgentOnlineStatus.UseVisualStyleBackColor = true;
            // 
            // checkBoxSaturday
            // 
            this.checkBoxSaturday.AutoSize = true;
            this.checkBoxSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSaturday.Location = new System.Drawing.Point(449, 249);
            this.checkBoxSaturday.Name = "checkBoxSaturday";
            this.checkBoxSaturday.Size = new System.Drawing.Size(30, 16);
            this.checkBoxSaturday.TabIndex = 35;
            this.checkBoxSaturday.Text = "S";
            this.checkBoxSaturday.UseVisualStyleBackColor = true;
            // 
            // checkBoxThursday
            // 
            this.checkBoxThursday.AutoSize = true;
            this.checkBoxThursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxThursday.Location = new System.Drawing.Point(386, 249);
            this.checkBoxThursday.Name = "checkBoxThursday";
            this.checkBoxThursday.Size = new System.Drawing.Size(29, 16);
            this.checkBoxThursday.TabIndex = 34;
            this.checkBoxThursday.Text = "T";
            this.checkBoxThursday.UseVisualStyleBackColor = true;
            // 
            // groupBoxLogOptions
            // 
            this.groupBoxLogOptions.Controls.Add(this.textBoxLimitLogs);
            this.groupBoxLogOptions.Controls.Add(this.checkBoxLimitLogs);
            this.groupBoxLogOptions.Controls.Add(this.checkBoxErrorsWarningsOnly);
            this.groupBoxLogOptions.Controls.Add(this.checkBoxErrorsOnly);
            this.groupBoxLogOptions.Controls.Add(this.checkBoxBackupLogs);
            this.groupBoxLogOptions.Controls.Add(this.checkBoxRestoreLogs);
            this.groupBoxLogOptions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxLogOptions.Location = new System.Drawing.Point(3, 457);
            this.groupBoxLogOptions.Name = "groupBoxLogOptions";
            this.groupBoxLogOptions.Size = new System.Drawing.Size(491, 67);
            this.groupBoxLogOptions.TabIndex = 1000;
            this.groupBoxLogOptions.TabStop = false;
            this.groupBoxLogOptions.Text = "logging options";
            // 
            // textBoxLimitLogs
            // 
            this.textBoxLimitLogs.Location = new System.Drawing.Point(402, 17);
            this.textBoxLimitLogs.Name = "textBoxLimitLogs";
            this.textBoxLimitLogs.Size = new System.Drawing.Size(40, 20);
            this.textBoxLimitLogs.TabIndex = 5;
            // 
            // checkBoxLimitLogs
            // 
            this.checkBoxLimitLogs.AutoSize = true;
            this.checkBoxLimitLogs.Location = new System.Drawing.Point(281, 19);
            this.checkBoxLimitLogs.Name = "checkBoxLimitLogs";
            this.checkBoxLimitLogs.Size = new System.Drawing.Size(124, 17);
            this.checkBoxLimitLogs.TabIndex = 4;
            this.checkBoxLimitLogs.Text = "Limit to last # of lines";
            this.checkBoxLimitLogs.UseVisualStyleBackColor = true;
            // 
            // checkBoxErrorsWarningsOnly
            // 
            this.checkBoxErrorsWarningsOnly.AutoSize = true;
            this.checkBoxErrorsWarningsOnly.Location = new System.Drawing.Point(130, 42);
            this.checkBoxErrorsWarningsOnly.Name = "checkBoxErrorsWarningsOnly";
            this.checkBoxErrorsWarningsOnly.Size = new System.Drawing.Size(146, 17);
            this.checkBoxErrorsWarningsOnly.TabIndex = 3;
            this.checkBoxErrorsWarningsOnly.Text = "Errors and Warnings Only";
            this.checkBoxErrorsWarningsOnly.UseVisualStyleBackColor = true;
            // 
            // checkBoxErrorsOnly
            // 
            this.checkBoxErrorsOnly.AutoSize = true;
            this.checkBoxErrorsOnly.Location = new System.Drawing.Point(130, 19);
            this.checkBoxErrorsOnly.Name = "checkBoxErrorsOnly";
            this.checkBoxErrorsOnly.Size = new System.Drawing.Size(77, 17);
            this.checkBoxErrorsOnly.TabIndex = 2;
            this.checkBoxErrorsOnly.Text = "Errors Only";
            this.checkBoxErrorsOnly.UseVisualStyleBackColor = true;
            // 
            // checkBoxBackupLogs
            // 
            this.checkBoxBackupLogs.AutoSize = true;
            this.checkBoxBackupLogs.Location = new System.Drawing.Point(6, 19);
            this.checkBoxBackupLogs.Name = "checkBoxBackupLogs";
            this.checkBoxBackupLogs.Size = new System.Drawing.Size(109, 17);
            this.checkBoxBackupLogs.TabIndex = 1;
            this.checkBoxBackupLogs.Text = "Get Backup Logs";
            this.checkBoxBackupLogs.UseVisualStyleBackColor = true;
            // 
            // checkBoxRestoreLogs
            // 
            this.checkBoxRestoreLogs.AutoSize = true;
            this.checkBoxRestoreLogs.Location = new System.Drawing.Point(6, 42);
            this.checkBoxRestoreLogs.Name = "checkBoxRestoreLogs";
            this.checkBoxRestoreLogs.Size = new System.Drawing.Size(109, 17);
            this.checkBoxRestoreLogs.TabIndex = 0;
            this.checkBoxRestoreLogs.Text = "Get Restore Logs";
            this.checkBoxRestoreLogs.UseVisualStyleBackColor = true;
            // 
            // checkBoxFriday
            // 
            this.checkBoxFriday.AutoSize = true;
            this.checkBoxFriday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFriday.Location = new System.Drawing.Point(418, 249);
            this.checkBoxFriday.Name = "checkBoxFriday";
            this.checkBoxFriday.Size = new System.Drawing.Size(30, 16);
            this.checkBoxFriday.TabIndex = 34;
            this.checkBoxFriday.Text = "F";
            this.checkBoxFriday.UseVisualStyleBackColor = true;
            // 
            // checkBoxWednesday
            // 
            this.checkBoxWednesday.AutoSize = true;
            this.checkBoxWednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxWednesday.Location = new System.Drawing.Point(350, 249);
            this.checkBoxWednesday.Name = "checkBoxWednesday";
            this.checkBoxWednesday.Size = new System.Drawing.Size(33, 16);
            this.checkBoxWednesday.TabIndex = 33;
            this.checkBoxWednesday.Text = "W";
            this.checkBoxWednesday.UseVisualStyleBackColor = true;
            // 
            // checkBoxTuesday
            // 
            this.checkBoxTuesday.AutoSize = true;
            this.checkBoxTuesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTuesday.Location = new System.Drawing.Point(319, 249);
            this.checkBoxTuesday.Name = "checkBoxTuesday";
            this.checkBoxTuesday.Size = new System.Drawing.Size(29, 16);
            this.checkBoxTuesday.TabIndex = 32;
            this.checkBoxTuesday.Text = "T";
            this.checkBoxTuesday.UseVisualStyleBackColor = true;
            // 
            // checkBoxSunday
            // 
            this.checkBoxSunday.AutoSize = true;
            this.checkBoxSunday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSunday.Location = new System.Drawing.Point(253, 249);
            this.checkBoxSunday.Name = "checkBoxSunday";
            this.checkBoxSunday.Size = new System.Drawing.Size(30, 16);
            this.checkBoxSunday.TabIndex = 31;
            this.checkBoxSunday.Text = "S";
            this.checkBoxSunday.UseVisualStyleBackColor = true;
            // 
            // checkBoxMonday
            // 
            this.checkBoxMonday.AutoSize = true;
            this.checkBoxMonday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonday.Location = new System.Drawing.Point(284, 249);
            this.checkBoxMonday.Name = "checkBoxMonday";
            this.checkBoxMonday.Size = new System.Drawing.Size(33, 16);
            this.checkBoxMonday.TabIndex = 30;
            this.checkBoxMonday.Text = "M";
            this.checkBoxMonday.UseVisualStyleBackColor = true;
            // 
            // comboBoxHaveWeeklySchedule
            // 
            this.comboBoxHaveWeeklySchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveWeeklySchedule.FormattingEnabled = true;
            this.comboBoxHaveWeeklySchedule.Items.AddRange(new object[] {
            "including",
            "not including"});
            this.comboBoxHaveWeeklySchedule.Location = new System.Drawing.Point(154, 247);
            this.comboBoxHaveWeeklySchedule.Name = "comboBoxHaveWeeklySchedule";
            this.comboBoxHaveWeeklySchedule.Size = new System.Drawing.Size(96, 21);
            this.comboBoxHaveWeeklySchedule.TabIndex = 28;
            // 
            // checkBoxHaveWeeklySchedule
            // 
            this.checkBoxHaveWeeklySchedule.AutoSize = true;
            this.checkBoxHaveWeeklySchedule.Location = new System.Drawing.Point(6, 249);
            this.checkBoxHaveWeeklySchedule.Name = "checkBoxHaveWeeklySchedule";
            this.checkBoxHaveWeeklySchedule.Size = new System.Drawing.Size(110, 17);
            this.checkBoxHaveWeeklySchedule.TabIndex = 26;
            this.checkBoxHaveWeeklySchedule.Text = "Weekly Schedule";
            this.checkBoxHaveWeeklySchedule.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(431, 227);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "GB";
            // 
            // textBoxHavePoolSize
            // 
            this.textBoxHavePoolSize.Location = new System.Drawing.Point(281, 224);
            this.textBoxHavePoolSize.Name = "textBoxHavePoolSize";
            this.textBoxHavePoolSize.Size = new System.Drawing.Size(144, 20);
            this.textBoxHavePoolSize.TabIndex = 24;
            // 
            // comboBoxHavePoolSize
            // 
            this.comboBoxHavePoolSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHavePoolSize.FormattingEnabled = true;
            this.comboBoxHavePoolSize.Items.AddRange(new object[] {
            "greater than",
            "less than"});
            this.comboBoxHavePoolSize.Location = new System.Drawing.Point(154, 224);
            this.comboBoxHavePoolSize.Name = "comboBoxHavePoolSize";
            this.comboBoxHavePoolSize.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHavePoolSize.TabIndex = 23;
            // 
            // dateTimePickerLastBackup
            // 
            this.dateTimePickerLastBackup.CustomFormat = "MM/dd/yy hh:mm tt";
            this.dateTimePickerLastBackup.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLastBackup.Location = new System.Drawing.Point(281, 179);
            this.dateTimePickerLastBackup.Name = "dateTimePickerLastBackup";
            this.dateTimePickerLastBackup.Size = new System.Drawing.Size(173, 20);
            this.dateTimePickerLastBackup.TabIndex = 22;
            // 
            // comboBoxHaveLastBackup
            // 
            this.comboBoxHaveLastBackup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveLastBackup.FormattingEnabled = true;
            this.comboBoxHaveLastBackup.Items.AddRange(new object[] {
            "before",
            "after"});
            this.comboBoxHaveLastBackup.Location = new System.Drawing.Point(154, 178);
            this.comboBoxHaveLastBackup.Name = "comboBoxHaveLastBackup";
            this.comboBoxHaveLastBackup.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveLastBackup.TabIndex = 21;
            // 
            // dateTimePickerLastSuccessfulBackup
            // 
            this.dateTimePickerLastSuccessfulBackup.CustomFormat = "MM/dd/yy hh:mm tt";
            this.dateTimePickerLastSuccessfulBackup.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLastSuccessfulBackup.Location = new System.Drawing.Point(281, 156);
            this.dateTimePickerLastSuccessfulBackup.Name = "dateTimePickerLastSuccessfulBackup";
            this.dateTimePickerLastSuccessfulBackup.Size = new System.Drawing.Size(173, 20);
            this.dateTimePickerLastSuccessfulBackup.TabIndex = 20;
            // 
            // comboBoxHaveLastSuccessfulBackup
            // 
            this.comboBoxHaveLastSuccessfulBackup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveLastSuccessfulBackup.FormattingEnabled = true;
            this.comboBoxHaveLastSuccessfulBackup.Items.AddRange(new object[] {
            "before",
            "after"});
            this.comboBoxHaveLastSuccessfulBackup.Location = new System.Drawing.Point(154, 155);
            this.comboBoxHaveLastSuccessfulBackup.Name = "comboBoxHaveLastSuccessfulBackup";
            this.comboBoxHaveLastSuccessfulBackup.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveLastSuccessfulBackup.TabIndex = 19;
            // 
            // comboBoxHaveLastBackupResult
            // 
            this.comboBoxHaveLastBackupResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveLastBackupResult.FormattingEnabled = true;
            this.comboBoxHaveLastBackupResult.Items.AddRange(new object[] {
            "Completed",
            "CompletedWithWarnings",
            "CompletedWithErrors",
            "Failed",
            "NeverRun",
            "Overdue"});
            this.comboBoxHaveLastBackupResult.Location = new System.Drawing.Point(281, 132);
            this.comboBoxHaveLastBackupResult.Name = "comboBoxHaveLastBackupResult";
            this.comboBoxHaveLastBackupResult.Size = new System.Drawing.Size(173, 21);
            this.comboBoxHaveLastBackupResult.TabIndex = 18;
            // 
            // comboBoxHaveLastBackupResultCompare
            // 
            this.comboBoxHaveLastBackupResultCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveLastBackupResultCompare.FormattingEnabled = true;
            this.comboBoxHaveLastBackupResultCompare.Items.AddRange(new object[] {
            "equal to",
            "not equal to"});
            this.comboBoxHaveLastBackupResultCompare.Location = new System.Drawing.Point(154, 132);
            this.comboBoxHaveLastBackupResultCompare.Name = "comboBoxHaveLastBackupResultCompare";
            this.comboBoxHaveLastBackupResultCompare.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveLastBackupResultCompare.TabIndex = 17;
            // 
            // checkBoxHavePoolSize
            // 
            this.checkBoxHavePoolSize.AutoSize = true;
            this.checkBoxHavePoolSize.Location = new System.Drawing.Point(6, 226);
            this.checkBoxHavePoolSize.Name = "checkBoxHavePoolSize";
            this.checkBoxHavePoolSize.Size = new System.Drawing.Size(70, 17);
            this.checkBoxHavePoolSize.TabIndex = 16;
            this.checkBoxHavePoolSize.Text = "Pool Size";
            this.checkBoxHavePoolSize.UseVisualStyleBackColor = true;
            // 
            // checkBoxHaveLastBackup
            // 
            this.checkBoxHaveLastBackup.AutoSize = true;
            this.checkBoxHaveLastBackup.Location = new System.Drawing.Point(6, 180);
            this.checkBoxHaveLastBackup.Name = "checkBoxHaveLastBackup";
            this.checkBoxHaveLastBackup.Size = new System.Drawing.Size(86, 17);
            this.checkBoxHaveLastBackup.TabIndex = 15;
            this.checkBoxHaveLastBackup.Text = "Last Backup";
            this.checkBoxHaveLastBackup.UseVisualStyleBackColor = true;
            // 
            // checkBoxHaveLastSuccessfulBackup
            // 
            this.checkBoxHaveLastSuccessfulBackup.AutoSize = true;
            this.checkBoxHaveLastSuccessfulBackup.Location = new System.Drawing.Point(6, 157);
            this.checkBoxHaveLastSuccessfulBackup.Name = "checkBoxHaveLastSuccessfulBackup";
            this.checkBoxHaveLastSuccessfulBackup.Size = new System.Drawing.Size(141, 17);
            this.checkBoxHaveLastSuccessfulBackup.TabIndex = 14;
            this.checkBoxHaveLastSuccessfulBackup.Text = "Last Successful Backup";
            this.checkBoxHaveLastSuccessfulBackup.UseVisualStyleBackColor = true;
            // 
            // checkBoxHaveLastBackupResult
            // 
            this.checkBoxHaveLastBackupResult.AutoSize = true;
            this.checkBoxHaveLastBackupResult.Location = new System.Drawing.Point(6, 134);
            this.checkBoxHaveLastBackupResult.Name = "checkBoxHaveLastBackupResult";
            this.checkBoxHaveLastBackupResult.Size = new System.Drawing.Size(119, 17);
            this.checkBoxHaveLastBackupResult.TabIndex = 13;
            this.checkBoxHaveLastBackupResult.Text = "Last Backup Result";
            this.checkBoxHaveLastBackupResult.UseVisualStyleBackColor = true;
            // 
            // comboBoxHaveSatelliteOperatingMode
            // 
            this.comboBoxHaveSatelliteOperatingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveSatelliteOperatingMode.FormattingEnabled = true;
            this.comboBoxHaveSatelliteOperatingMode.Items.AddRange(new object[] {
            "BypassSatellite",
            "DontReplicate",
            "NormalReplication",
            "ReplicateCustomerOnly",
            "RestoreOnly"});
            this.comboBoxHaveSatelliteOperatingMode.Location = new System.Drawing.Point(281, 40);
            this.comboBoxHaveSatelliteOperatingMode.Name = "comboBoxHaveSatelliteOperatingMode";
            this.comboBoxHaveSatelliteOperatingMode.Size = new System.Drawing.Size(173, 21);
            this.comboBoxHaveSatelliteOperatingMode.TabIndex = 12;
            // 
            // comboBoxHaveSatelliteOperatingModeCompare
            // 
            this.comboBoxHaveSatelliteOperatingModeCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveSatelliteOperatingModeCompare.FormattingEnabled = true;
            this.comboBoxHaveSatelliteOperatingModeCompare.Items.AddRange(new object[] {
            "equal to",
            "not equal to"});
            this.comboBoxHaveSatelliteOperatingModeCompare.Location = new System.Drawing.Point(154, 40);
            this.comboBoxHaveSatelliteOperatingModeCompare.Name = "comboBoxHaveSatelliteOperatingModeCompare";
            this.comboBoxHaveSatelliteOperatingModeCompare.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveSatelliteOperatingModeCompare.TabIndex = 11;
            // 
            // checkBoxHaveSatelliteOperatingMode
            // 
            this.checkBoxHaveSatelliteOperatingMode.AutoSize = true;
            this.checkBoxHaveSatelliteOperatingMode.Location = new System.Drawing.Point(6, 42);
            this.checkBoxHaveSatelliteOperatingMode.Name = "checkBoxHaveSatelliteOperatingMode";
            this.checkBoxHaveSatelliteOperatingMode.Size = new System.Drawing.Size(142, 17);
            this.checkBoxHaveSatelliteOperatingMode.TabIndex = 10;
            this.checkBoxHaveSatelliteOperatingMode.Text = "Satellite Operating Mode";
            this.checkBoxHaveSatelliteOperatingMode.UseVisualStyleBackColor = true;
            // 
            // comboBoxHaveOperatingMode
            // 
            this.comboBoxHaveOperatingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveOperatingMode.FormattingEnabled = true;
            this.comboBoxHaveOperatingMode.Items.AddRange(new object[] {
            "Default",
            "PausedReplication",
            "RedirectedBackups"});
            this.comboBoxHaveOperatingMode.Location = new System.Drawing.Point(281, 109);
            this.comboBoxHaveOperatingMode.Name = "comboBoxHaveOperatingMode";
            this.comboBoxHaveOperatingMode.Size = new System.Drawing.Size(173, 21);
            this.comboBoxHaveOperatingMode.TabIndex = 9;
            // 
            // comboBoxHaveOperatingModeCompare
            // 
            this.comboBoxHaveOperatingModeCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveOperatingModeCompare.FormattingEnabled = true;
            this.comboBoxHaveOperatingModeCompare.Items.AddRange(new object[] {
            "equal to",
            "not equal to"});
            this.comboBoxHaveOperatingModeCompare.Location = new System.Drawing.Point(154, 109);
            this.comboBoxHaveOperatingModeCompare.Name = "comboBoxHaveOperatingModeCompare";
            this.comboBoxHaveOperatingModeCompare.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveOperatingModeCompare.TabIndex = 8;
            // 
            // checkBoxHaveOperatingMode
            // 
            this.checkBoxHaveOperatingMode.AutoSize = true;
            this.checkBoxHaveOperatingMode.Location = new System.Drawing.Point(6, 111);
            this.checkBoxHaveOperatingMode.Name = "checkBoxHaveOperatingMode";
            this.checkBoxHaveOperatingMode.Size = new System.Drawing.Size(102, 17);
            this.checkBoxHaveOperatingMode.TabIndex = 7;
            this.checkBoxHaveOperatingMode.Text = "Operating Mode";
            this.checkBoxHaveOperatingMode.UseVisualStyleBackColor = true;
            // 
            // textBoxHaveMailServer
            // 
            this.textBoxHaveMailServer.Location = new System.Drawing.Point(281, 86);
            this.textBoxHaveMailServer.Name = "textBoxHaveMailServer";
            this.textBoxHaveMailServer.Size = new System.Drawing.Size(173, 20);
            this.textBoxHaveMailServer.TabIndex = 6;
            // 
            // comboBoxHaveMailServer
            // 
            this.comboBoxHaveMailServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveMailServer.FormattingEnabled = true;
            this.comboBoxHaveMailServer.Items.AddRange(new object[] {
            "not specified",
            "equal to"});
            this.comboBoxHaveMailServer.Location = new System.Drawing.Point(154, 86);
            this.comboBoxHaveMailServer.Name = "comboBoxHaveMailServer";
            this.comboBoxHaveMailServer.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveMailServer.TabIndex = 5;
            // 
            // checkBoxHaveMailServer
            // 
            this.checkBoxHaveMailServer.AutoSize = true;
            this.checkBoxHaveMailServer.Location = new System.Drawing.Point(6, 88);
            this.checkBoxHaveMailServer.Name = "checkBoxHaveMailServer";
            this.checkBoxHaveMailServer.Size = new System.Drawing.Size(79, 17);
            this.checkBoxHaveMailServer.TabIndex = 4;
            this.checkBoxHaveMailServer.Text = "Mail Server";
            this.checkBoxHaveMailServer.UseVisualStyleBackColor = true;
            // 
            // checkBoxHaveName
            // 
            this.checkBoxHaveName.AutoSize = true;
            this.checkBoxHaveName.Location = new System.Drawing.Point(6, 65);
            this.checkBoxHaveName.Name = "checkBoxHaveName";
            this.checkBoxHaveName.Size = new System.Drawing.Size(54, 17);
            this.checkBoxHaveName.TabIndex = 3;
            this.checkBoxHaveName.Text = "Name";
            this.checkBoxHaveName.UseVisualStyleBackColor = true;
            // 
            // textBoxHaveName
            // 
            this.textBoxHaveName.Location = new System.Drawing.Point(281, 63);
            this.textBoxHaveName.Name = "textBoxHaveName";
            this.textBoxHaveName.Size = new System.Drawing.Size(173, 20);
            this.textBoxHaveName.TabIndex = 2;
            // 
            // comboBoxHaveName
            // 
            this.comboBoxHaveName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHaveName.FormattingEnabled = true;
            this.comboBoxHaveName.Items.AddRange(new object[] {
            "equal to",
            "containing",
            "not equal to"});
            this.comboBoxHaveName.Location = new System.Drawing.Point(154, 63);
            this.comboBoxHaveName.Name = "comboBoxHaveName";
            this.comboBoxHaveName.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHaveName.TabIndex = 1;
            // 
            // checkBoxHaveSatellite
            // 
            this.checkBoxHaveSatellite.AutoSize = true;
            this.checkBoxHaveSatellite.Location = new System.Drawing.Point(6, 19);
            this.checkBoxHaveSatellite.Name = "checkBoxHaveSatellite";
            this.checkBoxHaveSatellite.Size = new System.Drawing.Size(63, 17);
            this.checkBoxHaveSatellite.TabIndex = 0;
            this.checkBoxHaveSatellite.Text = "Satellite";
            this.checkBoxHaveSatellite.UseVisualStyleBackColor = true;
            // 
            // dataGridViewResults
            // 
            this.dataGridViewResults.AllowUserToAddRows = false;
            this.dataGridViewResults.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(223)))), ((int)(((byte)(253)))));
            this.dataGridViewResults.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewResults.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResults.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResults.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewResults.Name = "dataGridViewResults";
            this.dataGridViewResults.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewResults.RowHeadersVisible = false;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResults.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewResults.Size = new System.Drawing.Size(526, 547);
            this.dataGridViewResults.TabIndex = 999;
            this.dataGridViewResults.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewResults_CellDoubleClick);
            // 
            // textBoxWritePath
            // 
            this.textBoxWritePath.Location = new System.Drawing.Point(234, 315);
            this.textBoxWritePath.Name = "textBoxWritePath";
            this.textBoxWritePath.Size = new System.Drawing.Size(245, 20);
            this.textBoxWritePath.TabIndex = 1001;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(140, 318);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 1002;
            this.label10.Text = "Write/Read Path";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPageTaskQuery);
            this.tabControl1.Controls.Add(this.tabPageAgentConfig);
            this.tabControl1.Location = new System.Drawing.Point(10, 341);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(519, 585);
            this.tabControl1.TabIndex = 1003;
            // 
            // tabPageTaskQuery
            // 
            this.tabPageTaskQuery.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageTaskQuery.Controls.Add(this.tabControlQueryTypes);
            this.tabPageTaskQuery.Location = new System.Drawing.Point(4, 22);
            this.tabPageTaskQuery.Name = "tabPageTaskQuery";
            this.tabPageTaskQuery.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTaskQuery.Size = new System.Drawing.Size(511, 559);
            this.tabPageTaskQuery.TabIndex = 0;
            this.tabPageTaskQuery.Text = "Task Query";
            // 
            // tabControlQueryTypes
            // 
            this.tabControlQueryTypes.Controls.Add(this.tabPage1);
            this.tabControlQueryTypes.Controls.Add(this.tabPage2);
            this.tabControlQueryTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlQueryTypes.Location = new System.Drawing.Point(3, 3);
            this.tabControlQueryTypes.Name = "tabControlQueryTypes";
            this.tabControlQueryTypes.SelectedIndex = 0;
            this.tabControlQueryTypes.Size = new System.Drawing.Size(505, 553);
            this.tabControlQueryTypes.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBoxThatWhat);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(497, 527);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.buttonAddDSCJob);
            this.tabPage2.Controls.Add(this.buttonAddDSC);
            this.tabPage2.Controls.Add(this.buttonAddDSCustomer);
            this.tabPage2.Controls.Add(this.buttonAddDSS);
            this.tabPage2.Controls.Add(this.buttonChecklistAddSite);
            this.tabPage2.Controls.Add(this.buttonChecklistAddAgent);
            this.tabPage2.Controls.Add(this.buttonChecklistAddGroup);
            this.tabPage2.Controls.Add(this.buttonChecklistAddSubsite);
            this.tabPage2.Controls.Add(this.textBoxSaveLoadList);
            this.tabPage2.Controls.Add(this.buttonLoadList);
            this.tabPage2.Controls.Add(this.buttonSaveTaskList);
            this.tabPage2.Controls.Add(this.buttonChecklistAddTask);
            this.tabPage2.Controls.Add(this.buttonChecklistRemoveSelected);
            this.tabPage2.Controls.Add(this.buttonChecklistSelectAll);
            this.tabPage2.Controls.Add(this.checkedListBoxQuery);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(497, 527);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Checklist";
            // 
            // buttonAddDSCJob
            // 
            this.buttonAddDSCJob.Location = new System.Drawing.Point(391, 236);
            this.buttonAddDSCJob.Name = "buttonAddDSCJob";
            this.buttonAddDSCJob.Size = new System.Drawing.Size(94, 23);
            this.buttonAddDSCJob.TabIndex = 14;
            this.buttonAddDSCJob.Text = "Add DSC Job";
            this.buttonAddDSCJob.UseVisualStyleBackColor = true;
            this.buttonAddDSCJob.Click += new System.EventHandler(this.buttonChecklistAddDSCJob_Click);
            // 
            // buttonAddDSC
            // 
            this.buttonAddDSC.Location = new System.Drawing.Point(391, 207);
            this.buttonAddDSC.Name = "buttonAddDSC";
            this.buttonAddDSC.Size = new System.Drawing.Size(94, 23);
            this.buttonAddDSC.TabIndex = 13;
            this.buttonAddDSC.Text = "Add DSC";
            this.buttonAddDSC.UseVisualStyleBackColor = true;
            this.buttonAddDSC.Click += new System.EventHandler(this.buttonChecklistAddDSC_Click);
            // 
            // buttonAddDSCustomer
            // 
            this.buttonAddDSCustomer.Location = new System.Drawing.Point(391, 178);
            this.buttonAddDSCustomer.Name = "buttonAddDSCustomer";
            this.buttonAddDSCustomer.Size = new System.Drawing.Size(94, 23);
            this.buttonAddDSCustomer.TabIndex = 12;
            this.buttonAddDSCustomer.Text = "Add DS Cust.";
            this.buttonAddDSCustomer.UseVisualStyleBackColor = true;
            this.buttonAddDSCustomer.Click += new System.EventHandler(this.buttonChecklistAddDSCustomer_Click);
            // 
            // buttonAddDSS
            // 
            this.buttonAddDSS.Location = new System.Drawing.Point(391, 149);
            this.buttonAddDSS.Name = "buttonAddDSS";
            this.buttonAddDSS.Size = new System.Drawing.Size(94, 23);
            this.buttonAddDSS.TabIndex = 11;
            this.buttonAddDSS.Text = "Add DSS";
            this.buttonAddDSS.UseVisualStyleBackColor = true;
            this.buttonAddDSS.Click += new System.EventHandler(this.buttonChecklistAddDSS_Click);
            // 
            // buttonChecklistAddSite
            // 
            this.buttonChecklistAddSite.Location = new System.Drawing.Point(391, 3);
            this.buttonChecklistAddSite.Name = "buttonChecklistAddSite";
            this.buttonChecklistAddSite.Size = new System.Drawing.Size(94, 23);
            this.buttonChecklistAddSite.TabIndex = 10;
            this.buttonChecklistAddSite.Text = "Add Site";
            this.buttonChecklistAddSite.UseVisualStyleBackColor = true;
            this.buttonChecklistAddSite.Click += new System.EventHandler(this.buttonChecklistAddSite_Click);
            // 
            // buttonChecklistAddAgent
            // 
            this.buttonChecklistAddAgent.Location = new System.Drawing.Point(391, 91);
            this.buttonChecklistAddAgent.Name = "buttonChecklistAddAgent";
            this.buttonChecklistAddAgent.Size = new System.Drawing.Size(94, 23);
            this.buttonChecklistAddAgent.TabIndex = 9;
            this.buttonChecklistAddAgent.Text = "Add Agent";
            this.buttonChecklistAddAgent.UseVisualStyleBackColor = true;
            this.buttonChecklistAddAgent.Click += new System.EventHandler(this.buttonChecklistAddAgent_Click);
            // 
            // buttonChecklistAddGroup
            // 
            this.buttonChecklistAddGroup.Location = new System.Drawing.Point(391, 61);
            this.buttonChecklistAddGroup.Name = "buttonChecklistAddGroup";
            this.buttonChecklistAddGroup.Size = new System.Drawing.Size(94, 23);
            this.buttonChecklistAddGroup.TabIndex = 8;
            this.buttonChecklistAddGroup.Text = "Add Group";
            this.buttonChecklistAddGroup.UseVisualStyleBackColor = true;
            this.buttonChecklistAddGroup.Click += new System.EventHandler(this.buttonChecklistAddGroup_Click);
            // 
            // buttonChecklistAddSubsite
            // 
            this.buttonChecklistAddSubsite.Location = new System.Drawing.Point(391, 32);
            this.buttonChecklistAddSubsite.Name = "buttonChecklistAddSubsite";
            this.buttonChecklistAddSubsite.Size = new System.Drawing.Size(94, 23);
            this.buttonChecklistAddSubsite.TabIndex = 7;
            this.buttonChecklistAddSubsite.Text = "Add Subsite";
            this.buttonChecklistAddSubsite.UseVisualStyleBackColor = true;
            this.buttonChecklistAddSubsite.Click += new System.EventHandler(this.buttonChecklistAddSubsite_Click);
            // 
            // textBoxSaveLoadList
            // 
            this.textBoxSaveLoadList.Location = new System.Drawing.Point(391, 431);
            this.textBoxSaveLoadList.Name = "textBoxSaveLoadList";
            this.textBoxSaveLoadList.Size = new System.Drawing.Size(94, 20);
            this.textBoxSaveLoadList.TabIndex = 6;
            this.textBoxSaveLoadList.Text = "ListName.txt";
            // 
            // buttonLoadList
            // 
            this.buttonLoadList.Location = new System.Drawing.Point(410, 402);
            this.buttonLoadList.Name = "buttonLoadList";
            this.buttonLoadList.Size = new System.Drawing.Size(75, 23);
            this.buttonLoadList.TabIndex = 5;
            this.buttonLoadList.Text = "Load List";
            this.buttonLoadList.UseVisualStyleBackColor = true;
            this.buttonLoadList.Click += new System.EventHandler(this.buttonLoadList_Click);
            // 
            // buttonSaveTaskList
            // 
            this.buttonSaveTaskList.Location = new System.Drawing.Point(410, 373);
            this.buttonSaveTaskList.Name = "buttonSaveTaskList";
            this.buttonSaveTaskList.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveTaskList.TabIndex = 4;
            this.buttonSaveTaskList.Text = "Save List";
            this.buttonSaveTaskList.UseVisualStyleBackColor = true;
            this.buttonSaveTaskList.Click += new System.EventHandler(this.buttonSaveTaskList_Click);
            // 
            // buttonChecklistAddTask
            // 
            this.buttonChecklistAddTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChecklistAddTask.Location = new System.Drawing.Point(391, 120);
            this.buttonChecklistAddTask.Name = "buttonChecklistAddTask";
            this.buttonChecklistAddTask.Size = new System.Drawing.Size(94, 23);
            this.buttonChecklistAddTask.TabIndex = 3;
            this.buttonChecklistAddTask.Text = "Add Task";
            this.buttonChecklistAddTask.UseVisualStyleBackColor = true;
            this.buttonChecklistAddTask.Click += new System.EventHandler(this.buttonChecklistAddTask_Click);
            // 
            // buttonChecklistRemoveSelected
            // 
            this.buttonChecklistRemoveSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChecklistRemoveSelected.Location = new System.Drawing.Point(391, 311);
            this.buttonChecklistRemoveSelected.Name = "buttonChecklistRemoveSelected";
            this.buttonChecklistRemoveSelected.Size = new System.Drawing.Size(103, 23);
            this.buttonChecklistRemoveSelected.TabIndex = 2;
            this.buttonChecklistRemoveSelected.Text = "Remove Selected";
            this.buttonChecklistRemoveSelected.UseVisualStyleBackColor = true;
            this.buttonChecklistRemoveSelected.Click += new System.EventHandler(this.buttonChecklistRemoveSelected_Click);
            // 
            // buttonChecklistSelectAll
            // 
            this.buttonChecklistSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChecklistSelectAll.Location = new System.Drawing.Point(419, 282);
            this.buttonChecklistSelectAll.Name = "buttonChecklistSelectAll";
            this.buttonChecklistSelectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonChecklistSelectAll.TabIndex = 1;
            this.buttonChecklistSelectAll.Text = "Select All";
            this.buttonChecklistSelectAll.UseVisualStyleBackColor = true;
            this.buttonChecklistSelectAll.Click += new System.EventHandler(this.buttonChecklistSelectAll_Click);
            // 
            // checkedListBoxQuery
            // 
            this.checkedListBoxQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxQuery.FormattingEnabled = true;
            this.checkedListBoxQuery.Location = new System.Drawing.Point(0, -1);
            this.checkedListBoxQuery.Name = "checkedListBoxQuery";
            this.checkedListBoxQuery.Size = new System.Drawing.Size(385, 499);
            this.checkedListBoxQuery.TabIndex = 0;
            // 
            // tabPageAgentConfig
            // 
            this.tabPageAgentConfig.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageAgentConfig.Controls.Add(this.groupBoxAgentWithWhat);
            this.tabPageAgentConfig.Location = new System.Drawing.Point(4, 22);
            this.tabPageAgentConfig.Name = "tabPageAgentConfig";
            this.tabPageAgentConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAgentConfig.Size = new System.Drawing.Size(511, 559);
            this.tabPageAgentConfig.TabIndex = 1;
            this.tabPageAgentConfig.Text = "Agent Config";
            // 
            // groupBoxAgentWithWhat
            // 
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxAgentConfigMailServerPort);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentConfigMailServerPort);
            this.groupBoxAgentWithWhat.Controls.Add(this.groupBox3);
            this.groupBoxAgentWithWhat.Controls.Add(this.groupBox2);
            this.groupBoxAgentWithWhat.Controls.Add(this.groupBox1);
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxAgentMailServerFromAddress);
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxAgentMailServerPassword);
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxAgentMailServerDomain);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentMailServerFromAddress);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentMailServerPassword);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentMailServerDomain);
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxcheckBoxAgentMailServerUsername);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentMailServerUsername);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentConfigNotificationsSuccess);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentConfigNotificationsFailure);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentConfigNotificationsError);
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxAgentConfigRecipients);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentConfigRecipients);
            this.groupBoxAgentWithWhat.Controls.Add(this.textBoxAgentConfigMailServer);
            this.groupBoxAgentWithWhat.Controls.Add(this.checkBoxAgentConfigMailServer);
            this.groupBoxAgentWithWhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxAgentWithWhat.Location = new System.Drawing.Point(3, 3);
            this.groupBoxAgentWithWhat.Name = "groupBoxAgentWithWhat";
            this.groupBoxAgentWithWhat.Size = new System.Drawing.Size(505, 553);
            this.groupBoxAgentWithWhat.TabIndex = 0;
            this.groupBoxAgentWithWhat.TabStop = false;
            this.groupBoxAgentWithWhat.Text = "with a(n)...";
            // 
            // textBoxAgentConfigMailServerPort
            // 
            this.textBoxAgentConfigMailServerPort.Location = new System.Drawing.Point(255, 41);
            this.textBoxAgentConfigMailServerPort.Name = "textBoxAgentConfigMailServerPort";
            this.textBoxAgentConfigMailServerPort.Size = new System.Drawing.Size(221, 20);
            this.textBoxAgentConfigMailServerPort.TabIndex = 3;
            // 
            // checkBoxAgentConfigMailServerPort
            // 
            this.checkBoxAgentConfigMailServerPort.AutoSize = true;
            this.checkBoxAgentConfigMailServerPort.Location = new System.Drawing.Point(6, 42);
            this.checkBoxAgentConfigMailServerPort.Name = "checkBoxAgentConfigMailServerPort";
            this.checkBoxAgentConfigMailServerPort.Size = new System.Drawing.Size(198, 17);
            this.checkBoxAgentConfigMailServerPort.TabIndex = 2;
            this.checkBoxAgentConfigMailServerPort.Text = "Notification Mail Server Port equal to";
            this.checkBoxAgentConfigMailServerPort.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonSuccessOn);
            this.groupBox3.Controls.Add(this.radioButtonSuccessOff);
            this.groupBox3.Location = new System.Drawing.Point(145, 240);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(88, 30);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            // 
            // radioButtonSuccessOn
            // 
            this.radioButtonSuccessOn.AutoSize = true;
            this.radioButtonSuccessOn.Location = new System.Drawing.Point(3, 12);
            this.radioButtonSuccessOn.Name = "radioButtonSuccessOn";
            this.radioButtonSuccessOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonSuccessOn.TabIndex = 21;
            this.radioButtonSuccessOn.TabStop = true;
            this.radioButtonSuccessOn.Text = "On";
            this.radioButtonSuccessOn.UseVisualStyleBackColor = true;
            // 
            // radioButtonSuccessOff
            // 
            this.radioButtonSuccessOff.AutoSize = true;
            this.radioButtonSuccessOff.Location = new System.Drawing.Point(43, 12);
            this.radioButtonSuccessOff.Name = "radioButtonSuccessOff";
            this.radioButtonSuccessOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonSuccessOff.TabIndex = 22;
            this.radioButtonSuccessOff.TabStop = true;
            this.radioButtonSuccessOff.Text = "Off";
            this.radioButtonSuccessOff.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonFailuresOn);
            this.groupBox2.Controls.Add(this.radioButtonFailuresOff);
            this.groupBox2.Location = new System.Drawing.Point(145, 205);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(88, 30);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            // 
            // radioButtonFailuresOn
            // 
            this.radioButtonFailuresOn.AutoSize = true;
            this.radioButtonFailuresOn.Location = new System.Drawing.Point(3, 12);
            this.radioButtonFailuresOn.Name = "radioButtonFailuresOn";
            this.radioButtonFailuresOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonFailuresOn.TabIndex = 18;
            this.radioButtonFailuresOn.TabStop = true;
            this.radioButtonFailuresOn.Text = "On";
            this.radioButtonFailuresOn.UseVisualStyleBackColor = true;
            // 
            // radioButtonFailuresOff
            // 
            this.radioButtonFailuresOff.AutoSize = true;
            this.radioButtonFailuresOff.Location = new System.Drawing.Point(43, 12);
            this.radioButtonFailuresOff.Name = "radioButtonFailuresOff";
            this.radioButtonFailuresOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonFailuresOff.TabIndex = 19;
            this.radioButtonFailuresOff.TabStop = true;
            this.radioButtonFailuresOff.Text = "Off";
            this.radioButtonFailuresOff.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonErrorsOn);
            this.groupBox1.Controls.Add(this.radioButtonErrorsOff);
            this.groupBox1.Location = new System.Drawing.Point(145, 170);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(88, 30);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // radioButtonErrorsOn
            // 
            this.radioButtonErrorsOn.AutoSize = true;
            this.radioButtonErrorsOn.Location = new System.Drawing.Point(3, 12);
            this.radioButtonErrorsOn.Name = "radioButtonErrorsOn";
            this.radioButtonErrorsOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonErrorsOn.TabIndex = 15;
            this.radioButtonErrorsOn.TabStop = true;
            this.radioButtonErrorsOn.Text = "On";
            this.radioButtonErrorsOn.UseVisualStyleBackColor = true;
            // 
            // radioButtonErrorsOff
            // 
            this.radioButtonErrorsOff.AutoSize = true;
            this.radioButtonErrorsOff.Location = new System.Drawing.Point(43, 12);
            this.radioButtonErrorsOff.Name = "radioButtonErrorsOff";
            this.radioButtonErrorsOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonErrorsOff.TabIndex = 16;
            this.radioButtonErrorsOff.TabStop = true;
            this.radioButtonErrorsOff.Text = "Off";
            this.radioButtonErrorsOff.UseVisualStyleBackColor = true;
            // 
            // textBoxAgentMailServerFromAddress
            // 
            this.textBoxAgentMailServerFromAddress.Location = new System.Drawing.Point(255, 155);
            this.textBoxAgentMailServerFromAddress.Name = "textBoxAgentMailServerFromAddress";
            this.textBoxAgentMailServerFromAddress.Size = new System.Drawing.Size(221, 20);
            this.textBoxAgentMailServerFromAddress.TabIndex = 13;
            // 
            // textBoxAgentMailServerPassword
            // 
            this.textBoxAgentMailServerPassword.Location = new System.Drawing.Point(255, 132);
            this.textBoxAgentMailServerPassword.Name = "textBoxAgentMailServerPassword";
            this.textBoxAgentMailServerPassword.Size = new System.Drawing.Size(221, 20);
            this.textBoxAgentMailServerPassword.TabIndex = 11;
            // 
            // textBoxAgentMailServerDomain
            // 
            this.textBoxAgentMailServerDomain.Location = new System.Drawing.Point(255, 109);
            this.textBoxAgentMailServerDomain.Name = "textBoxAgentMailServerDomain";
            this.textBoxAgentMailServerDomain.Size = new System.Drawing.Size(221, 20);
            this.textBoxAgentMailServerDomain.TabIndex = 9;
            // 
            // checkBoxAgentMailServerFromAddress
            // 
            this.checkBoxAgentMailServerFromAddress.AutoSize = true;
            this.checkBoxAgentMailServerFromAddress.Location = new System.Drawing.Point(6, 157);
            this.checkBoxAgentMailServerFromAddress.Name = "checkBoxAgentMailServerFromAddress";
            this.checkBoxAgentMailServerFromAddress.Size = new System.Drawing.Size(243, 17);
            this.checkBoxAgentMailServerFromAddress.TabIndex = 12;
            this.checkBoxAgentMailServerFromAddress.Text = "Notification Mail Server From Address equal to";
            this.checkBoxAgentMailServerFromAddress.UseVisualStyleBackColor = true;
            // 
            // checkBoxAgentMailServerPassword
            // 
            this.checkBoxAgentMailServerPassword.AutoSize = true;
            this.checkBoxAgentMailServerPassword.Location = new System.Drawing.Point(6, 134);
            this.checkBoxAgentMailServerPassword.Name = "checkBoxAgentMailServerPassword";
            this.checkBoxAgentMailServerPassword.Size = new System.Drawing.Size(225, 17);
            this.checkBoxAgentMailServerPassword.TabIndex = 10;
            this.checkBoxAgentMailServerPassword.Text = "Notification Mail Server Password equal to";
            this.checkBoxAgentMailServerPassword.UseVisualStyleBackColor = true;
            // 
            // checkBoxAgentMailServerDomain
            // 
            this.checkBoxAgentMailServerDomain.AutoSize = true;
            this.checkBoxAgentMailServerDomain.Location = new System.Drawing.Point(6, 111);
            this.checkBoxAgentMailServerDomain.Name = "checkBoxAgentMailServerDomain";
            this.checkBoxAgentMailServerDomain.Size = new System.Drawing.Size(215, 17);
            this.checkBoxAgentMailServerDomain.TabIndex = 8;
            this.checkBoxAgentMailServerDomain.Text = "Notification Mail Server Domain equal to";
            this.checkBoxAgentMailServerDomain.UseVisualStyleBackColor = true;
            // 
            // textBoxcheckBoxAgentMailServerUsername
            // 
            this.textBoxcheckBoxAgentMailServerUsername.Location = new System.Drawing.Point(255, 86);
            this.textBoxcheckBoxAgentMailServerUsername.Name = "textBoxcheckBoxAgentMailServerUsername";
            this.textBoxcheckBoxAgentMailServerUsername.Size = new System.Drawing.Size(221, 20);
            this.textBoxcheckBoxAgentMailServerUsername.TabIndex = 7;
            // 
            // checkBoxAgentMailServerUsername
            // 
            this.checkBoxAgentMailServerUsername.AutoSize = true;
            this.checkBoxAgentMailServerUsername.Location = new System.Drawing.Point(6, 88);
            this.checkBoxAgentMailServerUsername.Name = "checkBoxAgentMailServerUsername";
            this.checkBoxAgentMailServerUsername.Size = new System.Drawing.Size(227, 17);
            this.checkBoxAgentMailServerUsername.TabIndex = 6;
            this.checkBoxAgentMailServerUsername.Text = "Notification Mail Server Username equal to";
            this.checkBoxAgentMailServerUsername.UseVisualStyleBackColor = true;
            // 
            // checkBoxAgentConfigNotificationsSuccess
            // 
            this.checkBoxAgentConfigNotificationsSuccess.AutoSize = true;
            this.checkBoxAgentConfigNotificationsSuccess.Location = new System.Drawing.Point(5, 253);
            this.checkBoxAgentConfigNotificationsSuccess.Name = "checkBoxAgentConfigNotificationsSuccess";
            this.checkBoxAgentConfigNotificationsSuccess.Size = new System.Drawing.Size(138, 17);
            this.checkBoxAgentConfigNotificationsSuccess.TabIndex = 20;
            this.checkBoxAgentConfigNotificationsSuccess.Text = "Notification on Success";
            this.checkBoxAgentConfigNotificationsSuccess.UseVisualStyleBackColor = true;
            // 
            // checkBoxAgentConfigNotificationsFailure
            // 
            this.checkBoxAgentConfigNotificationsFailure.AutoSize = true;
            this.checkBoxAgentConfigNotificationsFailure.Location = new System.Drawing.Point(6, 218);
            this.checkBoxAgentConfigNotificationsFailure.Name = "checkBoxAgentConfigNotificationsFailure";
            this.checkBoxAgentConfigNotificationsFailure.Size = new System.Drawing.Size(133, 17);
            this.checkBoxAgentConfigNotificationsFailure.TabIndex = 17;
            this.checkBoxAgentConfigNotificationsFailure.Text = "Notification on Failures";
            this.checkBoxAgentConfigNotificationsFailure.UseVisualStyleBackColor = true;
            // 
            // checkBoxAgentConfigNotificationsError
            // 
            this.checkBoxAgentConfigNotificationsError.AutoSize = true;
            this.checkBoxAgentConfigNotificationsError.Location = new System.Drawing.Point(6, 183);
            this.checkBoxAgentConfigNotificationsError.Name = "checkBoxAgentConfigNotificationsError";
            this.checkBoxAgentConfigNotificationsError.Size = new System.Drawing.Size(124, 17);
            this.checkBoxAgentConfigNotificationsError.TabIndex = 14;
            this.checkBoxAgentConfigNotificationsError.Text = "Notification on Errors";
            this.checkBoxAgentConfigNotificationsError.UseVisualStyleBackColor = true;
            // 
            // textBoxAgentConfigRecipients
            // 
            this.textBoxAgentConfigRecipients.Location = new System.Drawing.Point(255, 63);
            this.textBoxAgentConfigRecipients.Name = "textBoxAgentConfigRecipients";
            this.textBoxAgentConfigRecipients.Size = new System.Drawing.Size(221, 20);
            this.textBoxAgentConfigRecipients.TabIndex = 5;
            // 
            // checkBoxAgentConfigRecipients
            // 
            this.checkBoxAgentConfigRecipients.AutoSize = true;
            this.checkBoxAgentConfigRecipients.Location = new System.Drawing.Point(6, 65);
            this.checkBoxAgentConfigRecipients.Name = "checkBoxAgentConfigRecipients";
            this.checkBoxAgentConfigRecipients.Size = new System.Drawing.Size(173, 17);
            this.checkBoxAgentConfigRecipients.TabIndex = 4;
            this.checkBoxAgentConfigRecipients.Text = "Notification Recipients equal to";
            this.checkBoxAgentConfigRecipients.UseVisualStyleBackColor = true;
            // 
            // textBoxAgentConfigMailServer
            // 
            this.textBoxAgentConfigMailServer.Location = new System.Drawing.Point(255, 17);
            this.textBoxAgentConfigMailServer.Name = "textBoxAgentConfigMailServer";
            this.textBoxAgentConfigMailServer.Size = new System.Drawing.Size(221, 20);
            this.textBoxAgentConfigMailServer.TabIndex = 1;
            // 
            // checkBoxAgentConfigMailServer
            // 
            this.checkBoxAgentConfigMailServer.AutoSize = true;
            this.checkBoxAgentConfigMailServer.Location = new System.Drawing.Point(6, 19);
            this.checkBoxAgentConfigMailServer.Name = "checkBoxAgentConfigMailServer";
            this.checkBoxAgentConfigMailServer.Size = new System.Drawing.Size(176, 17);
            this.checkBoxAgentConfigMailServer.TabIndex = 0;
            this.checkBoxAgentConfigMailServer.Text = "Notification Mail Server equal to";
            this.checkBoxAgentConfigMailServer.UseVisualStyleBackColor = true;
            // 
            // tabControlResults
            // 
            this.tabControlResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlResults.Controls.Add(this.tabPage3);
            this.tabControlResults.Controls.Add(this.tabPageTextBoxResults);
            this.tabControlResults.Location = new System.Drawing.Point(531, 328);
            this.tabControlResults.Name = "tabControlResults";
            this.tabControlResults.SelectedIndex = 0;
            this.tabControlResults.Size = new System.Drawing.Size(546, 598);
            this.tabControlResults.TabIndex = 1004;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.groupBoxDataGrid);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(538, 572);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "DataGrid Results";
            // 
            // groupBoxDataGrid
            // 
            this.groupBoxDataGrid.Controls.Add(this.dataGridViewResults);
            this.groupBoxDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxDataGrid.Location = new System.Drawing.Point(3, 3);
            this.groupBoxDataGrid.Name = "groupBoxDataGrid";
            this.groupBoxDataGrid.Size = new System.Drawing.Size(532, 566);
            this.groupBoxDataGrid.TabIndex = 0;
            this.groupBoxDataGrid.TabStop = false;
            // 
            // tabPageTextBoxResults
            // 
            this.tabPageTextBoxResults.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageTextBoxResults.Controls.Add(this.richTextBoxResults);
            this.tabPageTextBoxResults.Location = new System.Drawing.Point(4, 22);
            this.tabPageTextBoxResults.Name = "tabPageTextBoxResults";
            this.tabPageTextBoxResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTextBoxResults.Size = new System.Drawing.Size(538, 572);
            this.tabPageTextBoxResults.TabIndex = 1;
            this.tabPageTextBoxResults.Text = "TextBox Results";
            // 
            // richTextBoxResults
            // 
            this.richTextBoxResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxResults.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxResults.Name = "richTextBoxResults";
            this.richTextBoxResults.Size = new System.Drawing.Size(532, 566);
            this.richTextBoxResults.TabIndex = 1;
            this.richTextBoxResults.Text = "";
            // 
            // tabControlSystem
            // 
            this.tabControlSystem.Controls.Add(this.tabPageEVault);
            this.tabControlSystem.Controls.Add(this.tabPageAsigra);
            this.tabControlSystem.Location = new System.Drawing.Point(6, 4);
            this.tabControlSystem.Name = "tabControlSystem";
            this.tabControlSystem.SelectedIndex = 0;
            this.tabControlSystem.Size = new System.Drawing.Size(519, 277);
            this.tabControlSystem.TabIndex = 1005;
            // 
            // tabPageEVault
            // 
            this.tabPageEVault.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageEVault.Controls.Add(this.label1);
            this.tabPageEVault.Controls.Add(this.textBoxDirectorUsername);
            this.tabPageEVault.Controls.Add(this.textBoxDirectorPassword);
            this.tabPageEVault.Controls.Add(this.label2);
            this.tabPageEVault.Controls.Add(this.textBoxDomain);
            this.tabPageEVault.Controls.Add(this.label3);
            this.tabPageEVault.Controls.Add(this.textBoxPortalPassword);
            this.tabPageEVault.Controls.Add(this.textBoxVault);
            this.tabPageEVault.Controls.Add(this.textBoxPortalUsername);
            this.tabPageEVault.Controls.Add(this.label4);
            this.tabPageEVault.Controls.Add(this.label14);
            this.tabPageEVault.Controls.Add(this.buttonPortalLogIn);
            this.tabPageEVault.Controls.Add(this.label13);
            this.tabPageEVault.Controls.Add(this.buttonGetSubsites);
            this.tabPageEVault.Controls.Add(this.comboBoxSites);
            this.tabPageEVault.Controls.Add(this.label9);
            this.tabPageEVault.Controls.Add(this.comboBoxSubsites);
            this.tabPageEVault.Controls.Add(this.buttonDirectorLogIn);
            this.tabPageEVault.Controls.Add(this.label8);
            this.tabPageEVault.Controls.Add(this.buttonGroupAgents);
            this.tabPageEVault.Controls.Add(this.buttonGetSubsiteAgents);
            this.tabPageEVault.Controls.Add(this.buttonGetSiteAgents);
            this.tabPageEVault.Controls.Add(this.buttonGetTasks);
            this.tabPageEVault.Controls.Add(this.label7);
            this.tabPageEVault.Controls.Add(this.comboBoxAgents);
            this.tabPageEVault.Controls.Add(this.comboBoxGroups);
            this.tabPageEVault.Controls.Add(this.label5);
            this.tabPageEVault.Controls.Add(this.label6);
            this.tabPageEVault.Controls.Add(this.comboBoxJobs);
            this.tabPageEVault.Location = new System.Drawing.Point(4, 22);
            this.tabPageEVault.Name = "tabPageEVault";
            this.tabPageEVault.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEVault.Size = new System.Drawing.Size(511, 251);
            this.tabPageEVault.TabIndex = 0;
            this.tabPageEVault.Text = "EVault";
            // 
            // tabPageAsigra
            // 
            this.tabPageAsigra.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageAsigra.Controls.Add(this.label22);
            this.tabPageAsigra.Controls.Add(this.textBoxNOC);
            this.tabPageAsigra.Controls.Add(this.buttonNOCLogin);
            this.tabPageAsigra.Controls.Add(this.label21);
            this.tabPageAsigra.Controls.Add(this.textBoxNOCUsername);
            this.tabPageAsigra.Controls.Add(this.label20);
            this.tabPageAsigra.Controls.Add(this.textBoxNOCPassword);
            this.tabPageAsigra.Controls.Add(this.label16);
            this.tabPageAsigra.Controls.Add(this.label17);
            this.tabPageAsigra.Controls.Add(this.label18);
            this.tabPageAsigra.Controls.Add(this.label19);
            this.tabPageAsigra.Controls.Add(this.comboBoxSets);
            this.tabPageAsigra.Controls.Add(this.comboBoxClients);
            this.tabPageAsigra.Controls.Add(this.comboBoxCustomers);
            this.tabPageAsigra.Controls.Add(this.comboBoxSystems);
            this.tabPageAsigra.Location = new System.Drawing.Point(4, 22);
            this.tabPageAsigra.Name = "tabPageAsigra";
            this.tabPageAsigra.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAsigra.Size = new System.Drawing.Size(511, 251);
            this.tabPageAsigra.TabIndex = 1;
            this.tabPageAsigra.Text = "Asigra";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(57, 32);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "NOC";
            // 
            // textBoxNOC
            // 
            this.textBoxNOC.Location = new System.Drawing.Point(93, 29);
            this.textBoxNOC.Name = "textBoxNOC";
            this.textBoxNOC.Size = new System.Drawing.Size(193, 20);
            this.textBoxNOC.TabIndex = 321;
            this.textBoxNOC.Text = "http://db8.dakotabackup.com/noc";
            this.textBoxNOC.TextChanged += new System.EventHandler(this.textBoxNOCInfos_TextChanged);
            // 
            // buttonNOCLogin
            // 
            this.buttonNOCLogin.Location = new System.Drawing.Point(421, 1);
            this.buttonNOCLogin.Name = "buttonNOCLogin";
            this.buttonNOCLogin.Size = new System.Drawing.Size(48, 23);
            this.buttonNOCLogin.TabIndex = 320;
            this.buttonNOCLogin.Text = "Login";
            this.buttonNOCLogin.UseVisualStyleBackColor = true;
            this.buttonNOCLogin.Click += new System.EventHandler(this.buttonNOCLogIn_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(250, 6);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Password";
            // 
            // textBoxNOCUsername
            // 
            this.textBoxNOCUsername.Location = new System.Drawing.Point(93, 3);
            this.textBoxNOCUsername.Name = "textBoxNOCUsername";
            this.textBoxNOCUsername.Size = new System.Drawing.Size(151, 20);
            this.textBoxNOCUsername.TabIndex = 318;
            this.textBoxNOCUsername.TextChanged += new System.EventHandler(this.textBoxNOCInfos_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "NOC Username";
            // 
            // textBoxNOCPassword
            // 
            this.textBoxNOCPassword.Location = new System.Drawing.Point(309, 3);
            this.textBoxNOCPassword.Name = "textBoxNOCPassword";
            this.textBoxNOCPassword.PasswordChar = '*';
            this.textBoxNOCPassword.Size = new System.Drawing.Size(106, 20);
            this.textBoxNOCPassword.TabIndex = 319;
            this.textBoxNOCPassword.TextChanged += new System.EventHandler(this.textBoxNOCInfos_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 132);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Backup Set";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 106);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "DS-Client";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 81);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Customer";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 56);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "DS-System";
            // 
            // comboBoxSets
            // 
            this.comboBoxSets.FormattingEnabled = true;
            this.comboBoxSets.Location = new System.Drawing.Point(83, 129);
            this.comboBoxSets.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSets.Name = "comboBoxSets";
            this.comboBoxSets.Size = new System.Drawing.Size(276, 21);
            this.comboBoxSets.TabIndex = 325;
            // 
            // comboBoxClients
            // 
            this.comboBoxClients.FormattingEnabled = true;
            this.comboBoxClients.Location = new System.Drawing.Point(83, 104);
            this.comboBoxClients.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxClients.Name = "comboBoxClients";
            this.comboBoxClients.Size = new System.Drawing.Size(276, 21);
            this.comboBoxClients.TabIndex = 324;
            this.comboBoxClients.SelectedValueChanged += new System.EventHandler(this.comboBoxClients_SelectedValueChanged);
            // 
            // comboBoxCustomers
            // 
            this.comboBoxCustomers.FormattingEnabled = true;
            this.comboBoxCustomers.Location = new System.Drawing.Point(83, 79);
            this.comboBoxCustomers.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxCustomers.Name = "comboBoxCustomers";
            this.comboBoxCustomers.Size = new System.Drawing.Size(276, 21);
            this.comboBoxCustomers.TabIndex = 323;
            this.comboBoxCustomers.SelectedValueChanged += new System.EventHandler(this.comboBoxCustomers_SelectedValueChanged);
            // 
            // comboBoxSystems
            // 
            this.comboBoxSystems.FormattingEnabled = true;
            this.comboBoxSystems.Location = new System.Drawing.Point(83, 54);
            this.comboBoxSystems.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSystems.Name = "comboBoxSystems";
            this.comboBoxSystems.Size = new System.Drawing.Size(276, 21);
            this.comboBoxSystems.TabIndex = 322;
            this.comboBoxSystems.SelectedValueChanged += new System.EventHandler(this.comboBoxSystems_SelectedValueChanged);
            // 
            // buttonReadWritePath
            // 
            this.buttonReadWritePath.Location = new System.Drawing.Point(482, 313);
            this.buttonReadWritePath.Name = "buttonReadWritePath";
            this.buttonReadWritePath.Size = new System.Drawing.Size(36, 23);
            this.buttonReadWritePath.TabIndex = 1006;
            this.buttonReadWritePath.Text = "...";
            this.buttonReadWritePath.UseVisualStyleBackColor = true;
            this.buttonReadWritePath.Click += new System.EventHandler(this.buttonReadWritePath_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 936);
            this.Controls.Add(this.buttonReadWritePath);
            this.Controls.Add(this.tabControlSystem);
            this.Controls.Add(this.tabControlResults);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxWritePath);
            this.Controls.Add(this.buttonTextToPDF);
            this.Controls.Add(this.buttonDoIt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.comboBoxDoWhat);
            this.Controls.Add(this.buttonTextToCSV);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Dakota Backup Tools";
            this.groupBoxThatWhat.ResumeLayout(false);
            this.groupBoxThatWhat.PerformLayout();
            this.groupBoxQuery.ResumeLayout(false);
            this.groupBoxQuery.PerformLayout();
            this.groupBoxLogOptions.ResumeLayout(false);
            this.groupBoxLogOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResults)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageTaskQuery.ResumeLayout(false);
            this.tabControlQueryTypes.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPageAgentConfig.ResumeLayout(false);
            this.groupBoxAgentWithWhat.ResumeLayout(false);
            this.groupBoxAgentWithWhat.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControlResults.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBoxDataGrid.ResumeLayout(false);
            this.tabPageTextBoxResults.ResumeLayout(false);
            this.tabControlSystem.ResumeLayout(false);
            this.tabPageEVault.ResumeLayout(false);
            this.tabPageEVault.PerformLayout();
            this.tabPageAsigra.ResumeLayout(false);
            this.tabPageAsigra.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxDirectorUsername;
        private System.Windows.Forms.TextBox textBoxDirectorPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDomain;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxVault;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonTextToCSV;
        private System.Windows.Forms.Button buttonPortalLogIn;
        private System.Windows.Forms.Button buttonGroupAgents;
        private System.Windows.Forms.Button buttonGetSiteAgents;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxGroups;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxJobs;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxAgents;
        private System.Windows.Forms.Button buttonGetTasks;
        private System.Windows.Forms.Button buttonGetSubsiteAgents;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxSubsites;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxSites;
        private System.Windows.Forms.Button buttonGetSubsites;
        private System.Windows.Forms.Button buttonDirectorLogIn;
        private System.Windows.Forms.ComboBox comboBoxDoWhat;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonDoIt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxPortalUsername;
        private System.Windows.Forms.TextBox textBoxPortalPassword;
        private System.Windows.Forms.Button buttonTextToPDF;
        private System.Windows.Forms.GroupBox groupBoxThatWhat;
        private System.Windows.Forms.TextBox textBoxHaveName;
        private System.Windows.Forms.ComboBox comboBoxHaveName;
        private System.Windows.Forms.CheckBox checkBoxHaveSatellite;
        private System.Windows.Forms.CheckBox checkBoxHaveName;
        private System.Windows.Forms.CheckBox checkBoxHaveMailServer;
        private System.Windows.Forms.TextBox textBoxHaveMailServer;
        private System.Windows.Forms.ComboBox comboBoxHaveMailServer;
        private System.Windows.Forms.ComboBox comboBoxHaveOperatingMode;
        private System.Windows.Forms.ComboBox comboBoxHaveOperatingModeCompare;
        private System.Windows.Forms.CheckBox checkBoxHaveOperatingMode;
        private System.Windows.Forms.ComboBox comboBoxHaveSatelliteOperatingMode;
        private System.Windows.Forms.ComboBox comboBoxHaveSatelliteOperatingModeCompare;
        private System.Windows.Forms.CheckBox checkBoxHaveSatelliteOperatingMode;
        private System.Windows.Forms.DateTimePicker dateTimePickerLastSuccessfulBackup;
        private System.Windows.Forms.ComboBox comboBoxHaveLastSuccessfulBackup;
        private System.Windows.Forms.ComboBox comboBoxHaveLastBackupResult;
        private System.Windows.Forms.ComboBox comboBoxHaveLastBackupResultCompare;
        private System.Windows.Forms.CheckBox checkBoxHavePoolSize;
        private System.Windows.Forms.CheckBox checkBoxHaveLastBackup;
        private System.Windows.Forms.CheckBox checkBoxHaveLastSuccessfulBackup;
        private System.Windows.Forms.CheckBox checkBoxHaveLastBackupResult;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxHavePoolSize;
        private System.Windows.Forms.ComboBox comboBoxHavePoolSize;
        private System.Windows.Forms.DateTimePicker dateTimePickerLastBackup;
        private System.Windows.Forms.ComboBox comboBoxHaveLastBackup;
        private System.Windows.Forms.DataGridView dataGridViewResults;
        private System.Windows.Forms.CheckBox checkBoxSaturday;
        private System.Windows.Forms.CheckBox checkBoxThursday;
        private System.Windows.Forms.CheckBox checkBoxFriday;
        private System.Windows.Forms.CheckBox checkBoxWednesday;
        private System.Windows.Forms.CheckBox checkBoxTuesday;
        private System.Windows.Forms.CheckBox checkBoxSunday;
        private System.Windows.Forms.CheckBox checkBoxMonday;
        private System.Windows.Forms.ComboBox comboBoxHaveWeeklySchedule;
        private System.Windows.Forms.CheckBox checkBoxHaveWeeklySchedule;
        private System.Windows.Forms.GroupBox groupBoxLogOptions;
        private System.Windows.Forms.TextBox textBoxWritePath;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBoxErrorsOnly;
        private System.Windows.Forms.CheckBox checkBoxBackupLogs;
        private System.Windows.Forms.CheckBox checkBoxRestoreLogs;
        private System.Windows.Forms.TextBox textBoxLimitLogs;
        private System.Windows.Forms.CheckBox checkBoxLimitLogs;
        private System.Windows.Forms.CheckBox checkBoxErrorsWarningsOnly;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageTaskQuery;
        private System.Windows.Forms.TabPage tabPageAgentConfig;
        private System.Windows.Forms.GroupBox groupBoxAgentWithWhat;
        private System.Windows.Forms.TextBox textBoxAgentMailServerFromAddress;
        private System.Windows.Forms.TextBox textBoxAgentMailServerPassword;
        private System.Windows.Forms.TextBox textBoxAgentMailServerDomain;
        private System.Windows.Forms.CheckBox checkBoxAgentMailServerFromAddress;
        private System.Windows.Forms.CheckBox checkBoxAgentMailServerPassword;
        private System.Windows.Forms.CheckBox checkBoxAgentMailServerDomain;
        private System.Windows.Forms.TextBox textBoxcheckBoxAgentMailServerUsername;
        private System.Windows.Forms.CheckBox checkBoxAgentMailServerUsername;
        private System.Windows.Forms.CheckBox checkBoxAgentConfigNotificationsSuccess;
        private System.Windows.Forms.CheckBox checkBoxAgentConfigNotificationsFailure;
        private System.Windows.Forms.CheckBox checkBoxAgentConfigNotificationsError;
        private System.Windows.Forms.TextBox textBoxAgentConfigRecipients;
        private System.Windows.Forms.CheckBox checkBoxAgentConfigRecipients;
        private System.Windows.Forms.TextBox textBoxAgentConfigMailServer;
        private System.Windows.Forms.CheckBox checkBoxAgentConfigMailServer;
        private System.Windows.Forms.RadioButton radioButtonSuccessOff;
        private System.Windows.Forms.RadioButton radioButtonSuccessOn;
        private System.Windows.Forms.RadioButton radioButtonFailuresOff;
        private System.Windows.Forms.RadioButton radioButtonFailuresOn;
        private System.Windows.Forms.RadioButton radioButtonErrorsOff;
        private System.Windows.Forms.RadioButton radioButtonErrorsOn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxAgentConfigMailServerPort;
        private System.Windows.Forms.CheckBox checkBoxAgentConfigMailServerPort;
        private System.Windows.Forms.TextBox textBoxHaveSafesetCount;
        private System.Windows.Forms.ComboBox comboBoxHaveSafesetCount;
        private System.Windows.Forms.CheckBox checkBoxHaveSafesetCount;
        private System.Windows.Forms.ComboBox comboBoxHaveAgentOnlineStatus;
        private System.Windows.Forms.ComboBox comboBoxHaveAgentOnlineStatusCompare;
        private System.Windows.Forms.CheckBox checkBoxHaveAgentOnlineStatus;
        private System.Windows.Forms.ComboBox comboBoxHavePReqStatus;
        private System.Windows.Forms.ComboBox comboBoxHavePReqStatusCompare;
        private System.Windows.Forms.CheckBox checkBoxHavePReqStatus;
        private System.Windows.Forms.TabControl tabControlQueryTypes;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonChecklistSelectAll;
        private System.Windows.Forms.CheckedListBox checkedListBoxQuery;
        private System.Windows.Forms.Button buttonChecklistAddTask;
        private System.Windows.Forms.Button buttonChecklistRemoveSelected;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxAreChecklisted;
        private System.Windows.Forms.CheckBox checkBoxThatChecklisted;
        private System.Windows.Forms.TextBox textBoxSaveLoadList;
        private System.Windows.Forms.Button buttonLoadList;
        private System.Windows.Forms.Button buttonSaveTaskList;
        private System.Windows.Forms.Button buttonChecklistAddSite;
        private System.Windows.Forms.Button buttonChecklistAddAgent;
        private System.Windows.Forms.Button buttonChecklistAddGroup;
        private System.Windows.Forms.Button buttonChecklistAddSubsite;
        private System.Windows.Forms.TextBox textBoxQuery;
        private System.Windows.Forms.DateTimePicker dateTimePickerLastSafeset;
        private System.Windows.Forms.ComboBox comboBoxHaveLastSafeset;
        private System.Windows.Forms.CheckBox checkBoxHaveLastSafeset;
        private System.Windows.Forms.TabControl tabControlResults;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPageTextBoxResults;
        private System.Windows.Forms.RichTextBox richTextBoxResults;
        private System.Windows.Forms.Button buttonResetQuery;
        private System.Windows.Forms.GroupBox groupBoxQuery;
        private System.Windows.Forms.Button buttonGenerateQuery;
        private System.Windows.Forms.GroupBox groupBoxDataGrid;
        private System.Windows.Forms.TabControl tabControlSystem;
        private System.Windows.Forms.TabPage tabPageEVault;
        private System.Windows.Forms.TabPage tabPageAsigra;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboBoxSets;
        private System.Windows.Forms.ComboBox comboBoxClients;
        private System.Windows.Forms.ComboBox comboBoxCustomers;
        private System.Windows.Forms.ComboBox comboBoxSystems;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxNOC;
        private System.Windows.Forms.Button buttonNOCLogin;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxNOCUsername;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxNOCPassword;
        private System.Windows.Forms.Button buttonReadWritePath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button buttonAddDSCJob;
        private System.Windows.Forms.Button buttonAddDSC;
        private System.Windows.Forms.Button buttonAddDSCustomer;
        private System.Windows.Forms.Button buttonAddDSS;
    }
}

